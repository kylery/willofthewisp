﻿using System;
using System.Collections.Generic;

namespace Assets.Scripts
{
    /// <summary>
    /// There are many constants throughout the game that will be listed here. The
    /// constants is to reduce the use of magic numbers and magic strings.
    /// </summary>
    public static class Constants
    {
        public static readonly bool TRIAL_MODE = false;

        public static readonly float STD_SCREEN_WIDTH = 948f;
        public static readonly float STD_SCREEN_HEIGHT = 533f;

        public const string TURTLE_BOSS_LEVEL = "Simple_Turtle_Boss";
        public const string SHORCA_BOSS_LEVEL = "Shorca_Boss";
        public const string TURTLE_CHALLENGE_ONE_LEVEL = "Enraged_Turtle";
        public const string TURTLE_CHALLENGE_TWO_LEVEL = "TurtleLaserEnvy";
        public const string TURTLE_CHALLENGE_THREE_LEVEL = "MiniMeMassacre";
        public const string SHORCA_CHALLENGE_ONE_LEVEL = "ShorcaBouncyOrbs";
        public const string SHORCA_CHALLENGE_TWO_LEVEL = "ShorcaHideAndSmash";
        public const string SHORCA_CHALLENGE_THREE_LEVEL = "ShorcaTweens";
        public const string MAIN_MENU_LEVEL = "Main_Menu";
        public const string CREDITS_LEVEL = "Credits";

        public static readonly string TURTLE_NAME = "Magnia";
        public static readonly string SHORCA_NAME = "Shorca";
    }
}
