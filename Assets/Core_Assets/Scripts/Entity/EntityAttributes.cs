﻿using System;
using UnityEngine;

namespace Assets.Scripts.Entities
{
    /// <summary>
    /// The Entity Attributes are attributes that are inherint in
    /// all entities.
    /// </summary>
    public class EntityAttributes
    {
        /// <summary>
        /// The direction and speed (velocity) at which the
        /// entity will traverse.
        /// </summary>
        public Vector2 trajectory
        {
            get;
            set;
        }

        /// <summary>
        /// How fast the entity is traveling.
        /// </summary>
        public float speed
        {
            get;
            set;
        }
    }
}
