﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Entities.Player
{
    /// <summary>
    /// This is the public facing attributes that are global to the
    /// player no matter what the assignment they have. 
    /// </summary>
    public class PlayerAttributes : EntityAttributes
    {
    }
}
