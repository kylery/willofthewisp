using UnityEngine;
using System.Collections.Generic;
using System.Timers;
using System;

using Assets.Scripts.Entities;
using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Projectiles;
using Assets.Scripts.Environment;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Entities.Player
{
    /// <summary>
    /// This is the player's access to interact with the broader game. This will handle
    /// all the input control from the player and act accordingly like updating the
    /// swarm's mentality.
    /// </summary>
    public sealed class Player : Entity
    {
        private static readonly float JOYSTICK_EPSILON = 0.25f;
        private const string RETICLE_CHILD_NAME = "PlayerCoreParticleEffect";
        private const string RETICLE_TRAIL_CHILD_NAME = "CursorParticleTrail";

        [SerializeField]
        private float orbitMovementSpeed;
        [SerializeField]
        private float mass;
        [SerializeField]
        private float triggerRadius;
        [SerializeField]
        private float radiusModifier;
        [SerializeField]
        private float maxRadiusFromOrbitToAdjustSpeed;
        [SerializeField]
        private bool showOrb;

        private Vector2 last_position;

        /// <summary>
        /// This is the mass of the central orb and determines how much of an impact it will
        /// have on the orbiting swarm.
        /// </summary>
        public float Mass
        {
            get { return mass; }
        }

        /// <summary>
        /// This will tell if the player's reticle is on or off.
        /// </summary>
        public bool ReticleOn
        {
            get 
            { 
                GameObject cursorParticleSystem = GameObjectUtils.GetChild(gameObject, RETICLE_CHILD_NAME).gameObject;
                return cursorParticleSystem.particleSystem.enableEmission;
            }
        }

        /// <summary>
        /// This will initialize the player attributes that doesn't depend on
        /// other entities being initialized.
        /// </summary>
        void Awake()
        {
            attributes = new PlayerAttributes();
            attributes.speed = orbitMovementSpeed;
        }

        /// <summary>
        /// This will initialize the player attributes that depends on the
        /// other game objects being initialized.
        /// </summary>
        void Start()
        {
            transform.parent = PlayerManager.Instance.transform;
        }

        /// <summary>
        /// This will handle the Player's physics by updating its position based
        /// on the users input of the left joystick.
        /// </summary>
        void FixedUpdate()
        {
            Vector2 moveVector = InputManager.Instance.GetLeftJoystickVector();
            if (moveVector.magnitude < JOYSTICK_EPSILON)
                moveVector = Vector2.zero;
            attributes.trajectory = moveVector == Vector2.zero ? moveVector : moveVector.normalized;

            rigidbody2D.MovePosition(transform.position + (Vector3)(attributes.trajectory * orbitMovementSpeed * Time.deltaTime));
            last_position = transform.position;
        }

        /// <summary>
        /// This will update the player based on the user's input.
        /// </summary>
        void Update()
        {
            InputManager inputManager = InputManager.Instance;
            float rightTrigger = inputManager.GetRightTrigger();

            // Trigger Update
            float radModifer = rightTrigger * radiusModifier;
            float newRadius = triggerRadius + radModifer;
            float velocity = Mathf.Sqrt((MinionManager.Instance.gravity * mass) / newRadius);
            MinionManager.Instance.DampenSwarm(velocity);
        }

        /// <summary>
        /// This will freeze the player for "cutscenes" or "intros" in which the
        /// player should not interact.
        /// </summary>
        public void FreezePlayer()
        {
            enabled = false;
        }

        /// <summary>
        /// This will unfreeze the player.
        /// </summary>
        public void UnfreezePlayer()
        {
            enabled = true;
        }

        /// <summary>
        /// This will determine if the player is frozen or not.
        /// </summary>
        /// <returns>True if the player is frozen.</returns>
        public bool IsPlayerFrozen()
        {
            return !enabled;
        }

        public void TurnOffCursor()
        {
            GameObject cursorParticleSystem = GameObjectUtils.GetChild(gameObject, RETICLE_CHILD_NAME).gameObject;
            GameObject trailParticleSystem = GameObjectUtils.GetChild(gameObject, RETICLE_TRAIL_CHILD_NAME).gameObject;
            cursorParticleSystem.particleSystem.enableEmission = false;
            trailParticleSystem.particleSystem.enableEmission = false;
        }

        public void TurnOnCursor()
        {
            GameObject cursorParticleSystem = GameObjectUtils.GetChild(gameObject, RETICLE_CHILD_NAME).gameObject;
            GameObject trailParticleSystem = GameObjectUtils.GetChild(gameObject, RETICLE_TRAIL_CHILD_NAME).gameObject;
            cursorParticleSystem.particleSystem.enableEmission = true;
            trailParticleSystem.particleSystem.enableEmission = true;
        }

        /// <summary>
        /// This makes sure the orb does not leave the playing area or go through a wall.
        /// </summary>
        /// <param name="col"></param>
        private void OnCollisionEnter2D(Collision2D col)
        {
            if (col.gameObject.tag == "Wall")
            {
                rigidbody2D.MovePosition((Vector2)transform.position - last_position * 0.06f);
            }
        }
    }
}
