﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Entities.Player
{
	/// <summary>
	/// The Player Manager is the class that the game goes through in order to
	/// get certain details from the player that can't be directly called from
	/// the player.
	/// </summary>
	public sealed class PlayerManager : MonoBehaviour
	{
		private static PlayerManager instance;

		private GameObject player;

		/// <summary>
		/// This gets the current instance of the manager.
		/// </summary>
		public static PlayerManager Instance
		{
			get
			{
				if (instance == null)
				{
					instance = (PlayerManager)FindObjectOfType(typeof(PlayerManager));
					if (instance == null)
						UnityEngine.Debug.LogError("Player Manager does not exist");
				}
				return instance;
			}
		}

		/// <summary>
		/// This returns the current instance of the player.
		/// </summary>
		public GameObject Player
		{
			get
			{
                if (player == null)
                {
                    player = GameObject.FindGameObjectWithTag("Player");
                    if (player == null)
                        UnityEngine.Debug.LogError("Player does not exist");
                }
				return player;
			}
		}

        /// <summary>
        /// This will enable the player to update each frame.
        /// </summary>
        public void Enable()
        {
            player.GetComponent<Player>().enabled = true;
            enabled = true;
        }

        /// <summary>
        /// This will disable the player to update each frame.
        /// </summary>
        public void Disable()
        {
            player.GetComponent<Player>().enabled = false;
            enabled = false;
        }

		/// <summary>
		/// This initializes the manager by getting the current instance of the
		/// player.
		/// </summary>
		void Start()
		{
			player = GameObject.FindGameObjectWithTag("Player");
		}
	}
}
