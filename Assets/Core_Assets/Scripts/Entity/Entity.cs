﻿using System.Collections.Generic;
using UnityEngine;
using System;

using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Entities
{
    /// <summary>
    /// The Entity is the base class of all game objects that the player
    /// is able to interact with, and has some AI. 
    /// </summary>
    public abstract class Entity : MonoBehaviour
    {
        /// <summary>
        /// The current health of the entity
        /// </summary>
        public float health
        {
            protected set;
            get;
        }

        /// <summary>
        /// The Entity's attributes
        /// </summary>
        public EntityAttributes attributes
        {
            get;
            protected set;
        }

        /// <summary>
        /// This will directly update the trajectory of the entity.
        /// </summary>
        /// <param name="trajectory"></param>
        public void SetTrajectory(Vector2 trajectory)
        {
            attributes.trajectory = trajectory;
        }

        /// <summary>
        /// This will damage the entity by taking damage from its
        /// health. If the entity's health is lower than 0, then it
        /// will die.
        /// </summary>
        /// <param name="damage">How much damage to take off.</param>
        /// <returns>If the entity has died.</returns>
        public virtual bool LoseHealth(float damage)
        {
            if(health > 0)
            {
                health -= damage;
                if (health <= 0)
                {
                    //Kill Object
                    OnDeath();
                    return true;
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// This function is called when the entity is damaged and their health
        /// is below zero
        /// </summary>
        protected virtual void OnDeath()
        {
            Destroy(gameObject);
        }
    }
}
