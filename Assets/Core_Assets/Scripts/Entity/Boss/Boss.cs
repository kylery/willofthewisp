﻿using UnityEngine;
using System;
using System.Collections.Generic;

using Assets.Scripts.Entities;

namespace Assets.Scripts.Entities.Bosses
{
    /// <summary>
    /// Shared functionality among all game bosses.
    /// </summary>
    public abstract class Boss : Entity
    {
        ///<summary>
        ///  When the boss dies, go to the next level.
        ///</summary>
        ///<param name="damage">Amount to subract from health.</param>
        ///<returns>If the damage applied killed the entity.</returns>
        public override bool LoseHealth(float damage)
        {
            bool hasDied = base.LoseHealth(damage);

            if (hasDied)
            {
                //GameManager.Instance.UpdateGameBossDefeated(Application.loadedLevelName);
            }
            return hasDied;
        }
    }
}
