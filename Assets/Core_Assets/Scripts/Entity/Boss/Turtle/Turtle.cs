using UnityEngine;
using System.Collections;
using System.Linq;
using System;
using Assets.Scripts.Projectiles;

using Assets.Scripts.Cameras;
using Assets.Scripts.Entities.Player;
using Assets.Scripts.Effects;
using Assets.Scripts.Utils;
using Assets.Scripts.Environment;
using Assets.Scripts.LevelManager;
using Assets.Scripts.UI;

namespace Assets.Scripts.Entities.Bosses
{
    ///<summary>
    ///  Class that represents the turtle boss
    ///</summary>
    public sealed class Turtle : Boss, IDestroyableLimb
    {

        #region Serializable Fields

        [SerializeField]
        private bool shouldEnrage = true;
        [SerializeField]
        private float initialHealth;
        private float logicalHealth;
        [SerializeField]
        public float speed;
        private float steamInitialDelay = 2.0f;
        [SerializeField]
        private float switchToMoltenStateDelay;
        [SerializeField]
        private float switchToWAMFlameSpinStateDelay;
        [SerializeField]
        private GameObject[] topLevelLimbs;
        [SerializeField]
        private float limbDeathLength;
        [SerializeField]
        private Color moltenColor;
        [SerializeField]
        private float reacquirePlayerDelay;
        [SerializeField]
        private FlameThrower[] flameThrowers;
        [SerializeField]
        private GameObject[] shadowLimbs;
        [SerializeField]
        private float flameSpinSpeed;
        [SerializeField]
        private int wamSpins;
        [SerializeField]
        private int enragedSpins;
        [SerializeField]
        private float roarLength;
        [System.Serializable]
        class FireDeathSputter : System.Object
        {
            public int triesTillSteam = 0;
            public int curTriesTillSteam
            {
                get; set;
            }
            public float degradationRatio = 0;
            public float fireSputterOnTime = 0;
            public float curFireSputterOnTime
            {
                get; set;
            }

            public float fireSputterOffTime = 0;
            public float curFireSputterOffTime
            {
                get; set;
            }
        }
        [SerializeField]
        private FireDeathSputter fireDeathSputter;
        [SerializeField]
        private float jitterDelay;
        [SerializeField]
        private float jitterMagnitude;
        [SerializeField]
        private float deathCameraShakeTime;
        [SerializeField]
        private float deathFallScale = 5;

        [SerializeField]
        private AudioClip steamClip;
        [SerializeField]
        private AudioClip deathClip;
        [SerializeField]
        private float deathDuration = 1.5f;
        [SerializeField]
        private AudioClip roarClip;
        private float soundDuration;
        //sounds[0] == steam; sounds[1] == death; sounds[2] == turtle roar;
        private AudioSource[] sounds; 

        #endregion

        #region Private Fields

        private uint limbsLeft;

        private struct Limb
        {
            public DestroyableLimb prot;
            public WeakPointController wp;
            public ParticleSystem md;
            public Animator animator;
            public KillZoneScript heat;
            public FadeBack fader;
        }

        private Limb head;
        private Limb[] limbs;
        private ParticleSystem[] steamVents;
        private GameObject[] volcanos;
        private ParticleSystem[]  allParticles;

        private System.Random rand;

        private float curJitterDelay;
        private float curLimbDeath;
        private float curSteamInitial;
        private float curSwitchToMoltenState;
        private float curSwitchToWAMFlameSpinState;
        private uint curEnragedCount;
        private uint curSpins;
        private float curSpinRotation;
        private Vector2 wanderDir;

        [SerializeField]
        private float moveTowardsCenterDistanceThreshold;
        private Vector2 centerOfLevel;

        private float curReacquirePlayerDelay;
        
        private Player.Player player;

        private float curIntroTime;
        private float curWaitTime;
        private float introSpeed;

        private State goToStateFromWait;
        private State goToStateFromLimbDeath;
        private bool approachedPlayer;

        private float curFlameThrowerSwitchDelay;
        private bool spinning;
        private Quaternion beforeFlameRotation;

        private IEnumerator curRoarState;
        private float curRoarLength;

        private GameObject[] corners;

        #endregion

        #region Accessor Fields

        /// <summary>
        /// Is the turtle in the Intro State.
        /// </summary>
        public bool isIntroState
        {
            get { return state == State.INTRO; }
        }

        public uint destroyedCount
        {
            get
            {
                return (uint)topLevelLimbs.Length - limbsLeft;
            }
        }

		public uint startingLimbCount
		{
			get
			{
				return (uint)topLevelLimbs.Length;
			}
		}

        #endregion

        /// <summary>
        /// There are many different states that the turtle
        /// could be in, this lists them all.
        /// </summary>
        private enum State
        {
            WAIT,
            INTRO,
            WAM_STEAMINITIAL,
            WAM_STEAM,  //WAM stands for Whack a Mole
            WAM_MOLTEN,
            WAM_FLAMESPIN,
            ENRAGED_PREP,
            ENRAGED_MAIN,
            LIMB_DEATH,
            DEATH
        }
        private State state;

        ///<summary>
        ///  This is called when the object is created, before Start.
        ///</summary>
        void Awake()
        {
            health = initialHealth;
            logicalHealth = initialHealth;
            state = State.WAIT;
            curWaitTime = 1f;
            goToStateFromWait = State.INTRO;
            introSpeed = 350;
            approachedPlayer = false;
        }

        ///<summary>
        ///  This is called when the object is created, after Awake.
        ///</summary>
        void Start()
        {
            limbsLeft = (uint)topLevelLimbs.Length;

            //Initialize Limb vectors
            limbs = new Limb[topLevelLimbs.Length];
            steamVents = new ParticleSystem[topLevelLimbs.Length];
            for(int i = 0; i < topLevelLimbs.Length; i++)
            {
                limbs[i].prot = topLevelLimbs[i].transform.Find("Protusion").GetComponent<DestroyableLimb>();
                limbs[i].wp = limbs[i].prot.transform.Find("WeakPoint").GetComponent<WeakPointController>();
                limbs[i].md = limbs[i].prot.transform.Find("MoltenDrip").GetComponent<ParticleSystem>();
                limbs[i].animator = limbs[i].prot.GetComponent<Animator>();
                limbs[i].heat = limbs[i].wp.GetComponent<KillZoneScript>();
                limbs[i].fader = limbs[i].wp.GetComponent<FadeBack>();
                steamVents[i] = topLevelLimbs[i].transform.Find("SteamParticleSystem").GetComponent<ParticleSystem>();
                if(topLevelLimbs[i].name == "Head")
                {
                    head = limbs[i];
                }
            }
            //Choose volcanos to shoot towards
            volcanos = GameObject.FindGameObjectsWithTag("Volcano");

            //Save all particle system for rotation purposes
            allParticles = this.transform.GetComponentsInChildren<ParticleSystem>(true);
            
            spinning = false;
            beforeFlameRotation = transform.rotation;

            corners = TurtleLevelManager.Instance.Corners;
            centerOfLevel = TurtleLevelManager.Instance.CenterOfLevel.transform.position;

            curSteamInitial = steamInitialDelay;
            curSwitchToMoltenState = switchToMoltenStateDelay;
            curSwitchToWAMFlameSpinState = switchToWAMFlameSpinStateDelay;
            curEnragedCount = 0;

            curSpins = 0;
            curSpinRotation = 0f;

            rand = new System.Random();
            attributes = new EntityAttributes();
            player = PlayerManager.Instance.Player.GetComponent<Player.Player>();
            wanderDir = (player.transform.position - transform.position).normalized;
            sounds = GetComponents<AudioSource>();

            fireDeathSputter.curTriesTillSteam = 0;
            fireDeathSputter.curFireSputterOnTime = fireDeathSputter.fireSputterOnTime;
            fireDeathSputter.curFireSputterOffTime = fireDeathSputter.fireSputterOffTime;

            SetupIntro();            
        }

        /// <summary>
        /// All variables that need to be setup for the intro state will
        /// be defined here for clarity.
        /// </summary>
        private void SetupIntro()
        {
            curIntroTime = 2f;
            MakeInvincible();
            player.FreezePlayer();
            Camera.main.GetComponent<CameraShake>().ShakeCamera(curWaitTime);
        }

        #region Wander Code

        ///<summary>
        ///  Initialize wander direction to a random unit vector
        ///</summary>
        /// <param name="updateFacing">
        /// Whether to update the visual direction this turtle faces.
        /// </param>
        private void RandomWander(bool updateFacing = true)
        {
            SetWanderDirection(UnityEngine.Random.insideUnitCircle, updateFacing);
        }

        /// <summary>
        /// Set a random direction towards the center fo the
        /// field.
        /// </summary>
        /// <param name="updateFacing">
        /// Whether to update the visual direction this turtle faces.
        /// </param>
        private void WanderTowardsCenter(bool updateFacing = true)
        {
            const float WANDER_OFFSET_MAX = 20f;
            //GameObject wall = GameObject.FindGameObjectWithTag("Wall");
            //Vector2 center = wall.transform.GetComponent<EdgeCollider2D>().bounds.center;
            float range = UnityEngine.Random.value * WANDER_OFFSET_MAX;
            Vector2 wanderTowards = centerOfLevel + range * UnityEngine.Random.insideUnitCircle;
            Vector2 targetDir = (wanderTowards - (Vector2)transform.position).normalized;
            SetWanderDirection(targetDir, updateFacing);
        }

        /// <summary>
        /// This will make the turtle head towards the closest corner.
        /// </summary>
        /// <param name="updateFacing">
        /// Whether to update the visual direction this turtle faces.
        /// </param>
        /// <param name="updateFacing">
        /// Whether to update the visual direction this turtle faces.
        /// </param>
        private void WanderTowardsCorner(bool updateFacing = true)
        {
            GameObject corner = ClosestCorner();
            Vector2 targetDir = (corner.transform.position - transform.position).normalized;
            SetWanderDirection(targetDir, updateFacing);
        }

        /// <summary>
        /// This will set the turtle's direction to head towards the current position of the player.
        /// </summary>
        /// <param name="updateFacing">
        /// Whether to update the visual direction this turtle faces.
        /// </param>
        private void WanderTowardsPlayer(bool updateFacing = true)
        {
            curReacquirePlayerDelay = reacquirePlayerDelay;
            GameObject player = PlayerManager.Instance.Player;
            Vector2 targetDir = (player.transform.position - transform.position).normalized;
            SetWanderDirection(targetDir, updateFacing);
        }

        /// <summary>
        /// This will directly set the wander direction and handle any rotations and
        /// scaling to the turtle to make sure the turtle moves in a consistent way.
        /// </summary>
        /// <param name="newDirection"></param>
        /// <param name="changeFacing">
        /// Whether to update the visual direction this turtle faces.
        /// </param>
        private void SetWanderDirection(Vector2 newDirection, bool changeFacing)
        {
            if(changeFacing)
            {
                if (newDirection.x < 0 && transform.right.x > 0 || newDirection.x > 0 && transform.right.x < 0)
                {
                    transform.Rotate(new Vector3(0, 0, 180));
                    Vector3 oldScale = transform.localScale;
                    oldScale.y *= -1;
                    transform.localScale = oldScale;
                    //The particles do not scale with parent object and so are
                    //handled differently
                    foreach(ParticleSystem particleSystem in allParticles)
                    {
                        if(particleSystem != null)
                        {
                            //Hotfix for steam particles
                            if(particleSystem.gameObject.name == "SteamParticleSystem")
                                particleSystem.transform.forward = Vector3.Reflect(particleSystem.transform.forward, Vector3.up);
                            else
                                particleSystem.transform.right = Vector3.Reflect(particleSystem.transform.right, Vector3.right);
                        }
                    }
                }
            }
            wanderDir = newDirection;
        }

        #endregion

        #region Update Code

        ///<summary>
        ///  This is called once a frame.  The length between frames is
        ///  variable.  This function is only used for logical calculations
        ///  that do not involve physics.
        ///</summary>
        public void Update()
        {
            switch(state)
            {
                case State.WAIT:
                    WaitUpdate();
                    break;
                case State.INTRO:
                    IntroUpdate();
                    break;
                case State.WAM_STEAMINITIAL:
                    WamSteamInitialUpdate();
                    break;
                case State.WAM_STEAM:
                    WamSteamUpdate();
                    break;
                case State.WAM_MOLTEN:
                    WamMoltenUpdate();
                    break;
                case State.WAM_FLAMESPIN:
                    WamFlameSpinUpdate();
                    break;
                case State.ENRAGED_PREP:
                    EnragedPrepUpdate();
                    break;
                case State.ENRAGED_MAIN:
                    EnragedMainUpdate();
                    break;
                case State.LIMB_DEATH:
                    LimbDeathUpdate();
                    break;
                case State.DEATH:
                    DeathUpdate();
                    break;
                default:
                    Debug.LogError("The Turtle should always have a valid state.\nTurtle State: " + state);
                    break;
            }
            soundDuration -= Time.deltaTime;
            if(soundDuration <= 0)
            {
                sounds[1].Stop();
            }
        }

        /// <summary>
        /// This will stop the turtle's movement until the time has elapsed.
        /// It is up to the state that calls the wait state to determine which
        /// state it should return to.
        /// </summary>
        private void WaitUpdate()
        {
            curWaitTime -= Time.deltaTime;
            if (curWaitTime <= 0)
            {
                ChangeState(goToStateFromWait);
            }
        }

        /// <summary>
        /// This will determine the turtle's movement in the intro as it counts down and rushes
        /// the player.
        /// </summary>
        private void IntroUpdate()
        {
            const float DISTANCE_TO_SWITCH = 400f;

            curIntroTime -= Time.deltaTime;
            if (curIntroTime <= 0)
            {
                TurtleLevelManager.Instance.MoveViewBoxes();

                RemoveInvincibility();
                curSwitchToWAMFlameSpinState = switchToWAMFlameSpinStateDelay;
                player.UnfreezePlayer();
                ChangeState(State.WAM_STEAMINITIAL);
            }
            if (Vector2.Distance(transform.position, player.transform.position) < DISTANCE_TO_SWITCH & !approachedPlayer)
            {
                wanderDir = new Vector2(0, -1);
                introSpeed = 100;
                ChangeState(State.WAIT);
                curWaitTime = 1f;
                approachedPlayer = true;
                Camera.main.GetComponent<CameraShake>().ShakeCamera(curIntroTime);
            }
        }

        /// <summary>
        /// Initial Steam State before the turtle starts releasing steam
        /// </summary>
        private void WamSteamInitialUpdate()
        {
            if (curSteamInitial > 0f)
            {
                curSteamInitial -= Time.deltaTime;
            }
            else
            {
                curSteamInitial = steamInitialDelay;
                ChangeState(State.WAM_STEAM);
            }
        }

        /// <summary>
        ///
        /// </summary>
        private void WamSteamUpdate()
        {
            if(curSwitchToMoltenState > 0f)
            {
                curSwitchToMoltenState -= Time.deltaTime;
               
            }
            else
            {
                curSwitchToMoltenState = switchToMoltenStateDelay;
                ChangeState(State.WAM_MOLTEN);
            }
        }

        /// <summary>
        /// This will move the turtle in a normal fashion bases on its
        /// target direction and switch to the volcanic state when it is
        /// time.
        /// </summary>
        private void WamMoltenUpdate()
        {
            // Flame spin in place.
            if (curSwitchToWAMFlameSpinState > 0f)
            {
                curSwitchToWAMFlameSpinState -= Time.deltaTime;
            }
            else
            {
                curSwitchToWAMFlameSpinState = switchToWAMFlameSpinStateDelay;
                ChangeState(State.WAM_FLAMESPIN);
            }
        }

        /// <summary>
        /// This will spin the turtle untill it is time to switch states
        /// </summary>
        private void WamFlameSpinUpdate()
        {
            // Sets wander direction
            if (curReacquirePlayerDelay > 0f)
            {
                curReacquirePlayerDelay -= Time.deltaTime;
            }
            else
            {
                WanderTowardsPlayer(!spinning);
            }

            if (!FlameSpinUpdate(wamSpins))
            {
                ChangeState(State.WAM_STEAMINITIAL);
            }
        }

        /// <summary>
        /// Enraged Preparation Update
        /// </summary>
        private void EnragedPrepUpdate()
        {
            //Initial time for turtle to move towards center
            if(Vector2.Distance(centerOfLevel,(Vector2)transform.position) <= moveTowardsCenterDistanceThreshold)
            {
                //Enable the shell colliders
                var tmp = GetComponents<Collider2D>();
                foreach(var t in tmp)
                {
                    t.enabled = true;
                }

                //If enraged count hit, go back to steam
                if (curEnragedCount >= destroyedCount)
                {
                    curEnragedCount = 0;
                    ChangeState(State.WAM_STEAMINITIAL);
                }
                else
                {
                    ChangeState(State.ENRAGED_MAIN);
                }
            }
        }

        /// <summary>
        /// Enraged Main Update
        /// </summary>
        private void EnragedMainUpdate()
        {
            // Sets wander direction
            if (curReacquirePlayerDelay > 0f)
            {
                curReacquirePlayerDelay -= Time.deltaTime;
            }
            else
            {
                WanderTowardsPlayer(!spinning);
            }

            if (!FlameSpinUpdate(enragedSpins))
            {
                ChangeState(State.ENRAGED_PREP);
            }
            
        }

        /// <summary>
        /// Death Limb Update
        /// </summary>
        private void LimbDeathUpdate()
        {
            curLimbDeath -= Time.deltaTime;
            curRoarState.MoveNext();
            if (curLimbDeath <= 0)
            {
                ChangeState(goToStateFromLimbDeath);
            }
        }

        /// <summary>
        /// Death state update
        /// </summary>
        private void DeathUpdate()
        {
            // Jitter
            curJitterDelay -= Time.deltaTime;
            if(curJitterDelay <= 0 && fireDeathSputter.curTriesTillSteam < fireDeathSputter.triesTillSteam)
            {
                curJitterDelay = jitterDelay;
                transform.position = transform.position + (Vector3)UnityEngine.Random.insideUnitCircle*jitterMagnitude;
            }
            //Fire death animation
            fireDeathSputter.curFireSputterOnTime -= Time.deltaTime;
            if(fireDeathSputter.curFireSputterOnTime <= 0)
            {
                ActivateFlames(false);  //Turn fire off
                fireDeathSputter.curFireSputterOffTime -= Time.deltaTime;
                //Repeat fire spurt
                if(fireDeathSputter.curFireSputterOffTime <= 0)
                {
                    fireDeathSputter.curTriesTillSteam++;
                    if(fireDeathSputter.curTriesTillSteam >= fireDeathSputter.triesTillSteam)
                    {
                        //Turn steam on and point them up
                        foreach(ParticleSystem st in steamVents)
                        {
                            st.gameObject.SetActive(true);
                            st.transform.rotation = Quaternion.Euler(90, 90, 0);
                        }
                    }
                    else
                    {
                        ActivateFlames(true);  //Turn fire on
                        fireDeathSputter.fireSputterOffTime *= fireDeathSputter.degradationRatio;
                        fireDeathSputter.curFireSputterOffTime = fireDeathSputter.fireSputterOffTime;
                        fireDeathSputter.curFireSputterOnTime = fireDeathSputter.fireSputterOnTime;
                    }
                }
            }
        }

        /// <summary>
        /// This will spin the turtle around an the z-axis with flamethrowers
        /// on every flameSpinDelay+spinTime seconds.
        /// </summary>
        /// <returns>Bool of whether or not the Flame spin is done. True if in process of spinning, false otherwise</returns>
        private bool FlameSpinUpdate(int spins)
        {
            if(spins <= 0)
            {
                return false;
            }
            
            if(!spinning)
            {
                if(curRoarState == null)
                {
                    sounds[2].clip = roarClip;
                    sounds[2].Play();
                    curRoarState = Roar(true);
                }

                //Don't start spinning until we have roared
                if(!curRoarState.MoveNext())
                {
                    sounds[2].Stop();
                    curSpins = 0;
                    spinning = true;
                    ActivateFlames(true);
                    beforeFlameRotation = transform.rotation;
                    RetractWeakPoints();
                }
            }

            //If we are spinning, spin!
            if(spinning)
            {
                if(curSpins < spins)
                {
                    transform.Rotate(0,0, flameSpinSpeed*Time.deltaTime);
                    curSpinRotation += flameSpinSpeed * Time.deltaTime;
                    if(curSpinRotation >= 360)
                    {
                        curSpinRotation = 0;
                        curSpins++;
                    }
                }
                else
                {
                    ResetSpin();
                    return false;
                }
            }
            return true;
        }

        #endregion

        ///<summary>
        ///  Called once per physics frame.  The time between frames is fixed.
        ///</summary>
        public void FixedUpdate()
        {
            float turtleSpeed = 0;
            bool obeyPhysics = true;
            bool toMove = true;  //A very bad FIX to allow the turtle to be affected by gravity
            switch (state)
            {
                case State.DEATH:
                    toMove = false;
                    goto case State.WAIT;
                case State.LIMB_DEATH:
                case State.WAM_MOLTEN:
                case State.WAIT:
                    turtleSpeed = 0;
                    break;
                case State.INTRO:
                    turtleSpeed = introSpeed;
                    break;
                case State.ENRAGED_PREP:
                    obeyPhysics = false;
                    goto default;
                default:
                    turtleSpeed = speed;
                    break;
            }

            if(toMove)
            {
                if (obeyPhysics)
                    rigidbody2D.MovePosition((Vector2)transform.position + turtleSpeed * wanderDir * Time.deltaTime);
                else
                    transform.position = transform.position + turtleSpeed * (Vector3)wanderDir * Time.deltaTime;
            }
        }

        #region State Change Functions

        ///<summary>
        ///  Change the state of the turtle.
        ///</summary>
        private void ChangeState(State nextState)
        {
            switch(nextState)
            {
                case State.WAIT:
                case State.INTRO:
                    // Nothing needs to be changed to go to these states (they are
                    // handled at the start). Do Nothing.
                    break;
                case State.WAM_STEAMINITIAL:
                    ChangeToWamSteamInitial();
                    break;
                case State.WAM_STEAM:
                    ChangeToWamSteam();
                    break;
                case State.WAM_MOLTEN:
                    ChangeToWamMolten();
                    break;
                case State.WAM_FLAMESPIN:
                    ChangeToWamFlameSpin();
                    break;
                case State.ENRAGED_PREP:
                    ChangeToEnragedPrep();
                    break;
                case State.ENRAGED_MAIN:
                    ChangeToEnragedMain();
                    break;
                case State.LIMB_DEATH:
                    ChangeToLimbDeath();
                    break;
                case State.DEATH:
                    ChangeToDeath();
                    break;
                default:
                    Debug.LogError("The Turtle should always have a valid state.\nState: " + state);
                    break;
            }

            state = nextState;
        }

        /// <summary>
        /// Change to initial Steam State
        /// </summary>
        private void ChangeToWamSteamInitial()
        {
            RetractWeakPoints();
            wanderDir = Vector2.zero;
        }

        /// <summary>
        /// Change to Steam State. Where the turtle actually streams steam
        /// </summary>
        private void ChangeToWamSteam()
        {
            uint nonMolten;
            if(limbsLeft != 0)
              nonMolten = (uint)UnityEngine.Random.Range(1, (int)(limbsLeft));
            else
              nonMolten = 0;
            ReleaseSteam(nonMolten);
        }
        
        /// <summary>
        /// When changing to normal, this will choose the target direction for the
        /// Turtle to go. If it has been in a corner for too long, then it will go
        /// towards the center. It will go towards a corner based on a probabilistic
        /// chance where the probability rises as health goes down. Otherwise it will
        /// choose a random direction.
        /// </summary>
        private void ChangeToWamMolten()
        {
            wanderDir = Vector2.zero;
            ExposeWeakPoints(UnreleaseSteam());
        }

        /// <summary>
        /// Changes States to WAM Flame Spin
        /// </summary>
        private void ChangeToWamFlameSpin()
        {
            WanderTowardsPlayer();
        }

        /// <summary>
        /// Change States to Enraged
        /// </summary>
        private void ChangeToEnragedPrep()
        {
            if (shouldEnrage)
            {
                //Disable the shell colliders
                var tmp = GetComponents<Collider2D>();
                foreach(var t in tmp)
                {
                    t.enabled = false;
                }
                RetractWeakPoints();
                WanderTowardsCenter();
            }
            else
            {
                ChangeState(State.WAM_FLAMESPIN);
            }
        }

        /// <summary>
        /// Changes States to Enraged Main
        /// </summary>
        private void ChangeToEnragedMain()
        {
            ShootPermutation(3);// Fire At Volcanos
            WanderTowardsPlayer();
            curEnragedCount++;
        }

        /// <summary>
        /// Changes States to Limb Death 
        /// Note: goToStateFromLimbDeath is expected to be set up by caller.
        /// Note: curRoarState is expected to be set by caller of func.
        /// </summary>
        private void ChangeToLimbDeath()
        {
            curLimbDeath = limbDeathLength;
        }

        /// <summary>
        /// Changes States to Death 
        /// </summary>
        private void ChangeToDeath()
        {
            curJitterDelay = jitterDelay;
            rigidbody2D.gravityScale = deathFallScale;
            Camera.main.GetComponent<CameraShake>().ShakeCamera(deathCameraShakeTime);
            TurtleLevelManager.Instance.DestroyBoss(this.gameObject);
            ActivateFlames(true);
        }

        #endregion

        #region Turtle Functions

        /// <summary>
        /// Make the turtle roar.  This is a generator function which ends when
        /// the roar has ended.
        /// </summary>
        /// <param name="flameIndicate">
        /// Whether to use flame indication while roaring.
        /// </param>
        /// <returns>An iterable object repesenting Roar update</returns>
        private IEnumerator Roar(bool flameIndicate)
        {
            //initialize
            curRoarLength = roarLength;
            if (head.prot != null)
            {
                head.animator.SetTrigger("Roar");
            }
            if(flameIndicate)
              IndicateFlames(true);
            yield return null;
            //Update
            curRoarLength -= Time.deltaTime;
            while (curRoarLength > 0f)
            {
                curRoarLength -= Time.deltaTime;
                yield return null;
            }
            if(flameIndicate)
              IndicateFlames(false);
        }

        /// <summary>
        /// Change a limb into molten or revert it.
        /// </summary>
        /// <param name="limb"> The limb to change </param>
        /// <param name="changeMolten"> Whether to change or revert </param>
        private void TurnLimbMolten(Limb limb, bool changeMolten)
        {
            //Reset the fade if it stopped midway
            limb.fader.EndFade();
            limb.wp.enabled = changeMolten;
            limb.heat.enabled = !changeMolten;
            limb.md.gameObject.SetActive(!changeMolten);
            limb.animator.SetBool("Molten", changeMolten);

        }


        /// <summary>
        /// Reset everything to do with spinning.  If the turtle was spinning,
        /// it will put the turtle at the rotation before spinning, expose
        /// weakpoints, and deactivate the flamethrowers.
        /// </summary>
        private void ResetSpin()
        {
            curRoarState = null;
            curSpins = 0;
            curSpinRotation = 0;
            //If we were spinning
            if (spinning)
            {
                spinning = false;
                transform.rotation = beforeFlameRotation;
            }
            ActivateFlames(false);
            IndicateFlames(false);
        }

        /// <summary>
        /// Releases Steam Vents
        /// </summary>
        /// <param name="weakPoint">Number of the steam vent to leave off</param>
        private void ReleaseSteam(uint weakPoint)
        {
            uint nonMolten = weakPoint;
            if(weakPoint > limbsLeft)
            {
                Debug.LogError("Invalid WeakPoint Chosen");
            }
            uint count = 0;
            for(int i = 0; i < steamVents.Length; i++)
            {
                if(limbs[i].prot != null)
                { 
                    if (count == nonMolten)
                        steamVents[i].gameObject.SetActive(false);
                    else
                        steamVents[i].gameObject.SetActive(true);
                    count++;
                }
            }
            sounds[0].clip = steamClip;
            sounds[0].Play();
        }

        /// <summary>
        /// Unreleases Steam Vents
        /// </summary>
        /// <returns>then number of the steam vent that was not active. If all were active, returns 0</returns>
        private uint UnreleaseSteam()
        {
            uint ventOff = 0;
            uint count = 0;
            for(int i = 0; i < steamVents.Length; i++)
            {
                if(limbs[i].prot != null)
                {
                    if(steamVents[i] != null)
                    { 
                        if (!steamVents[i].gameObject.activeSelf)
                            ventOff = count;
                        else
                            steamVents[i].gameObject.SetActive(false);
                    }
                    count++;
                }
            }
            sounds[0].Stop();
            return ventOff;
        }

        /// <summary>
        ///  Protude all protusions and activate weakpoints.
        /// </summary>
        /// <param name="weakPoint">Weak Point that should turn molten</param>
        private void ExposeWeakPoints(uint weakPoint)
        {
            uint nonMolten = weakPoint;
            if(weakPoint > limbsLeft)
            {
                Debug.LogError("Invalid WeakPoint Chosen");
            }
            uint count = 0;
            foreach (Limb limb in limbs)
            {
                if (limb.prot != null)
                {
                    //Expose point
                    limb.animator.SetBool("Retract", false);
                    limb.wp.gameObject.SetActive(true);

                    //Set one point to not molten
                    TurnLimbMolten(limb, count == nonMolten);
                    limb.animator.SetLayerWeight(2, 0);
                    limb.animator.SetLayerWeight(1, 1);
                    count++;
                }
            }

            //Reset flamethrowers
            foreach (FlameThrower fl in flameThrowers)
            {
                fl.StartIndicating(false);
                fl.StopThrowing();
            }

        }

        ///<summary>
        ///  Retract all protusions and disable weakpoints.
        ///</summary>
        private void RetractWeakPoints()
        {
            foreach (Limb limb in limbs)
            {
                if (limb.prot != null)
                {
                    limb.animator.SetBool("Retract", true);
                    if(limb.wp != null)
                    {
                        limb.wp.gameObject.SetActive(false);
                        TurnLimbMolten(limb, true);
                    }
                }
            }
        }

        /// <summary>
        /// This will give the Turtle invincibility.
        /// </summary>
        private void MakeInvincible()
        {
            foreach (Limb limb in limbs)
            {
                if (limb.prot != null)
                {
                    limb.wp.gameObject.SetActive(false);
                }
            }
        }

        /// <summary>
        /// This will rmeove the turtles invincibility so that it can be
        /// attacked again.
        /// </summary>
        private void RemoveInvincibility()
        {
            foreach (Limb limb in limbs)
            {
                if (limb.prot != null)
                {
                    limb.wp.gameObject.SetActive(true);
                }
            }
        }

        /// <summary>
        /// Turn on or off all available flame throwers
        /// </summary>
        /// <param name="on">Whether to turn on or off</param>
        private void ActivateFlames(bool on)
        {
            //Turn on or off all available flame throwers
            foreach (FlameThrower fl in flameThrowers)
            {
                if (on)
                    fl.StartThrowing();
                else
                    fl.StopThrowing();
            }
        }

        /// <summary>
        /// Show or hide indication for all available flame throwers
        /// </summary>
        /// <param name="show">Whether to show or hide</param>
        private void IndicateFlames(bool show)
        {
            //Turn on or off all available flame throwers
            foreach (FlameThrower fl in flameThrowers)
            {
                fl.StartIndicating(show);
            }
        }

        /// <summary>
        /// Shoots to shots random unique volcanos when called.  This function
        /// will emit the charge at the nearest emitter to the volcano.
        /// </summary>
        /// <param name="shots">The number of shots to fire or volcanos to hit</param>
        private void ShootPermutation(uint shots)
        {
            //Shuffle
            int n = volcanos.Length;
            while (n > 1)
            {
                n--;
                int k = rand.Next(n + 1);
                var temp = volcanos[k];
                volcanos[k] = volcanos[n];
                volcanos[n] = temp;
            }
            var chosen = volcanos.Take((int)shots);

            //Shoot out closest emitters to volcano
            foreach (GameObject volcano in chosen)
            {
                GameObject closest = null;
                float minDist = Single.PositiveInfinity;
                foreach (GameObject go in topLevelLimbs)
                {
                    float dist = Vector2.Distance(go.transform.position, volcano.transform.position);
                    if (minDist > dist)
                    {
                        minDist = dist;
                        closest = go;
                    }
                }
                closest.transform.Find("Emitter").GetComponent<Emitter>().Emit(volcano);
            }
        }

        #endregion

        #region Turtle Utils

        /// <summary>
        /// This will return the position of the closest corner that
        /// the turtle is near.
        /// </summary>
        /// <returns>The closest corner</returns>
        private GameObject ClosestCorner()
        {
            GameObject closestCorner = null;
            if (corners != null && corners.Length > 0)
            {
                closestCorner = corners[0];
                for (int index = 1; index < corners.Length; index++)
                {
                    if (corners[index] != null)
                    {
                        if (Vector2.Distance(corners[index].transform.position, transform.position) <
                            Vector2.Distance(closestCorner.transform.position, transform.position))
                        {
                            closestCorner = corners[index];
                        }
                    }
                }
            }
            return closestCorner;
        }

        #endregion

        ///<summary>
        ///  Cause damage to the turtle through the limbs.
        ///</summary>
        ///<param name="damage">Damage to apply.</param>
        ///<param name="limb">The limb being damaged</param>
        ///<returns>If the limb should be destroyed</returns>
        public bool LoseLimbHealth(float damage, GameObject limb)
        {
            logicalHealth -= damage;
            float nextDegredation = (initialHealth * ((limbsLeft - 1)/(float)topLevelLimbs.Length));
            base.LoseHealth(base.health - logicalHealth);
            if(nextDegredation > logicalHealth)
            {
                LimbKilled(limb);
                return true;
            }
            return false;
        }

        ///<summary>
        /// Overwrite destruction of this object such that it isn't destroyed
        /// when health drops below zero
        ///</summary>
        protected override void OnDeath()
        {
            //Do Nothing
		}
	

        /// <summary>
        /// If a Limb is killed. Respond
        /// <param name="limb">The limb killed</param>
        /// </summary>
        private void LimbKilled(GameObject limb)
        {
            sounds[1].clip = deathClip;
            soundDuration = deathDuration;
            sounds[1].Play();
            State stateToChange = shouldEnrage ? State.ENRAGED_PREP : State.WAM_FLAMESPIN;
            goToStateFromLimbDeath = stateToChange;
            //Means we are going to die when this function returns
            if(limbsLeft == 1)
              goToStateFromLimbDeath = State.DEATH;

            Destroy(limb, limbDeathLength);
            limbsLeft--;

            switch(state)
            {
                case State.WAM_MOLTEN:
                    curReacquirePlayerDelay = reacquirePlayerDelay;
                    curSwitchToWAMFlameSpinState = switchToWAMFlameSpinStateDelay;
                    ChangeState(State.LIMB_DEATH);
                    break;
                case State.WAM_FLAMESPIN:
                    ResetSpin();
                    ChangeState(State.LIMB_DEATH);
                    break;
                case State.WAIT:
                case State.INTRO:
                case State.WAM_STEAM:
                case State.ENRAGED_PREP:
                case State.ENRAGED_MAIN:
                case State.LIMB_DEATH:
                    Debug.LogError("Turtle Should Not Have Lost a Limb");
                    break;
                case State.DEATH:
                    //Do nothing because we are dieing
                    break;
                default:
                    Debug.LogError("The Turtle should always have a valid state.\nState: " + state);
                    break;
            }

            if(limb != head.prot)
                curRoarState = Enumerable.Empty<int>().GetEnumerator();
            else
                curRoarState = Roar(false);


        }

        ///<summary>
        ///  Called whenever something collides with the turtle.
        ///</summary>
        public void OnCollisionEnter2D(Collision2D col)
        {
            if(col.gameObject.tag == "Wall")
            {
                if(state == State.WAM_MOLTEN && !spinning)
                {
                    //Stop wandering into wall.
                    while(Vector2.Dot(wanderDir, col.contacts[0].normal) < 0)
                    {
                        RandomWander();
                    }
                }
                else if (state == State.ENRAGED_PREP)
                {
                    WanderTowardsCenter();
                }
            }
        }
    }
}
