﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class for simple flame thrower which can be turned on and off
/// </summary>
public class FlameThrower : MonoBehaviour
{
    private ParticleSystem particles;
    private Collider2D[] colliders;
    private ParticleSystem indication;

    [SerializeField]
    private AudioClip flameThrowerClip;

    private AudioSource sound;

    /// <summary>
    /// Called when object becomes awake in the scene
    /// </summary>
    private void Awake()
    {
        indication = transform.Find("Indicator").gameObject.GetComponent<ParticleSystem>();
        particles = transform.Find("Particles").gameObject.GetComponent<ParticleSystem>();
        indication.Stop();
        particles.Stop();
        colliders = GetComponents<Collider2D>();
        sound = gameObject.GetComponent<AudioSource>();
    }

    /// <summary>
    /// Show the indication that flames are coming, or hide it.
    /// </summary>
    /// <param name="show">Whether to show or hide</param>
    public void StartIndicating(bool show)
    {
        if(show)
          indication.Play();
        else
          indication.Stop();
    }

    /// <summary>
    /// Starts emitting flame and damaging minions
    /// </summary>
    public void StartThrowing()
    {
        sound.clip = flameThrowerClip;
        sound.Play();

        particles.Play();
        foreach(Collider2D collider in colliders)
          collider.enabled = true;
    }

    /// <summary>
    /// Stops emitting flame and damaging minions
    /// </summary>
    public void StopThrowing()
    {
        sound.Stop();
        particles.Stop();
        foreach(Collider2D collider in colliders)
          collider.enabled = false;
    }
}
