﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Entities.Bosses
{
    ///<summary>
    /// Class which supports chunked damage applied to an entity whose limb
    /// this represents.
    ///</summary>
    public class DestroyableLimb : Entity
    {
        [SerializeField]
        private Turtle Owner;
        private IDestroyableLimb IOwner;

        /// <summary>
        /// Ensure that the owner object inherits from the expected interface.
        /// Note: This is because interface variables are not serializable in
        /// inspector.
        /// </summary>
        public void Awake()
        {
            IOwner = Owner.GetComponent(typeof(IDestroyableLimb)) as IDestroyableLimb;
            DebugUtils.Assert(IOwner != null,
                              "Owner object does not inherit from IDestroyableLimb!");
        }

        ///<summary>
        ///  Damage the main limb owner.
        ///</summary>
        ///<param name="damage">Amount to subract from health.</param>
        ///<returns>If the damage applied killed the limb or not.</returns>
        public override bool LoseHealth(float damage)
        {
            damage = damage + 1;  // +1 to avoid rounding errors
            if(IOwner.LoseLimbHealth(damage, this.gameObject))
            {
                GetComponent<Animator>().SetTrigger("Death");
                return true;
            }
            return false;
        }
    }
}
