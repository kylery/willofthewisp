﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Entities.Bosses
{
    /// <summary>
    /// Interface which dictates the necessary components of an entity which
    /// loses it's health and notifies callers that a limb should have died.
    /// </summary>
    public interface IDestroyableLimb
    {
        /// <summary>
        /// Called when a limb connected to this entity is losing health.
        /// <param name="damage"> Damage applied </param>
        /// <param name="limb"> Limb being damaged </param>
        /// <returns>If the limb should destroy itself or not</returns>
        bool LoseLimbHealth(float damage, GameObject limb);
    }
}
