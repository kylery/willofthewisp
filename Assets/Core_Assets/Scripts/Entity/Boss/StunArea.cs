﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Entities.Bosses
{
    /// <summary>
    /// Stun area script
    /// </summary>
    public class StunArea : MonoBehaviour
    {
        public float impactThreshold;
        [SerializeField]
        private GameObject stunnable;
        private IStunnable istunnable;
        
        private Vector2 sumImpact;

        /// <summary>
        /// Ensure the object given to us is actually stunnable.
        /// </summary>
        public void Start()
        {
            istunnable = stunnable.GetComponent(typeof(IStunnable)) as IStunnable;
            DebugUtils.Assert(istunnable != null,
                              "Stunnable object does not inherit from IStunnable!");
        }

        /// <summary>
        /// Checks if the sum impact across frames was large enough to stun the
        /// stunnable object.
        /// </summary>
        public void FixedUpdate()
        {
            if(sumImpact.magnitude >= impactThreshold)
            {
                istunnable.Stun();
            }
            sumImpact = Vector2.zero;
        }

        /// <summary>
        /// Called when something collides with the stunnable area.  This
        /// calculates the impact of a single object.
        /// </summary>
        public void OnTriggerEnter2D(Collider2D col)
        {
            sumImpact += Physics2d.CalculateImpact(col);
        }
    }
}
