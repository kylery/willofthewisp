﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Entities.Bosses
{
    /// <summary>
    /// Interface which dictates whether an object is stunnable or not.
    /// </summary>
    public interface IStunnable
    {
        void Stun();
    }
}
