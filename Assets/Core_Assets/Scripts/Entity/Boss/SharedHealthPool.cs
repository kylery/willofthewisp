﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Entities
{
    ///<summary>
    ///  Class used to damage two entities as if they shared a single health
    ///  pool.  The amount that this class shares with the other entity is
    ///  adjustable.
    ///</summary>
    public class SharedHealthPool : Entity
    {
        [SerializeField]
        private float initialHealth;

        [SerializeField]
        private Entity SharedEntity;

        /// <summary>
        /// Only for Base Lose Health. If Lose Health would get the entity 0 health. Respond
        /// </summary>
        [SerializeField]
        private OnLosingAllHealth onLosingAllHealth = OnLosingAllHealth.DESTROY;
      
        private bool permaDisabled = false;

        public enum OnLosingAllHealth
        {
            DISABLE,
            DESTROY,
            NOTHING
        }

        ///<summary>
        ///  Called when object is created, but before Start.
        ///</summary>
        void Awake()
        {
            health = initialHealth;
        }

        ///<summary>
        ///  Lose health and which is shared by SharedEntity.
        ///</summary>
        ///<param name="damage">Amount to subract from health.</param>
        ///<returns>If the damage applied killed the entity.</returns>
        public override bool LoseHealth(float damage)
        {
            damage = damage + 1;  // +1 to avoid rounding errors
            float prevHealth = health;
            bool res =  base.LoseHealth(damage);
            //Don't deal more damage than what is left
            if(res)
            {
                damage = prevHealth;
            }
            if(SharedEntity.LoseHealth(damage))
            {
                return true;
            }
            return res;
        }


        protected override void OnDeath()
        {
            switch (onLosingAllHealth)
            {
                case (OnLosingAllHealth.DESTROY):
                    Destroy(gameObject);
                    break;
                case (OnLosingAllHealth.DISABLE):
                    permaDisabled = true;
                    gameObject.SetActive(false);
                    break;
                case (OnLosingAllHealth.NOTHING):
                    // Do Nothing
                    break;
            }
        }

        /// <summary> q
        /// SUPER GHETTO PATCH. If its perma disabled, don't allow enabling.
        /// </summary>
        public void OnEnable()
        {
            if (permaDisabled)
                gameObject.SetActive(false);
        }
    }
}
