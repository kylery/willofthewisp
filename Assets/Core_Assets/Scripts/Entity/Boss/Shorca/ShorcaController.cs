﻿using UnityEngine;
using System.Collections;

using Assets.Scripts.Utils;
using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Entities.BossLevels;
using Assets.Scripts.Entities.Player;
using Assets.Scripts.Entities;
using System.Collections.Generic;
using Assets.Scripts.Projectiles;
using Assets.Scripts.Effects;
using Assets.Scripts.Cameras;
using Assets.Scripts.LevelManager;

namespace Assets.Scripts.Entities.Bosses
{
    /// <summary>
    /// Shorca boss main script
    /// </summary>
    public class ShorcaController : Boss
    {
        //Health
        [SerializeField]
        private float initialHealth;

        [SerializeField]
        private float introWaitTime;
        private float curWaitTime;
        private ShorcaState stateToChangeFromWait;

        // Shorca State Machine
        private ShorcaState curState;
        private enum ShorcaState
        {
            WAIT,
            START,
            IDLE_PATHING,
            CHARGING_PREPARATION,
            CHARGING_PREPARATION_STEP2,
            CHARGING_ACTIVE,
            DEATH
        }

        // Protusion variables
        [SerializeField]
        private ShorcaArmorController[] armorPieces;
        private List<GameObject> Protusions = new List<GameObject>();
        private uint currentArmor;
        public float tosserImpactThreshold; // Impact ice balls require to knock off armor

        // Movement variables
        public float idleSpeed;
        [SerializeField]
        private float maxSpeed;
        public float rotSpeed;
        public float flipDelta;

        // Pathing Variables
        public GameObject[] path;
        private uint curPathNode;
        public float transitionNodeDistance; // Distance from a node before it transitions nodes or switches states

        //Color fading back to idle
        private Color initialColor;
        [SerializeField]
        private float fadeBackDelay = 1f;
        private IEnumerator curFadeBackDelay;
        private bool idleFading = false;
        
        //Charging variables
        public int pathPointsTillCharge;
        public GameObject[] chargingPoints;
        public float chargeRotSpeed;
        private Quaternion chargePrepAngle;
        public float preparationSpeed = 30f;
        private uint curChargingNode; // Current charging node
        public Color chargeColor = new Color(1f, 0f, 0f);
        public float chargePreparationDelay = 5.0f; 
        public float chargingDelay = 5.0f;
        public float chargeSpeed = 100.0f;
        public int additiveCharges = 1;
        private int curAdditiveCharges;

        // Varaibles for if the shorca collides with a ice berg upon charging
        [Tooltip("Slows down the shorca to the given percentage of the charge speed when the Shorca hits an iceberg")]
        [Range(0.1f, 1.0f)]
        public float chargeSpeedSlowed = 0.9f;
        public float chargeSlowedDistance = 10.0f;
        private float curChargeSlowedDistance = 0.0f;
        private bool collidedWithTundra = false;

        // Charging variables current values
        private int curPathPointsTouched;
        private float curChargePreparationDelay;
        private float curChargingDelay;
        private Vector2 chargeDir;

        // Invulnerability variables upon entering idle from charge
        [SerializeField]
        private float invulnerabilityToIceUponEnteringIdleDelay;
        private float curInvulnerabilityDelay;
        private bool invulnerableToIce;

        [SerializeField]
        private float waitAfterDeathTime;
        [SerializeField]
        private float floatGravityScale;
        private float curAfterDeath;

        //Sounds 
        [SerializeField]
        private AudioClip chargeClip;
        [SerializeField]
        private AudioClip startUpClip;
        [SerializeField]
        private AudioClip deathClip;
        [SerializeField]
        private AudioClip armorClip;
        // sound[0] == charge; sound[1] startUp; sound[2] = death sound;
        // sound[3] = armor break;
        private AudioSource[] sound;

        //Animation variables
        private Animator animator;

        /// <summary>
        /// Use this for initialization
        /// </summary>
        public void Start ()
        {
            initialColor = GetComponent<SpriteRenderer>().color;
            currentArmor = 0;
            curAdditiveCharges = 0;
            this.health = initialHealth;
            this.attributes = new EntityAttributes();

            curPathNode = 0;
            curChargingNode = 0;

            curChargePreparationDelay = chargePreparationDelay;
            curChargingDelay = chargingDelay;

            curWaitTime = introWaitTime;
            stateToChangeFromWait = ShorcaState.START;
            curState = ShorcaState.WAIT;
            curAfterDeath = 0;
            Protusions = new List<GameObject>();

            animator = GetComponent<Animator>();
            if (animator == null)
                Debug.LogError("Animator not attached to Shorca");

            sound = gameObject.GetComponents<AudioSource>();
            if(sound == null)
            {
                Debug.LogError("There is no audio source on the Shorca");
            }
        }

        ///<summary>
        /// Update function which is called once per frame.
        ///</summary>
        public void Update()
        {
            
            if(curState == ShorcaState.START)
            {
                ShorcaLevelManager.Instance.MoveViewBoxes();
                SwitchStates();
            }

            //Always face up
            FaceUp();
            //Handle Timers for tracking times in states
            HandleStateTimers();
        }

        /// <summary>
        /// Handles all timers in all states.
        /// </summary>
        private void HandleStateTimers()
        {
            switch (curState)
            {
                case ShorcaState.CHARGING_PREPARATION:
                case ShorcaState.START:
                    {
                        // Do Nothing. No Timers
                    }
                    break;
                case ShorcaState.DEATH:
                    if (curAfterDeath < waitAfterDeathTime)
                    {
                        curAfterDeath += Time.deltaTime;
                        if (curAfterDeath >= waitAfterDeathTime)
                        {
                            rigidbody2D.gravityScale = floatGravityScale;
                        }
                    }
                    break;
                case ShorcaState.WAIT:
                    curWaitTime -= Time.deltaTime;
                    if (curWaitTime <= 0)
                    {
                        SwitchStates();
                    }
                    break;
                case ShorcaState.IDLE_PATHING:
                    {
                        //Update the fade back to color
                        idleFading = curFadeBackDelay.MoveNext();

                        if(invulnerableToIce)
                        {
                            curInvulnerabilityDelay -= Time.deltaTime;
                            if(curInvulnerabilityDelay <= 0 )
                            {
                                invulnerableToIce = false;
                            }
                        }
                    }
                    break;
                case ShorcaState.CHARGING_PREPARATION_STEP2:
                    {
                        // Waits a bit before it actually charges the player.
                        curChargePreparationDelay -= Time.deltaTime;

                        // Ghetto Indicator right now
                        float ratio = curChargePreparationDelay / chargePreparationDelay;
                        GetComponent<SpriteRenderer>().color = Color.Lerp(chargeColor, initialColor, ratio);

                        if (curChargePreparationDelay <= 0)
                        {
                            SwitchStates();
                        }
                    }
                    break;
                case ShorcaState.CHARGING_ACTIVE:
                    {
                        // Reduce timer that is keeping track of charging time
                        curChargingDelay -= Time.deltaTime;

                        if (curChargingDelay <= 0 && !collidedWithTundra)
                        {
                            SwitchStates();
                        }
                    }
                    break;
                default:
                    Debug.LogError("Unknown Shorca State");
                    break;
            }
        }

        /// <summary>
        /// Fades the Shorca to a color from a color over a set time.
        /// </summary>
        /// <param name="from">The color to start at.</param>
        /// <param name="to">The color to end at.</param>
        /// <param name="fadeBackDelay">The total time for the transition</param>
        /// <returns>Enumerator to call next on during update</returns>
        private IEnumerator FadeBackIdle(Color from, Color to, float fadeBackDelay)
        {
            float current = fadeBackDelay;
            while(current > 0f)
            {
                current -= Time.deltaTime;
                GetComponent<SpriteRenderer>().color = Color.Lerp(to, from, current/fadeBackDelay);
                yield return null;
            }
            GetComponent<SpriteRenderer>().color = to;
        }

        
        #region Switch State Methods
        /// <summary>
        /// Every call switches the Orca to the next State
        /// 
        /// TODO: WHY!?!
        /// </summary>
        private void SwitchStates()
        {
            switch (curState)
            {
                case ShorcaState.WAIT:
                    curState = stateToChangeFromWait;
                    break;
                case ShorcaState.START:
                    {
                        SwitchStateToChargingPrepTwo();
                    }
                    break;
                case ShorcaState.IDLE_PATHING:
                    {
                        GetComponent<FadeBack>().EndFade();
                        SwitchStateToChargingPrep();
                    }
                    break;
                case ShorcaState.CHARGING_PREPARATION:
                    {
                        SwitchStateToChargingPrepTwo();
                    }
                    break;
                case ShorcaState.CHARGING_PREPARATION_STEP2:
                    {
                        SwitchStateToChargingActive();
                    }
                    break;
                case ShorcaState.CHARGING_ACTIVE:
                    {
                        curAdditiveCharges++;
                        if (curAdditiveCharges < additiveCharges)
                        {
                            SwitchStateToChargingPrepTwo();
                        }
                        else
                        {
                            curAdditiveCharges = 0;
                            SwitchStateToIdle();
                        }
                    }
                    break;
                case ShorcaState.DEATH:
                    break; // Do Nothing
                default:
                    Debug.LogError("Unknown Shorca State");
                    break;
            }
        }

        /// <summary>
        /// Method that switches the state to IDLE
        /// </summary>
        private void SwitchStateToIdle()
        {
            animator.speed = 1.0f;
            animator.SetBool("charging", false);

            curState = ShorcaState.IDLE_PATHING;
            curPathPointsTouched = 0;

            curFadeBackDelay = FadeBackIdle(GetComponent<SpriteRenderer>().color,
                                            initialColor,
                                            fadeBackDelay);

            //Makes sure the Shroca is invulernable to ice for a certain delay upon entering idle state.
            //Handles cases where Shorca would suicide into ice pieces upon charging
            invulnerableToIce = true;
            curInvulnerabilityDelay = invulnerabilityToIceUponEnteringIdleDelay;

            TurnProtusionsOn();

            // Determines which node it should go to
            // Find any nodes in front of the Shorca
            float closestNodeDistance = Mathf.Infinity;
            for (uint i = 0; i < path.Length; i++)
            {
                float distance = Vector2.Distance(path[i].transform.position, transform.position);
                
                // If in front of shorca
                if(InFrontOfShorca(path[i]))
                {
                    // If current path node is not in front of the Shorca, prioritize the one in front of the shorca. Then priortize distance.
                    if( ((distance < (Vector2.Distance(path[curPathNode].transform.position,transform.position))) || !InFrontOfShorca(path[curPathNode])) )
                    {
                        closestNodeDistance = distance;
                        curPathNode = i;    
                    }
                }
                else
                {
                    // If current path node is not in front of the shorca, prioritize node upon distance
                    if(!InFrontOfShorca(path[curPathNode]))
                    {
                        if (distance < closestNodeDistance)
                        {
                            closestNodeDistance = distance;
                            curPathNode = i;
                        }    
                    }
                }
            }

        }


        /// <summary>
        /// Method that switches the state to CHARGING_PREPARATION
        /// </summary>
        private void SwitchStateToChargingPrep()
        {
            animator.speed = 2.0f ;
            
            curState = ShorcaState.CHARGING_PREPARATION;

            TurnProtusionsOff();

            //Start up sound here.
            sound[1].clip = startUpClip;
            sound[1].Play();
            //Determines the node point that is the farthest from the player
            float farthestNodeDistance = Mathf.NegativeInfinity;
            for (uint i = 0; i < chargingPoints.Length; i++)
            {
                float distance = Vector2.Distance(chargingPoints[i].transform.position, PlayerManager.Instance.Player.transform.position);
                if (distance > farthestNodeDistance)
                {
                    farthestNodeDistance = distance;
                    curChargingNode = i;
                }
            }
        }

        /// <summary>
        /// Method that switches the state to CHARGING_PREPARATION2
        /// </summary>
        private void SwitchStateToChargingPrepTwo()
        {
            animator.speed = 1.0f;
            animator.SetBool("charging", false);
            curState = ShorcaState.CHARGING_PREPARATION_STEP2;
            curChargePreparationDelay = chargePreparationDelay;

            // Sets charge direction
            GameObject chargeTarget = PlayerManager.Instance.Player;
            //MinionManager.Instance.getMinionClosestToObjectLinearSearch(gameObject);
            
            chargeDir = (chargeTarget.transform.position - transform.position).normalized;
            // Save off current rotation
            chargePrepAngle = transform.rotation;
        }

        /// <summary>
        /// Method that switches the state to CHARGING_ACTIVE
        /// </summary>
        private void SwitchStateToChargingActive()
        {
            animator.speed = 1.0f;

            animator.SetBool("charging", true);

            //charging
            sound[1].Stop();
            sound[0].clip = chargeClip;
            sound[0].Play();

            curState = ShorcaState.CHARGING_ACTIVE;
            curChargingDelay = chargingDelay;
            collidedWithTundra = false;
            curChargeSlowedDistance = 0;
            float angle = Mathf.Atan2(chargeDir.y, chargeDir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        /// <summary>
        /// Switch State to Death and Play Death Animation
        /// </summary>
        private void SwitchStateToDeath()
        {
            ShorcaLevelManager.Instance.DestroyBoss(this.gameObject);
            //Place sound here
            sound[2].clip = deathClip;
            sound[2].Play();
            curState = ShorcaState.DEATH;
            animator.SetBool("death", true);
        }

        #endregion


        /// Ensures the Shorca is facing upwards regardless of its rotation.
        ///</summary>
        private void FaceUp()
        {
            const float FLIP_ANGLE = 90f;

            //If we have rotated 90 from start, go ahead and flip y scale
            if(Vector2.Angle(-Vector2.right, transform.right) > FLIP_ANGLE)
            {
                Vector3 tmpScale = transform.localScale;
                tmpScale.y = Mathf.Abs(tmpScale.y);
                transform.localScale = tmpScale;
            }
            else
            {
                Vector3 tmpScale = transform.localScale;
                tmpScale.y = -Mathf.Abs(tmpScale.y);
                transform.localScale = tmpScale;
            }
        }

        void FixedUpdate()
        {
            Move();
        }

        /// <summary>
        /// Rotate the Shorca by rotSpeed torwards targetDir.  This function
        /// will never rotate past the direction.  This function will also
        /// perform an about face if the Shorca is further than flip delta away
        /// from target direction.
        /// <summary
        /// <param name="targetDir">
        /// The direction this Shorca is desired to face.
        /// </param>
        /// <param name="rotSpeed">The speed to rotate the Shorca.</param>
        private void RotateTowards(Vector2 targetDir, float rotSpeed)
        {
            float positiveDiff = Vector2.Angle(targetDir, this.transform.right);
            bool rotHard = positiveDiff > flipDelta;
            //About face if we are going opposite direction
            if(rotHard)
            {
                transform.right = -transform.right;
            }
            else
            {
                float targetAngle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg;
                float curAngle = Mathf.Atan2(transform.right.y, transform.right.x) * Mathf.Rad2Deg;
                float angleDiff = Mathf.DeltaAngle(curAngle, targetAngle);
                Vector3 rot = transform.rotation.eulerAngles;
                rot.z = rot.z + Mathf.Sign(angleDiff)*Mathf.Min(Time.deltaTime*rotSpeed, Mathf.Abs(angleDiff));
                transform.rotation = Quaternion.Euler(rot);
            }
        }

        /// <summary>
        /// More direct movement
        /// </summary>
        /// <param name="target">Target destination</param>
        /// <returns>A bool of whether or not the Shorca is within "transition Node Distance"</returns>
        private bool MoveTowardsDirect(Vector2 target, float speed, float rotSpeed)
        {
            Vector2 targetDir = (target - (Vector2)transform.position).normalized;

            RotateTowards(targetDir, rotSpeed);

            rigidbody2D.MovePosition(transform.position + (Vector3)(targetDir * speed * Time.deltaTime));
            SetTrajectory(transform.right);

            if (Vector2.Distance(target, transform.position) <= transitionNodeDistance)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        
        /// <summary>
        /// All movement that the shorca does is contained in this function.
        /// Transisions that occur between states in this function are strictly related to reaching destinations.
        /// </summary>
        public void Move()
        {
            switch(curState)
            {
                case ShorcaState.WAIT:
                case ShorcaState.DEATH:
                case ShorcaState.START:
                    {
                        // Do Nothing
                    }
                    break;
                case ShorcaState.IDLE_PATHING:
                    { 
                        //Move towards the pathing node cycling through pathing nodes
                        if (MoveTowardsDirect(path[curPathNode].transform.position, idleSpeed, rotSpeed))
                        {
                            curPathNode++;
                            if (curPathNode >= path.Length)
                            {
                                curPathNode = 0;
                            }
                            // After touching a certain amount of pathing nodes, switch states
                            curPathPointsTouched++;
                            if (curPathPointsTouched >= pathPointsTillCharge)
                            {
                                curPathPointsTouched = 0;
                                SwitchStates();
                            }
                        }
                    }
                    break;
                case ShorcaState.CHARGING_PREPARATION:
                    {
                        // Move towards the charging node then prepare to charge
                        if (MoveTowardsDirect(chargingPoints[curChargingNode].transform.position, preparationSpeed, chargeRotSpeed))
                        {
                            SwitchStates();
                        }         
                    }
                    break;
                case ShorcaState.CHARGING_PREPARATION_STEP2:
                    {
                        // Waits a bit before it actually charges the player.
                        float ratio = curChargePreparationDelay / chargePreparationDelay;

                        // Rotates
                        float angle = Mathf.Atan2(chargeDir.y, chargeDir.x) * Mathf.Rad2Deg;
                        transform.rotation = Quaternion.Lerp(chargePrepAngle, Quaternion.AngleAxis(angle, Vector3.forward), 1-ratio);
                    }
                    break;
                case ShorcaState.CHARGING_ACTIVE:
                    {
                        // Charge the given charge direction
                        // Move at max speed if the Shorca hasn't collied with a Tundra
                        // Else move at a fractional slowed speed a certain amount of distance before switching states
                        if (!collidedWithTundra)
                        {
                            rigidbody2D.MovePosition((Vector2)transform.position + chargeSpeed * chargeDir * Time.deltaTime);
                        }
                        else
                        {
                            float slowedSpeed = chargeSpeed * chargeSpeedSlowed;
                            float distanceTraveled = slowedSpeed * Time.deltaTime;
                            rigidbody2D.MovePosition((Vector2)transform.position + distanceTraveled * chargeDir);
                            curChargeSlowedDistance += distanceTraveled;

                            // If the Shorca goes the max distance slowed. Switch States
                            if(curChargeSlowedDistance >= chargeSlowedDistance)
                            { 
                                SwitchStates();
                            }
                        }
                    }
                    break;
                default:
                    Debug.LogError("Unknown Shorca State");
                    break;
            }
        }


        /// <summary>
        /// Turns Protusions on
        /// </summary>
        private void TurnProtusionsOn()
        {
            foreach( GameObject pro in Protusions)
            {
                if(pro != null)
                { 
                    pro.SetActive(true);
                }
            }
        }

        /// <summary>
        /// Turns Protusions off
        /// </summary>
        private void TurnProtusionsOff()
        {
            foreach (GameObject pro in Protusions)
            {
                if(pro != null)
                { 
                    pro.SetActive(false);
                }
            }
        }


        /// <summary>
        /// Determines if the given object is in front of the Shorca
        /// </summary>
        /// <param name="item">Item to see if is in front of the Shorca</param>
        /// <returns>Bool of whether or not object is in front of Shorca</returns>
        private bool InFrontOfShorca(GameObject item)
        {
            // Determine objects position relative to Shorca
            bool shouldFaceRight;
            // To the right
            if (item.transform.position.x >= transform.position.x)
            {
                shouldFaceRight = true;
            }
            // To the Left
            else
            {
                shouldFaceRight = false;
            }

            return (shouldFaceRight == (transform.right.x >= 0));
        }

        /// <summary>
        /// Overrided Death
        /// </summary>
        protected override void OnDeath()
        {
            SwitchStateToDeath();
        } 

        /// <summary>
        /// Called when something collides with the shorca.
        /// </summary>
        public void OnCollisionEnter2D(Collision2D col)
        {
            if(curState == ShorcaState.IDLE_PATHING)
            { 
                //Determines if armor should be knocked off
                if(col.gameObject.tag == "Tosser" && !invulnerableToIce && col.gameObject.GetComponent<IceBall>().Lethal && Physics2d.CalculateImpact(col).magnitude >= tosserImpactThreshold)
                {
                    if(currentArmor < armorPieces.Length)
                    {
                        PlayArmorBreakSound();
                        Protusions.Add( armorPieces[currentArmor++].Eject());
                        SwitchStates();
                    }

                    col.gameObject.SendMessage("Destroy", null, SendMessageOptions.DontRequireReceiver);
                }
                
                if((col.gameObject.tag == "Tosser" || col.gameObject.tag == "Minion") && !idleFading)
                  GetComponent<FadeBack>().BeginFade();
            }

            if(curState == ShorcaState.CHARGING_ACTIVE)
            {
                const float CAMERA_SHAKE_TIME = 1;

                if(col.gameObject.tag == "Minion")
                {
                    col.gameObject.GetComponent<Minion>().LoseHealth(9001);
                }

                //If collides with the tundra.
                if(col.gameObject.tag == "Tundra")
                {
                    col.gameObject.GetComponent<Tundra>().LoseHealth (9000);
                    collidedWithTundra = true;
                    Camera.main.GetComponent<CameraShake>().ShakeCamera(CAMERA_SHAKE_TIME);
                }

                if(col.gameObject.tag =="Wall")
                {
                    Camera.main.GetComponent<CameraShake>().ShakeCamera(CAMERA_SHAKE_TIME);
                    SwitchStates();
                }
            }
        }

        private void PlayArmorBreakSound()
        {
            sound[3].clip = armorClip;
            sound[3].Play();
        }
    }
}
