﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Environment;

namespace Assets.Scripts.Entities.Bosses
{
    /// <summary>
    /// Primary script which controls the armor of the Shorca boss.
    /// </summary>
    public class ShorcaArmorController : MonoBehaviour
    {

        [SerializeField]
        private GameObject CoveredObject;

        [SerializeField]
        private float ejectionMagnitude;
        [SerializeField]
        private float ejectionTorque;
        
        private GameObject detachedArmorObject;


        /// <summary>
        /// Use this for initialization
        /// </summary>
        public void Start ()
        {
            CoveredObject.SetActive(false);
        }
        
        /// <summary>
        /// Update is called once per frame
        /// </summary>
        public void Update ()
        {
        }

        /// <summary>
        /// Ejects this piece of armor upwards (relative). Armor that is ejected is a new instance
        /// spawned. Spawned instance will live indefinetly. Controller is disabled upon ejection.
        /// </summary>
        public GameObject Eject()
        {
            CoveredObject.SetActive(true);
            rigidbody2D.isKinematic = false;
            
            //Find Attached armor
            Transform attachedArmor = transform.FindChild("Mesh");
            SpriteRenderer attachedSpriteRenderer = attachedArmor.GetComponent<SpriteRenderer>();
            
            //Instantiate Detached Armor
            detachedArmorObject = (GameObject) Instantiate(new GameObject(), transform.position,attachedArmor.rotation);
            
            //Scale detached equally to attached
            detachedArmorObject.transform.localScale = attachedArmor.lossyScale;

            //Add spriterenderer to spawned object and assign it matching properties
            SpriteRenderer detachedSpriteRenderer = detachedArmorObject.AddComponent<SpriteRenderer>();
            detachedSpriteRenderer.sprite = attachedSpriteRenderer.sprite;
            detachedSpriteRenderer.color = attachedSpriteRenderer.color;
            
            //Give the detached armor piece a rigidbody and force.
            Rigidbody2D childBody = detachedArmorObject.AddComponent<Rigidbody2D>();
            childBody.AddForce(this.transform.up * ejectionMagnitude);
            childBody.AddTorque(ejectionTorque);
            Disable();
           
            return CoveredObject;
        }

        /// <summary>
        /// This function "kills" the armor piece when called, removing it from
        /// the game.
        /// </summary>
        private void Disable()
        {
            gameObject.SetActive(false);
        }
    }
}
