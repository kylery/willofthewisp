﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Entities.Bosses
{
    /// <summary>
    /// Interface which dictates the necessary components of an entity which
    /// loses it's health in chunks (even though damage is applied along a
    /// continuous distribution).
    /// </summary>
    public interface IChunkedHealth
    {
        /// <summary>
        /// Called when damage is dealt to this entity via a source.  The
        /// damage is aggregated until it is enough to decrement the actual
        /// health by a chunk.  
        /// <param name="damage">
        /// The damage to be applied.  This is normal damage which will be
        /// accrued until the entity chooses to decrease it's own health bar.
        /// </param>
        /// <returns>If the healthbar decremented a chunk or not</returns>
        bool LoseLogicalHealth(float damage);
    }
}
