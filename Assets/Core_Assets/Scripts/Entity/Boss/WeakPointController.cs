﻿using UnityEngine;
using System.Collections;

using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Effects;
using Assets.Scripts.UI;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Entities.Bosses
{
    ///<summary>
    ///  Script meant to act as a point things can be damaged at.
    ///</summary>
    public class WeakPointController : MonoBehaviour
    {
        [SerializeField]
        public float minImpactTillDamage;
        [SerializeField]
        public float baseDamageReceived;
        [SerializeField]
        public float maxImpactReceived;
        [SerializeField]
        public float maxDamageReceived;

        [SerializeField]
        private Entity entityToDamage;
        private Vector2 sumImpact;
        private Vector2 sumPosition;
        private uint minionsSoaked;

        private FadeBack damageFader;

        [SerializeField]
        private GameObject[] objectsToEnableOnDestruction;

        [SerializeField]
        private GameObject damageFeedback;
        [SerializeField]
        public float largeDamageAmount;
        [SerializeField]
        private AudioClip damageSoundClip;
        [SerializeField]
        private float durationDamageSound = 1.0f;
        private float soundTimer;
        private AudioSource sound;

        public bool destroyControllerOnLosingAllHealth = true;

        ///<summary>
        ///  Called when object is created, but before Start.  This call grabs
        ///  components on the game object and changes the parent of this game
        ///  object as the entity which we should damage.
        ///</summary>
        void Awake()
        {
            //soundSource = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundController>();
            sound = gameObject.GetComponent<AudioSource>();
            if(sound == null)
            {
                Debug.LogError("There is no sound in " + gameObject);
                return;
            }

            damageFader = GetComponent<FadeBack>();
            minionsSoaked = 0;
        }

        ///<summary>
        ///  Called when object is created, after Awake.
        ///</summary>
        void Start ()
        {
            sumImpact = Vector2.zero;
            sumPosition = Vector2.zero;   
        }

        ///<summary>
        ///  Update function used for physics simulation.  This update function
        ///  is called at a fixed time interval.  This class uses this funciton
        ///  to compute if it has been hit hard enough to damage the parent
        ///  entity.
        ///</summary>
        public void FixedUpdate()
        {
            if(sumImpact.magnitude >= minImpactTillDamage && sumImpact.sqrMagnitude > 0f)
            {
                float impact = Mathf.Min(sumImpact.magnitude, maxImpactReceived);
                float damage = Math2d.ConvertToRange(minImpactTillDamage, maxImpactReceived, baseDamageReceived, maxDamageReceived, impact);
                if(damage > 0f)
                {
                    if(damage > largeDamageAmount)
                    {
                        //Play sound here.
                        if(soundTimer <= 0.0f)
                        {
                            soundTimer = durationDamageSound;
                            sound.clip = damageSoundClip;
                            sound.Play();
                        }
                        
                        GameObject effect = (GameObject)GameObject.Instantiate(damageFeedback, sumPosition/minionsSoaked, Quaternion.identity);
                        effect.transform.right = -sumImpact.normalized;
                    }
                    damageFader.BeginFade();

                    if(entityToDamage.LoseHealth(damage))
                    {
                        //Activate corresponding objects in list
                        if (objectsToEnableOnDestruction.Length > 0)
                        {
                            foreach (GameObject item in objectsToEnableOnDestruction)
                            {
                                if (item != null)
                                    item.SetActive(true);
                                    
                            }
                        }
                        if(destroyControllerOnLosingAllHealth)
                        {
                            Destroy(this.gameObject);
                        }
                    }
                }
            }
            sumImpact = Vector2.zero;
            sumPosition = Vector2.zero;
            minionsSoaked = 0;

            //sound duration for damage.
            soundTimer -= Time.deltaTime;
            if(soundTimer <= 0.0f)
            {
                sound.Stop();
            }
        }

        ///<summary>
        ///  Called whenever a collision happens with this object.  This is
        ///  used to compute the amount of impact an object  has collided with
        ///  us.
        ///</summary>
        public void OnCollisionEnter2D(Collision2D col)
        {
            if (IsCollisionCollectedMinion(col.gameObject.GetComponent<Minion>()))
            {
                sumImpact += Physics2d.CalculateImpact(col);
                sumPosition += (Vector2)col.transform.position;
                minionsSoaked++;
            }
        }

        ///<summary>
        ///  Called whenever a trigger happens with this object.  This is
        ///  similar to the OnCollisionEnter2D function except it ignores the
        ///  angle the collision came from since we do not have this
        ///  information.
        ///</summary>
        public void OnTriggerEnter2D(Collider2D col)
        {
            if(IsCollisionCollectedMinion(col.gameObject.GetComponent<Minion>()))
            {
                sumImpact += Physics2d.CalculateImpact(col);
                sumPosition += (Vector2)col.transform.position;
                minionsSoaked++;
            }
        }

        /// <summary>
        /// Determines if the collision is an collected minion.
        /// </summary>
        /// <param name="minion">The minion (can be null).</param>
        /// <returns>If the collision is an uncollected minion.</returns>
        private bool IsCollisionCollectedMinion(Minion minion)
        {
            return (minion != null && minion.isCollected);
        }
    }
}
