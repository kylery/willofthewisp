using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Assets.Scripts.Entities.Player;
using Assets.Scripts.Utils;
using Assets.Scripts.Environment;
using Assets.Scripts.Environment.Pathing;

namespace Assets.Scripts.Entities.Swarm
{
    /// <summary>
    /// The Minion Manager manages all the minions in the swarm by handling its resources when a minion is
    /// created or destroyed. It also handles all state changes of the swarm and updates each one accordingly
    /// and makes sure that they update in the correct way.
    /// </summary>
    public sealed class MinionManager : MonoBehaviour
    {
        private static MinionManager instance;

        public int maxCapacity = 100;

        /// <summary>
        /// The global instance of the Minion Manager.
        /// </summary>
        public static MinionManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = GameObject.Find("Minion_Manager").GetComponent<MinionManager>();
                    if (instance == null)
                    {
                        UnityEngine.Debug.LogError("Minion Manager does not exist");
                    }
                }
                return instance;
            }
        }

        [SerializeField]
        private float gravitationalConstant;
        [SerializeField]
        private int startNumMinions;
        [SerializeField]
        private float maxDistFromPlayer;
        [SerializeField]
        private GameObject minionPrefab;

        private List<Minion> minionList;
        private List<Minion> uncollectedMinions;
        private KDTree uncollectedMinionsKDTree;
        private GameObject player;
        private Vector2 offset;

        /// <summary>
        /// The Minion Manager will hold the gravitational constant
        /// of the entire game that affects how massive a force will
        /// be between two objects.
        /// 
        /// TODO: This may be better in the Game Manager.
        /// </summary>
        public float gravity
        {
            get { return gravitationalConstant; }
        }

        /// <summary>
        /// The average force of the swarm. A more uniform motion will yield a
        /// bigger force while movement in all directions will resutl in a force
        /// that is closer to zero.
        /// </summary>
        public Vector2 avgForce
        {
            get;
            private set;
        }

        /// <summary>
        /// The geometric mean of the swarm.
        /// </summary>
        public Vector2 center
        {
            get;
            private set;
        }

        /// <summary>
        /// The number of minions in the swarm.
        /// </summary>
        public int swarmSize
        {
            get
            {
                return minionList.Count - uncollectedMinions.Count;
            }
        }

		/// <summary>
		/// The number of uncollected minions.
		/// </summary>
		public int uncollectedSwarmSize
		{
			get
			{
				return uncollectedMinions.Count;
			}
		}

        /// <summary>
        /// The list of positions of each minion in the swarm.
        /// </summary>
        public List<Vector2> swarmPositions
        {
            get
            {
                var list = from minion in minionList where !uncollectedMinions.Contains(minion) select (Vector2)minion.transform.position;
                return new List<Vector2>(list);
            }
        }

        /// <summary>
        /// A KD Tree that stores the spacial partition 
        /// </summary>
        public KDTree uncollectedMinionsKD
        {
            get { return uncollectedMinionsKDTree; }
        }

        /// <summary>
        /// The gravity offset vector set by the right stick.
        /// </summary>
        public Vector2 gravityOffset
        {
            get { return offset; }
        }

        /// <summary>
        /// This will initialize the manager's properties before it
        /// is called by any other object.
        /// </summary>
        void Awake()
        {
            minionList = new List<Minion>();
            uncollectedMinions = new List<Minion>();
            uncollectedMinionsKDTree = new KDTree();
            avgForce = Vector2.zero;
        }

        /// <summary>
        /// This will create the starting number of minions in the swarm.
        /// </summary>
        void Start()
        {
            player = PlayerManager.Instance.Player;
            for (int i = 0; i < startNumMinions; i++)
            {
                CreateMinion(UnityEngine.Random.value * maxDistFromPlayer);
            }
        }

        /// <summary>
        /// This will update the average force and the geometric
        /// mean of the swarm.
        /// </summary>
        void FixedUpdate()
        {
            Vector2 sumForce = Vector2.zero;
            var list = from minion in minionList select minion.gameObject;
            center = Math2d.GeometricMean(list.ToList());

            foreach (Minion minion in minionList)
            {
                sumForce += (minion.attributes.trajectory / minionList.Count);
            }
            avgForce = sumForce;
        }

        /// <summary>
        /// This will update the KD Tree for the uncollected minions each frame.
        /// </summary>
        void Update()
        {
            UpdateGravityOffset();

            if (uncollectedMinions.Count > 0)
            {
                var elements = from minion in uncollectedMinions select minion.gameObject;
                uncollectedMinionsKDTree.RebuildTree(elements.ToList());
            }
        }

        /// <summary>
        /// This will update the gravity offset set by the right stick so that
        /// the value doesn't have to be requested from the Input Manager by
        /// every minion each frame.
        /// </summary>
        private void UpdateGravityOffset()
        {
            int up;
            int right;

            if (Input.GetKey(KeyCode.UpArrow))
                up = 1;
            else if (Input.GetKey(KeyCode.DownArrow))
                up = -1;
            else
                up = 0;
            if (Input.GetKey(KeyCode.RightArrow))
                right = 1;
            else if (Input.GetKey(KeyCode.LeftArrow))
                right = -1;
            else
                right = 0;

            if (up == 0 && right == 0)
            {
                offset = InputManager.Instance.GetRightJoystickVector();
            }
            else
            {
                offset.x = right;
                offset.y = up;
            }
        }

        /// <summary>
        /// Enable will enable the swarm to update each frame.
        /// </summary>
        public void Enable()
        {
            foreach (Minion minion in minionList)
            {
                if (minion == null)
                    continue;
                minion.Enable();
                minion.GetComponent<SpriteRenderer>().sortingLayerID = 1;
            }
            enabled = true;
        }

        /// <summary>
        /// Disable will stop the swarm from updating each frame.
        /// </summary>
        public void Disable()
        {
            foreach (Minion minion in minionList)
            {
                if (minion == null)
                    continue;
                minion.Disable();
                minion.GetComponent<SpriteRenderer>().sortingLayerID = 0;
            }
            enabled = false;
        }

        /// <summary>
        /// This will bring the swarm from rendering in the foreground to the default
        /// layer.
        /// </summary>
        public void MoveSwarmBack()
        {
            foreach(Minion minion in minionList)
                minion.GetComponentInChildren<SpriteRenderer>().sortingLayerID = 0;
        }

        /// <summary>
        /// This will bring the swarm from the default layer to the foreground rendering layer.
        /// </summary>
        public void MoveSwarmForward()
        {
            foreach (Minion minion in minionList)
                minion.GetComponentInChildren<SpriteRenderer>().sortingLayerID = 1;
        }

        /// <summary>
        /// Dampen Swarm will change the speed of the swarm when the mass
        /// is affected.
        /// </summary>
        /// <param name="speed">The speed to change the swarm to.</param>
        public void DampenSwarm(float speed)
        {
            foreach (Minion minion in minionList)
            {
                if (minion.isCollected)
                {
                    if (minion == null)
                        continue;
                    minion.ChangeSpeed(speed);
                }
            }
        }

        /// <summary>
        /// This will remove a collected minion from the uncollected list.
        /// </summary>
        /// <param name="minion"></param>
        public void CollectMinion(Minion minion)
        {
            uncollectedMinions.Remove(minion);
        }

        /// <summary>
        /// This will remove a minion from the minionList when it has
        /// died.
        /// </summary>
        /// <param name="minion">The minion to remove.</param>
        public void RemoveMinion(Minion minion)
        {
            minionList.Remove(minion);
            if (!minion.isCollected)
                uncollectedMinions.Remove(minion);
            GameObject.Destroy(minion.gameObject);
        }

        /// <summary>
        /// This will remove all minions from the Game.
        /// Be careful, since killing all minions will result
        /// in a game over.
        /// </summary>
        public void RemoveAll()
        {
            for (int index = minionList.Count - 1; index >= 0; index--)
            {
                Minion minion = minionList[index];
                RemoveMinion(minion);
            }
        }

        /// <summary>
        /// Reposition each minion to a random radius around the player. Used when the
        /// player's position has to suddenly change.
        /// </summary>
        public void RepositionSwarm(float maxRadius)
        {
            foreach (Minion minion in minionList)
            {
                float radius = UnityEngine.Random.value * maxRadius;
                Vector2 newPosition = (Vector2)player.transform.position + (UnityEngine.Random.insideUnitCircle * radius);
                minion.transform.position = newPosition;
            }
        }

        /// <summary>
        /// This will create a minion that is uncollected by the player.
        /// </summary>
        /// <param name="position">The position where the minion will start at.</param>
        public void CreateUncollectedMinion(Vector2 position, LinkedList<GameObject> path, UncollectedSwarmSpawner.Pathing howToPath, float speed, bool invulnerable)
        {
            if (minionList.Count < maxCapacity)
            {
                GameObject minion = (GameObject)Instantiate(minionPrefab, position, Quaternion.identity);
                minionList.Add(minion.GetComponent<Minion>());
                uncollectedMinions.Add(minion.GetComponent<Minion>());
                minion.GetComponent<Minion>().SetUncollected(path, howToPath, speed, invulnerable);
            }
        }

        /// <summary>
        /// This will create a minion at an exact position in the game.
        /// </summary>
        /// <param name="position">Position to create the minion.</param>
        public void CreateMinion(Vector2 position)
        {
            if(minionList.Count < maxCapacity)
            {
                GameObject minion = (GameObject)Instantiate(minionPrefab, position, Quaternion.identity);

                Transform ThreeDCollider = minion.transform.Find("Collider");
                if (ThreeDCollider != null)
                    ThreeDCollider.gameObject.SetActive(true);

                minionList.Add(minion.GetComponent<Minion>());
            }
        }

        /// <summary>
        /// This will randomly place a minion at a certain distance from the player.
        /// </summary>
        /// <param name="distanceFromPlayer">Distance to place the minion.</param>
        public void CreateMinion(float distanceFromPlayer)
        {
            if(minionList.Count <= maxCapacity)
            {
                Vector2 position = (Vector2)player.transform.position + UnityEngine.Random.insideUnitCircle * distanceFromPlayer;
                GameObject minion = (GameObject)Instantiate(minionPrefab, position, Quaternion.identity);

                Transform ThreeDCollider = minion.transform.Find("Collider");
                if (ThreeDCollider != null)
                    ThreeDCollider.gameObject.SetActive(true);

                minionList.Add(minion.GetComponent<Minion>());
            }
        }

        /// <summary>
        /// Gets the minion closest to the given object.
        /// </summary>
        /// <returns>The minion closest to object.</returns>
        /// <param name="subject">Object that the closest minion should be near</param>
        public GameObject getMinionClosestToObjectLinearSearch(GameObject subject)
        {
            //Performs linear search for the closest minion
            List<Minion> minions =  MinionManager.instance.minionList;
            if(minions == null)
            {
                return null;
            }

            int collectedLayer = LayerMask.NameToLayer("CollectedMinion");

            Minion closestMinion = minions.First<Minion>();
            float closestDistance = ((Vector2)closestMinion.transform.position -(Vector2) subject.transform.position).sqrMagnitude;
            foreach (Minion m in minions)
            {
                if(m.gameObject.layer == collectedLayer)
                { 
                    float distance =((Vector2)m.transform.position - (Vector2)subject.transform.position).sqrMagnitude; 
                    if ( distance < closestDistance)
                    {
                        closestMinion = m;
                        closestDistance = distance;
                    }
                }
            }

            return closestMinion.gameObject;
        }
    }
}
