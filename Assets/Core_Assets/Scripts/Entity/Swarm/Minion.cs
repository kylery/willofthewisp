﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System;

using Assets.Scripts.Utils;
using Assets.Scripts.Environment;
using Assets.Scripts.Environment.Pathing;
using Assets.Scripts.Entities.Player;
using Assets.Scripts.Effects;

namespace Assets.Scripts.Entities.Swarm
{
    /// <summary>
    /// The minion represents a single entity in a swarm. It's behaviour is based off their
    /// current state and will update in accordance to its neighbors to achieve a hive mind.
    /// It's default behaviour is to protect the player by swarming them in which the player
    /// can then switch their states to control them indirectly.
    /// </summary>
    public sealed class Minion : Entity
    {
        /// <summary>
        /// This will show the different states that the minion can be in.
        /// It is either collected by the player in which it will swarm around
        /// the central point. If it is uncollected, then it will wander.
        /// </summary>
        private enum MinionState
        {
            COLLECTED,
            UNCOLLECTED
        }

        [SerializeField]
        private float initialHealth;
        [SerializeField]
        private float baseOffsetForce;
        [SerializeField]
        private float maxSpeed;
        [SerializeField]
		private float distanceToBeCollected = 40f;
        [SerializeField]
        private float uncollectedScaleFactor;
        [SerializeField]
        private Gradient forceIndicator;
        [SerializeField]
        private GameObject deathParticlePrefab;
		
        private bool dampen;
        private float saveSpeed;
        private Player.Player player;
        private MinionState minionState;

        // Uncollected properties
        private UncollectedSwarmSpawner.Pathing howToPath;
        private LinkedListNode<GameObject> pathNodeToGoTowards;
        private bool goingForward;
        private bool invulnerable;

        private SpriteRenderer spriteRenderer;

        private FadeBack damageFader;

        /// <summary>
        /// This returns the "gravitational" force between the minion and
        /// the central orb.
        /// </summary>
        public Vector2 force
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns if the minion has been collected by the main swarm or not.
        /// </summary>
        public bool isCollected
        {
            get { return minionState == MinionState.COLLECTED; }
        }

        /// <summary>
        /// This will initialize the minion that doesn't depend on any other
        /// entity to be initialized.
        /// </summary>
        void Awake()
        {
            health = initialHealth;
            attributes = new MinionAttributes();
            attributes.speed = 0;
            dampen = false;
            Physics2D.IgnoreCollision(collider2D, collider2D);
            saveSpeed = 0;
            minionState = MinionState.COLLECTED;

            damageFader = GetComponent<FadeBack>();

            spriteRenderer = GetComponent<SpriteRenderer>();
            if(spriteRenderer == null)
                spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        }

        /// <summary>
        /// This initializes the minion attributes that needs the other entities to
        /// be initialized.
        /// </summary>
        void Start()
        {
            transform.parent = MinionManager.Instance.transform;
            player = PlayerManager.Instance.Player.GetComponent<Player.Player>();
        }

        /// <summary>
        /// This will update the physics and the position of the minion by recalculating the
        /// "gravitational" force between the minion and the player if it is collected by the
        /// main swarm.
        /// </summary>
        void FixedUpdate()
        {
            switch(minionState)
            {
                case MinionState.UNCOLLECTED:
                    UncollectedFixedUpdate();
                    break;
                default:
                case MinionState.COLLECTED:
                    CollectedFixedUpdate();
                    break;
            }
			if(!float.IsNaN(attributes.trajectory.x) && !float.IsNaN(attributes.trajectory.y))
            	transform.right = attributes.trajectory;
        }

        /// <summary>
        /// This will monitor the position of uncollected minions and will assign them to be collected
        /// when they are close enough to the player.
        /// </summary>
        void Update()
        {
            MinionManager minionManager = MinionManager.Instance;

            switch(minionState)
            {
                case MinionState.UNCOLLECTED:
                    if (Vector2.Distance(player.transform.position, transform.position) <= distanceToBeCollected &&
                        minionManager.swarmSize < minionManager.maxCapacity && player.gameObject.activeSelf)
                    {
                        minionState = MinionState.COLLECTED;
                        Transform ThreeDCollider = transform.Find("Collider");
                        if (ThreeDCollider != null)
                            ThreeDCollider.gameObject.SetActive(true);
                        minionManager.CollectMinion(this);
                        gameObject.layer = LayerMask.NameToLayer("CollectedMinion");
                        transform.position = player.transform.position + (Vector3)UnityEngine.Random.insideUnitCircle;
                        Vector3 position = transform.position;
                        position.z = 0;
                        transform.position = position;
						transform.localScale = new Vector3(transform.localScale.x / uncollectedScaleFactor, transform.localScale.y / uncollectedScaleFactor, transform.localScale.z);
                    }
                    break;
            }
        }

        /// <summary>
        /// When the minion collides with a wall, then it will apply a bounce-back
        /// effect by reflecting across the the wall's normal.
        /// </summary>
        void OnCollisionEnter2D(Collision2D coll)
        {
            string collTag = coll.gameObject.tag;
            if (collTag == "Minion")
                return;
                
            ContactPoint2D contactPoint = coll.contacts[0];
            float dot = Vector2.Dot(attributes.trajectory, contactPoint.normal);

            attributes.trajectory = (attributes.trajectory - 2 * dot * contactPoint.normal) * 0.8f;

            if (dot < 0)
            {
                if (collTag == "Wall" || collTag == "Tundra")
                {
                    HandleWalls(coll);
                }
            }
        }

        /// <summary>
        /// There are cases where a minion gets stuck moves a bit, but doesn't get out of the tundra
        /// or wall. The collision won't enter again since it is still colliding. This will check for
        /// stuck minions again while they continue to be stuck.
        /// </summary>
        void OnCollisionStay2D(Collision2D coll)
        {
            string collTag = coll.gameObject.tag;
            if (collTag == "Wall" || collTag == "Tundra")
            {
                HandleWalls(coll);
            }
        }

        /// <summary>
        /// If a minion is stuck in a wall or iceburg, this will move the minion
        /// out of the wall by providing a push back towards the center. This handles
        /// all cases since it is not possible to tell which side of the wall or tundra a
        /// minion is on.
        /// </summary>
        private void HandleWalls(Collision2D coll)
        {
            const float EPSILON = 10f;

            Vector2 center;
            Vector2 pushTrajectory;
            Vector2 collContactPoint = coll.contacts[0].point;
            if (coll.gameObject.tag == "Tundra")
            {
                Transform bottomTundra = GameObjectUtils.GetChild(coll.gameObject, "Bottom");
                if (bottomTundra != null)
                {
                    center = bottomTundra.position;
                    pushTrajectory = (collContactPoint - center).normalized;
                }
                else
                {
                    center = coll.collider.bounds.center;
                    pushTrajectory = -(center - collContactPoint).normalized;
                }
            }
            else
            {
                center = coll.collider.bounds.center;
                pushTrajectory = (center - collContactPoint).normalized;
            }

            gameObject.transform.position += (Vector3)(pushTrajectory * (spriteRenderer.bounds.extents.x + EPSILON));
        }

        /// <summary>
        /// All minions default to collected, this will seperate them from the main swarm.
        /// </summary>
        public void SetUncollected(LinkedList<GameObject> path, UncollectedSwarmSpawner.Pathing howToPath, float baseUncollectedMinionSpeed, bool invulnerable)
        {
            this.minionState = MinionState.UNCOLLECTED;
            this.howToPath = howToPath;
            this.goingForward = true;
            this.pathNodeToGoTowards = path.First;
            this.invulnerable = invulnerable;
            attributes.speed = baseUncollectedMinionSpeed;
			transform.localScale = new Vector3 (transform.localScale.x * uncollectedScaleFactor, transform.localScale.y * uncollectedScaleFactor, transform.localScale.z);
            gameObject.layer = LayerMask.NameToLayer("UncollectedMinion");
        }

        /// <summary>
        /// When the minion loses damage, it will start its fade by
        /// changing color.
        /// </summary>
        /// <param name="damage"></param>
        ///<returns>If the damage applied killed the entity.</returns>
        public override bool LoseHealth(float damage)
        {
            damageFader.BeginFade();

            bool hasDied = false;

            if (CanKillUncollectedMinion())
            {
                hasDied = base.LoseHealth(damage);

                if (hasDied)
                {
                    MinionManager.Instance.RemoveMinion(this);
                    GameObject.Instantiate(deathParticlePrefab, transform.position, Quaternion.identity);
                }
            }
            return hasDied;
        }

        /// <summary>
        /// Kills the minion
        /// </summary>
        public void  Kill()
        {
            LoseHealth(health);
        }

        /// <summary>
        /// This will enable the minion by setting its speed to its
        /// original speed before it was disabled.
        /// </summary>
        public void Enable()
        {
            enabled = true;
            attributes.speed = saveSpeed;
        }

        /// <summary>
        /// This will disable the minion from updating and preserve its original
        /// speed before setting it to 0.
        /// </summary>
        public void Disable()
        {
            enabled = false;
            saveSpeed = attributes.speed;
            attributes.speed = 0;
        }

        /// <summary>
        /// This will apply the dampening force on the minion in which
        /// its speed will be .1% of the original speed.
        /// </summary>
        public void Dampen()
        {
            dampen = true;
        }

        /// <summary>
        /// This will remove the dampening force on the minion.
        /// </summary>
        public void Undampen()
        {
            dampen = false;
            attributes.speed = attributes.trajectory.magnitude;
        }

        /// <summary>
        /// This will allow an outside entity to directly effect the
        /// speed of the minion.
        /// </summary>
        /// <param name="speed">The speed to change the minion to.</param>
        public void ChangeSpeed(float speed)
        {
            attributes.speed = speed;
            //maxSpeed = speed;
        }

        /// <summary>
        /// Returns true if an uncollected minion can die. If the minion is collected, then it
        /// doesn't matter if its invulenerable or not.
        /// </summary>
        public bool CanKillUncollectedMinion()
        {
            return !(minionState == MinionState.UNCOLLECTED && invulnerable);
        }

        /// <summary>
        /// When the minion is uncollected, then it will path around the outside of the 
        /// level until it is either collected or destroyed.
        /// </summary>
        private void UncollectedFixedUpdate()
        {
            const float DISTANCE_TO_SWITCH = 20f;

            GameObject pathNode = pathNodeToGoTowards.Value;
            PathNodeData pathNodeData = pathNode.GetComponent<PathNode>().info;
            if (Vector2.Distance(pathNode.transform.position, transform.position) < DISTANCE_TO_SWITCH)
            {
                GoToNextPathNode(ref pathNode);
                float changeSpeed;
                switch(pathNodeData.state)
                {
                    default:
                    case PathNodeData.PathNodeState.LINEAR:
                        changeSpeed = pathNodeData.changeSpeedRange.min;
                        break;
                    case PathNodeData.PathNodeState.RANDOM:
                        Range speedRange = pathNodeData.changeSpeedRange;
                        changeSpeed = UnityEngine.Random.Range(speedRange.min, speedRange.max);
                        break;
                }
                attributes.speed = changeSpeed;
                Vector3 position = transform.position;
                position.z = pathNodeData.targetZ;
                transform.position = position;
            }

            Vector2 primaryTrajectory = (pathNode.transform.position - transform.position);
            attributes.trajectory = (primaryTrajectory).normalized;
            
            Vector2 velocity = (attributes.speed * attributes.trajectory);
            rigidbody2D.MovePosition((Vector2)transform.position + velocity * Time.deltaTime);
        }

        /// <summary>
        /// When a minion reaches its target, this will determine which path node it needs to go towards
        /// next depending on how it should path if its a cycle or backtracking.
        /// </summary>
        /// <param name="pathNode">Current Path Node</param>
        private void GoToNextPathNode(ref GameObject pathNode)
        {
            switch (howToPath)
            {
                default:
                case UncollectedSwarmSpawner.Pathing.CYCLE:
                    pathNodeToGoTowards = pathNodeToGoTowards.NextOrFirst();
                    break;
                case UncollectedSwarmSpawner.Pathing.SINGLE:
                    LinkedListNode<GameObject> previousNode = pathNodeToGoTowards;
                    pathNodeToGoTowards = pathNodeToGoTowards.Next;
                    if (pathNodeToGoTowards == null)
                    {
                        MinionManager.Instance.RemoveMinion(this);
                        pathNodeToGoTowards = previousNode;
                    }
                    break;
                case UncollectedSwarmSpawner.Pathing.BACKTRACK:
                    if (goingForward)
                    {
                        if (pathNodeToGoTowards.Next == null)
                        {
                            goingForward = false;
                            pathNodeToGoTowards = pathNodeToGoTowards.Previous;
                        }
                        else
                        {
                            pathNodeToGoTowards = pathNodeToGoTowards.Next;
                        }
                    }
                    else
                    {
                        if (pathNodeToGoTowards.Previous == null)
                        {
                            goingForward = true;
                            pathNodeToGoTowards = pathNodeToGoTowards.Next;
                        }
                        else
                        {
                            pathNodeToGoTowards = pathNodeToGoTowards.Previous;
                        }
                    }
                    break;
            }
            
            pathNode = pathNodeToGoTowards.Value;
        }

        /// <summary>
        /// When a minion is collected, then it will swarm around the main player by using it as
        /// a gravitational center and using gravitational forces to orbit around the main player.
        /// </summary>
        private void CollectedFixedUpdate()
        {
            const float DAMPENING_FORCE = 0.001f;

            float gravityForce;
            float offsetForce;

            // calculate forces to apply.
            Vector2 gravityTrajectory = FindGravityVector(out gravityForce);
            Vector2 gravityOffset = FindGravityOffset(out offsetForce);
            force = (gravityTrajectory + gravityOffset).normalized * (gravityForce * offsetForce);

            // get new velocity from forces.
            attributes.speed += ((gravityForce + offsetForce) * Time.deltaTime);

            // add current trajectory to current force vectors.
            attributes.trajectory += (gravityTrajectory + gravityOffset);
            attributes.trajectory = attributes.trajectory.normalized;

            // Clamp attributes.speed so it doesn't go too fast.
            attributes.speed = attributes.speed > maxSpeed ? maxSpeed : attributes.speed;

            // scale the trajectory by the velocity
            attributes.trajectory *= attributes.speed;

            if (dampen)
            {
                attributes.trajectory *= DAMPENING_FORCE;
            }

            // update position based on velocity
			Vector2 new_position = (Vector2)transform.position + (Vector2)(attributes.trajectory) * Time.deltaTime;
    		
			if(!float.IsNaN (new_position.x) && !float.IsNaN(new_position.y))
				rigidbody2D.MovePosition(new_position);
        }

        /// <summary>
        /// This will return the force of gravity from the minion to the center of the
        /// player. It will return both the force of gravity and the vector of its
        /// trajectory.
        /// </summary>
        /// <param name="gravityForce">The force of gravity that will be returned to the caller.</param>
        /// <returns>The trajectory of the gravitational force.</returns>
        private Vector2 FindGravityVector(out float gravityForce)
        {
            Vector2 gravityTrajectory = (Vector2)(player.transform.position - transform.position);
            gravityForce = MinionManager.Instance.gravity * player.Mass / gravityTrajectory.sqrMagnitude;
            return gravityTrajectory;
        }

        /// <summary>
        /// The user has the option to offset the swarm by applying a force in the
        /// direction of the right joystick. This will return the force and 
        /// the trajectory of the offset.
        /// </summary>
        /// <param name="force">The force of the offset that is returned to the player.</param>
        /// <returns>The vector of the offset force.</returns>
        private Vector2 FindGravityOffset(out float force)
        {
            const float TRIGGER_EPSILON = 0.25f;

            Vector2 offset = MinionManager.Instance.gravityOffset;

            if (offset.magnitude >= TRIGGER_EPSILON)
            {
                force = baseOffsetForce;
                offset *= force;
            }
            else
            {
                offset = Vector2.zero;
                force = 0;
            }
            return offset;
        }
    }
}
