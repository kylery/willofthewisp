﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Entities.Swarm
{
    /// <summary>
    /// This class abstracts all public facing attributes of a minion.
    /// </summary>
    public class MinionAttributes : EntityAttributes
    {
        
    }
}
