﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Entities;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// This class contains some simple or game specific physics calculations
    /// that may be reused in multiple areas.
    /// </summary>
    public static class Physics2d
    {

        /// <summary>
        /// Calculates the impact of an object with information from a
        /// collision event.
        /// </summary>
        /// <param name="col">The collision event</param>
        /// <returns>The vector of impact</returns>
        public static Vector2 CalculateImpact(Collision2D col)
        {
            Vector2 glancingFactor = -Vector2.Dot(col.relativeVelocity, col.contacts[0].normal)*col.relativeVelocity.normalized;
            Vector2 impact = col.rigidbody != null ? glancingFactor*col.rigidbody.mass : Vector2.zero;
            return impact;
        }


        /// <summary>
        /// Calculates the impact of an object with information from a
        /// trigger event.
        /// </summary>
        /// <param name="col">The trigger event</param>
        /// <returns>The vector of impact</returns>
        public static Vector2 CalculateImpact(Collider2D col)
        {
            Vector2 glancingFactor = Vector2.zero;
            Entity entity = col.gameObject.GetComponent<Entity>();
            if(entity == null)
            {
                glancingFactor = col.rigidbody2D != null ? col.rigidbody2D.velocity : Vector2.zero;
            }
            else
            {
                glancingFactor = entity.attributes.trajectory;
            }
            Vector2 impact = col.rigidbody2D != null ? glancingFactor*col.rigidbody2D.mass : Vector2.zero;
            return impact;
        }
    }
}
