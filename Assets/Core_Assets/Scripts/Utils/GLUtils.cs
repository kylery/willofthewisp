﻿using UnityEngine;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// This utility class will abstract OpenGL commands to do basic drawing
    /// where Unity's OnGUI does not satisfy.
    /// </summary>
	public static class GLUtils
	{
        /// <summary>
        /// This will provide a default material for OpenGL calls.
        /// </summary>
        private static Material mat
        {
            get
            {
                var material = new Material("Shader \"Lines/Colored Blended\" {" +
                                            "SubShader { Pass { " +
                                            "    Blend SrcAlpha OneMinusSrcAlpha " +
                                            "    ZWrite Off Cull Off Fog { Mode Off } " +
                                            "    BindChannels {" +
                                            "      Bind \"vertex\", vertex Bind \"color\", color }" +
                                            "} } }");
                material.hideFlags = HideFlags.HideAndDontSave;
                material.shader.hideFlags = HideFlags.HideAndDontSave;
                return material;
            }
        }

        /// <summary>
        /// This will draw a 2D line with a default color of white.
        /// </summary>
        /// <param name="start">Start of the line.</param>
        /// <param name="end">End of the line.</param>
        public static void DrawLine(Vector2 start, Vector2 end)
        {
            DrawLine(start, end, Color.white);
        }

        /// <summary>
        /// This will draw a line on the screen.
        /// </summary>
        /// <param name="start">Start of the line.</param>
        /// <param name="end">End of the line.</param>
        /// <param name="color">Color of the line.</param>
        public static void DrawLine(Vector2 start, Vector2 end, Color color)
        {
            Vector3 startVector = start;
            Vector3 endVector = end;
            startVector.z = -5;
            endVector.z = -5;

            GL.PushMatrix();
            mat.SetPass(0);
            GL.LoadOrtho();
            GL.Begin(GL.LINES);
            GL.Color(color);
            GL.Vertex(startVector);
            GL.Vertex(endVector);
            GL.End();
            GL.PopMatrix();
        }
	}
}
