﻿using System;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// For better readability than using a Vector2 or
    /// encapsulating and getting a better memory footprint.
    /// This struct will hold a range for any purpose whether
    /// it be for time, distances, etc. This is used primarily
    /// for Unity's Inspector.
    /// </summary>
    [System.Serializable]
    public struct Range
    {
        public float min;
        public float max;
    }
}
