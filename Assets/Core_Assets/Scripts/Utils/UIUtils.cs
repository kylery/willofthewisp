﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// Many UI Elements will share the same functionality. UI Utils will have all methods
    /// that helps out a UI element.
    /// </summary>
    public static class UIUtils
    {
        /// <summary>
        /// This will set the alpha for a sprite renderer.
        /// </summary>
        /// <param name="sprite">The sprite to apply the changes.</param>
        /// <param name="alpha">The alpha to set the sprite to.</param>
        public static void SetSpriteAlpha(SpriteRenderer sprite, float alpha)
        {
            Color spriteColor = sprite.color;
            spriteColor.a = alpha;
            sprite.color = spriteColor;
        }

        /// <summary>
        /// This will set the alpha for a UI text.
        /// </summary>
        /// <param name="text">The text to apply the changes.</param>
        /// <param name="alpha">The alpha to set the text to.</param>
        public static void SetTextAlpha(UnityEngine.UI.Text text, float alpha)
        {
            Color textColor = text.color;
            textColor.a = alpha;
            text.color = textColor;
        }
    }
}
