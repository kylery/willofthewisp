using System;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// Unity's built-in input manager does not account for differences in OS and the button mapping
    /// for Windows and Mac have some overlap for the Xbox controller. Instead of relying on the user
    /// to change their mapping to account for the different OS, this class will be used as a central
    /// point for the game to request if a button is down or up. It will then determine the appropriate
    /// settings of the user's control and OS setting to get the right state from Unity. This will also
    /// map the keyboard to the Xbox control so that if the user presses a key it can be properly mapped
    /// the corresponding xbox button. Different support can be implemented later for different kinds of
    /// controllers such as Playstation, Logitech, or any other.
    /// </summary>
	public class InputManager
	{
        private static readonly float TRIGGER_EPSILON = 0.1f;

        private static InputManager instance;

        public static InputManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new InputManager();
                return instance;
            }
        }

        /// <summary>
        /// All the different button types the the user can
        /// use with the xbox controller.
        /// </summary>
        public enum ButtonTypes
        {
            A,
            A_0,
            A_1,
            B,
			B_0,
			B_1,
            Y,
            X,
            D_UP,
            D_RIGHT,
            D_LEFT,
            D_DOWN,
            RIGHT_JOYSTICK_BUTTON,
            LEFT_JOYSTICK_BUTTON,
            RIGHT_BUMPER,
            LEFT_BUMPER,
            START,
            BACK
        }

        private Dictionary<ButtonTypes, string> windowsButtonMapping;
        private Dictionary<ButtonTypes, string> macButtonMapping;
        //private Dictionary<ButtonTypes, KeyCode> keyboardButtonMapping;

        /// <summary>
        /// This will initialize the mapping from the Xbox controller layout to
        /// its respective Unity Input property or key.
        /// </summary>
        private InputManager()
        {
            windowsButtonMapping = new Dictionary<ButtonTypes, string>();
            windowsButtonMapping.Add(ButtonTypes.X, "Joystick X");
            windowsButtonMapping.Add(ButtonTypes.Y, "Joystick Y");
            windowsButtonMapping.Add(ButtonTypes.A, "Joystick A");
            windowsButtonMapping.Add(ButtonTypes.B, "Joystick B");
            windowsButtonMapping.Add(ButtonTypes.D_DOWN, "D-Pad Down");
            windowsButtonMapping.Add(ButtonTypes.D_LEFT, "D-Pad Left");
            windowsButtonMapping.Add(ButtonTypes.D_RIGHT, "D-Pad Right");
            windowsButtonMapping.Add(ButtonTypes.D_UP, "D-Pad Up");
            windowsButtonMapping.Add(ButtonTypes.LEFT_JOYSTICK_BUTTON, "Left Joystick Button");
            windowsButtonMapping.Add(ButtonTypes.RIGHT_JOYSTICK_BUTTON, "Right Joystick Button");
            windowsButtonMapping.Add(ButtonTypes.RIGHT_BUMPER, "Right Bumper");
            windowsButtonMapping.Add(ButtonTypes.LEFT_BUMPER, "Left Bumper");
            windowsButtonMapping.Add(ButtonTypes.START, "Start");
            windowsButtonMapping.Add(ButtonTypes.BACK, "Back");

            macButtonMapping = new Dictionary<ButtonTypes, string>();
            macButtonMapping.Add(ButtonTypes.X, "Joystick X Mac");
            macButtonMapping.Add(ButtonTypes.Y, "Joystick Y Mac");
            macButtonMapping.Add(ButtonTypes.A, "Joystick A Mac");
            macButtonMapping.Add(ButtonTypes.B, "Joystick B Mac");
            macButtonMapping.Add(ButtonTypes.D_DOWN, "D-Pad Down Mac");
            macButtonMapping.Add(ButtonTypes.D_LEFT, "D-Pad Left Mac");
            macButtonMapping.Add(ButtonTypes.D_RIGHT, "D-Pad Right Mac");
            macButtonMapping.Add(ButtonTypes.D_UP, "D-Pad Up Mac");
            macButtonMapping.Add(ButtonTypes.LEFT_JOYSTICK_BUTTON, "Left Joystick Button Mac");
            macButtonMapping.Add(ButtonTypes.RIGHT_JOYSTICK_BUTTON, "Right Joystick Button Mac");
            macButtonMapping.Add(ButtonTypes.RIGHT_BUMPER, "Right Bumper Mac");
            macButtonMapping.Add(ButtonTypes.LEFT_BUMPER, "Left Bumper Mac");
            macButtonMapping.Add(ButtonTypes.START, "Start Mac");
            macButtonMapping.Add(ButtonTypes.BACK, "Back Mac");

            /*keyboardButtonMapping = new Dictionary<ButtonTypes, KeyCode>();
            keyboardButtonMapping.Add(ButtonTypes.X, KeyCode.Z);
            keyboardButtonMapping.Add(ButtonTypes.Y, KeyCode.X);
            keyboardButtonMapping.Add(ButtonTypes.A, KeyCode.C);
            keyboardButtonMapping.Add(ButtonTypes.A_0, KeyCode.Return);
            keyboardButtonMapping.Add(ButtonTypes.A_1, KeyCode.KeypadEnter);
            keyboardButtonMapping.Add(ButtonTypes.B, KeyCode.V);
            keyboardButtonMapping.Add(ButtonTypes.B_0, KeyCode.B);
            keyboardButtonMapping.Add(ButtonTypes.B_1, KeyCode.Delete);
            keyboardButtonMapping.Add(ButtonTypes.D_DOWN, KeyCode.K);
            keyboardButtonMapping.Add(ButtonTypes.D_LEFT, KeyCode.J);
            keyboardButtonMapping.Add(ButtonTypes.D_RIGHT, KeyCode.L);
            keyboardButtonMapping.Add(ButtonTypes.D_UP, KeyCode.I);
            keyboardButtonMapping.Add(ButtonTypes.LEFT_JOYSTICK_BUTTON, KeyCode.E);
            keyboardButtonMapping.Add(ButtonTypes.RIGHT_JOYSTICK_BUTTON, KeyCode.R);
            keyboardButtonMapping.Add(ButtonTypes.RIGHT_BUMPER, KeyCode.G);
            keyboardButtonMapping.Add(ButtonTypes.LEFT_BUMPER, KeyCode.F);
            keyboardButtonMapping.Add(ButtonTypes.START, KeyCode.Escape);
            keyboardButtonMapping.Add(ButtonTypes.BACK, KeyCode.Delete);*/
        }

        /// <summary>
        /// This will get the vector of the left joystick or by the combination of WASD
        /// keyboard control. Even though the joystick will return a normal vector, that
        /// is not the case with the keyboard. It will return the normalized vector.
        /// </summary>
        /// <returns>The normalized vector of the left joystick.</returns>
        public Vector2 GetLeftJoystickVector()
        {
            var vec = new Vector2(Input.GetAxisRaw("Left Joystick Horizontal"), Input.GetAxisRaw("Left Joystick Vertical"));
            if (vec.magnitude > TRIGGER_EPSILON)
                vec.Normalize();
            return vec;
        }

        /// <summary>
        /// This will get the vector of the right joystick or by the combination of the keyboard arrows
        /// keyboard control. Even though the joystick will return a normal vector, that
        /// is not the case with the keyboard. It will return the normalized vector.
        /// </summary>
        /// <returns>The normalized vector of the right joystick.</returns>
        public Vector2 GetRightJoystickVector()
        {
            var vec = new Vector2(Input.GetAxisRaw("Right Joystick Horizontal"), Input.GetAxisRaw("Right Joystick Vertical"));
            
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            vec = new Vector2(Input.GetAxisRaw("Right Joystick Horizontal Mac"), Input.GetAxisRaw("Right Joystick Vertical Mac"));
#endif

			if (vec.magnitude > TRIGGER_EPSILON)
				vec.Normalize();
            return vec;
        }

        /// <summary>
        /// This will return the axis value of the right
        /// trigger from [0, 1]. The keyboard will return
        /// a binary value of either 1 or 0 by pressing the
        /// enter or return key.
        /// </summary>
        /// <returns>The axis value between [0, 1]</returns>
        public float GetRightTrigger()
        {
            float rightTrigger = 0f;
            rightTrigger = Input.GetAxisRaw("Right Trigger");

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            rightTrigger = Input.GetAxisRaw("Right Trigger Mac");
#endif

            if (rightTrigger == 0f)
            {
                rightTrigger = Input.GetKey(KeyCode.Space) ? 1f : 0f;
            }

			if(rightTrigger < 0f)
			{
				rightTrigger += 1.0f;
			}

            return rightTrigger;
        }

        /// <summary>
        /// This will return the axis value of the left
        /// trigger from [0, 1]. The keyboard will return
        /// a binary value of either 1 or 0 by pressing the
        /// Q key.
        /// </summary>
        /// <returns>The axis value between [0, 1]</returns>
        public float GetLeftTrigger()
        {
            float leftTrigger = 0f;

            leftTrigger = Input.GetAxisRaw("Left Trigger");
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            leftTrigger = Input.GetAxisRaw("Left Trigger Mac");
#endif

            /*if (leftTrigger == 0f)
            {
                leftTrigger = Input.GetKey(KeyCode.Q) ? 1f : 0f;
            }*/

            return leftTrigger;
        }

        /// <summary>
        /// This will return if the control button is currently
        /// down by using the mapping for either OS. The D-Pad is
        /// handled differently in Windows and Mac. Windows treats
        /// them as an axis, while Mac treats them as buttons. For
        /// the game, the D-Pads will be treated as buttons so extra
        /// computation will be needed for Windows by converting the
        /// axis to a button.
        /// </summary>
        /// <param name="buttonType">The button type on the controller.</param>
        /// <returns>If the button is down or not.</returns>
        public bool GetButton(ButtonTypes buttonType)
        {
            bool isButton = false;
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            float axis;
            if (buttonType == ButtonTypes.D_UP) 
            {
                axis = Input.GetAxisRaw("D-Pad Up");
                if (axis == 1f)
                {
                    isButton = true;
                }
            }
            else if (buttonType == ButtonTypes.D_DOWN)
            {
                axis = Input.GetAxisRaw("D-Pad Up");
                if (axis == -1f)
                {
                    isButton = true;
                }
            }
            else if (buttonType == ButtonTypes.D_RIGHT)
            {
                axis = Input.GetAxisRaw("D-Pad Right");
                if (axis == 1f)
                {
                    isButton = true;
                }
            }
            else if (buttonType == ButtonTypes.D_LEFT)
            {
                axis = Input.GetAxisRaw("D-Pad Right");
                if (axis == -1f)
                {
                    isButton = true;
                }
            }
            else
            {
                isButton = Input.GetButton(windowsButtonMapping[buttonType]);
            }
#endif
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            isButton = Input.GetButton(macButtonMapping[buttonType]);
#endif

            /*if (!isButton)
            {
                isButton = Input.GetKey(keyboardButtonMapping[buttonType]);
            }*/

            return isButton;
        }

        /// <summary>
        /// This will return true if the button is currently held
        /// down for the frame it was requested. It will not return
        /// true again until the button is released and then pressed
        /// down again.
        /// 
        /// TODO: Add proper support for the D-Pad in Windows.
        /// </summary>
        /// <param name="buttonType">The button type on the controller</param>
        /// <returns>If the button is down.</returns>
        public bool GetButtonDown(ButtonTypes buttonType)
        {
            bool isButtonDown = false;
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (ButtonTypeIsDPad(buttonType))
            {
            }
            else
            {
                isButtonDown = Input.GetButtonDown(windowsButtonMapping[buttonType]);
            }
#endif
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            isButtonDown = Input.GetButtonDown(macButtonMapping[buttonType]);
#endif

            /*if (!isButtonDown)
            {
                isButtonDown = Input.GetKey(keyboardButtonMapping[buttonType]);
            }*/

            return isButtonDown;
        }

        /// <summary>
        /// This will return if the control button is currently
        /// released by using the mapping for either OS. The D-Pad is
        /// handled differently in Windows and Mac. Windows treats
        /// them as an axis, while Mac treats them as buttons. For
        /// the game, the D-Pads will be treated as buttons so extra
        /// computation will be needed for Windows by converting the
        /// axis to a button.
        /// 
        /// TODO: Add proper support for the D-Pad with Windows.
        /// </summary>
        /// <param name="buttonType">The button type on the controller.</param>
        /// <returns>If the button was currently released</returns>
        public bool GetButtonUp(ButtonTypes buttonType)
        {
            bool isButtonUp = false;

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (ButtonTypeIsDPad(buttonType))
            {
            }
            else
            {
                isButtonUp = Input.GetButtonUp(windowsButtonMapping[buttonType]);
            }
#endif
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            isButtonUp = Input.GetButtonUp(macButtonMapping[buttonType]);
#endif

            /*if (!isButtonUp)
            {
                isButtonUp = Input.GetKeyUp(keyboardButtonMapping[buttonType]);
            }*/

            return isButtonUp;
        }

        private bool ButtonTypeIsDPad(ButtonTypes buttonType)
        {
            return buttonType == ButtonTypes.D_DOWN || buttonType == ButtonTypes.D_LEFT || buttonType == ButtonTypes.D_RIGHT || buttonType == ButtonTypes.D_UP;
        }
	}
}
