﻿using System;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// Debug Utils contains methods that are used to help the programmer debug the
    /// game and check for code quality.
    /// </summary>
    public static class DebugUtils
    {
        /// <summary>
        /// Assert checks the given condition. If the condition fails,
        /// then the game will crash.
        /// </summary>
        /// <param name="condition">The condition to check.</param>
        public static void Assert(bool condition)
        {
            Assert(condition, "");
        }

        /// <summary>
        /// Assert checks the given condition. If the condition fails,
        /// then the game will crash.
        /// </summary>
        /// <param name="msg">The message to show to the programmer.</param>
        /// <param name="condition">The condition to check.</param>
        public static void Assert(bool condition, string msg)
        {
            if (!condition)
            {
                UnityEngine.Debug.Log(UnityEngine.StackTraceUtility.ExtractStackTrace());
                throw new UnityAssertException(msg);
            }
        }
    }

    /// <summary>
    /// This exception should be thrown if an assert fails.
    /// </summary>
    public class UnityAssertException : Exception
    {
        public UnityAssertException()
        {
        }

        public UnityAssertException(string message)
            : base(message)
        {
        }

        public UnityAssertException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}