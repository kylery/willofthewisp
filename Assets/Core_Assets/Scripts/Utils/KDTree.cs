﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// The KD-tree is a binary tree data structure that keeps track of the Game Object's
    /// spatial position. The tree represents a 2D spatial plane. The data structure
    /// can find the nearest game object, k-nearest game objects, and all game 
    /// objects within a range.
    /// </summary>
    public sealed class KDTree
    {
        /// <summary>
        /// The node holds the Game Object data and 
        /// </summary>
        private class Node
        {
            public GameObject item;
            public Node leftChild;
            public Node rightChild;

            /// <summary>
            /// The height of the node from its current
            /// position in the tree to the furthest leaf
            /// node.
            /// </summary>
            public int Height
            {
                get
                {
                    if (IsLeafNode())
                        return 1;
                    return 1 + (int)Math.Max(leftChild.Height, rightChild.Height);
                }
            }

            /// <summary>
            /// A node is a leaf if it has no children.
            /// </summary>
            /// <returns>True if it is a leaf node</returns>
            public bool IsLeafNode()
            {
                return leftChild == null && rightChild == null;
            }
        }

        private static readonly int NUM_AXIS = 2;

        private Node root;
        private int size;

        /// <summary>
        /// Number of Game Objects in the tree.
        /// </summary>
        public int Count
        {
            get { return this.size; }
        }

        /// <summary>
        /// Create an empty KD Tree.
        /// </summary>
        public KDTree()
        {
            size = 0;
            root = null;
        }

        /// <summary>
        /// This will create a balanced kd tree from a list
        /// of game objects.
        /// </summary>
        /// <param name="elements"></param>
        public KDTree(List<GameObject> elements)
        {
            size = 0;
            root = null;
            CreateTree(elements);
        }

        /// <summary>
        /// Rebuild the KD Tree with an updated list of game objects.
        /// </summary>
        /// <param name="elements">Game Objects to rebuild the tree.</param>
        public void RebuildTree(List<GameObject> elements)
        {
            size = 0;
            root = null;
            CreateTree(elements);
        }

        /// <summary>
        /// This will convert the tree into a List.
        /// </summary>
        /// <returns>Each element of the tree in a List</returns>
        public List<GameObject> ToList()
        {
            var list = new List<GameObject>();
            var queue = new Queue<Node>();

            queue.Enqueue(root);
            while (queue.Count > 0)
            {
                Node node = queue.Dequeue();
                list.Add(node.item);
                if (node.rightChild != null)
                    queue.Enqueue(node.rightChild);
                if (node.leftChild != null)
                    queue.Enqueue(node.leftChild);
            }

            return list;
        }

        /// <summary>
        /// This will add the game object to the tree.
        /// </summary>
        /// <param name="item">The game object to add</param>
        public void Add(GameObject item)
        {
            var node = new Node();
            node.item = item;
            if (size == 0)
                root = node;
            else
                Add(node, root, 0);
            size++;
        }

        /// <summary>
        /// This will recurse down the tree comparing x or y axis of game object.
        /// If the game objects x or y position is less than the game object it 
        /// is being compared to, then it will go on the left side of the sub-tree,
        /// otherwise the right side. It will continue to do this until it reaches
        /// a leaf state. 
        /// </summary>
        /// <param name="node">The game object to add to the tree</param>
        /// <param name="curNode">The current node that </param>
        /// <param name="axis">Compare the 'x' (0) or 'y' (1) axis</param>
        private void Add(Node node, Node curNode, int axis)
        {
            if (curNode.IsLeafNode())
            {
                if (node.item.transform.position[axis] < curNode.item.transform.position[axis])
                    curNode.leftChild = node;
                else
                    curNode.rightChild = node;
                return;
            }

            if (node.item.transform.position[axis] < curNode.item.transform.position[axis])
            {
                if (curNode.leftChild == null)
                {
                    curNode.leftChild = node;
                    return;
                }
                Add(node, curNode.leftChild, (axis + 1) % NUM_AXIS);
            }
            else
            {
                if (curNode.rightChild == null)
                {
                    curNode.rightChild = node;
                    return;
                }
                Add(node, curNode.rightChild, (axis + 1) % NUM_AXIS);
            }
        }

        /// <summary>
        /// This will remove the game object from the tree.
        /// </summary>
        /// <param name="item">The game object to remove.</param>
        public void Remove(GameObject item)
        {
            List<GameObject> list = ToList();
            list.Remove(item);
            CreateTree(list);
        }

        /// <summary>
        /// Given a range and a position, this will return a list of all
        /// game objects that fall within the given range.
        /// </summary>
        /// <param name="position">The position to check the range.</param>
        /// <param name="range">The range from the position to check.</param>
        /// <returns></returns>
        public List<GameObject> RangeSearch(Vector3 position, float range, GameObject exclude)
        {
            var nearestObjects = new List<GameObject>();
            var queue = new Queue<Node>();

            if (root != null)
            {
                queue.Enqueue(root);
                while (queue.Count > 0)
                {
                    Node node = queue.Dequeue();
                    float distance = Vector3.Distance(node.item.transform.position, position);

                    if (distance <= range)
                    {
                        if(node.item != exclude)
                            nearestObjects.Add(node.item);
                        if (node.leftChild != null)
                            queue.Enqueue(node.leftChild);
                        if (node.rightChild != null)
                            queue.Enqueue(node.rightChild);
                    }
                    else
                    {
                        if (node.leftChild != null)
                            queue.Enqueue(node.leftChild);
                        if (node.rightChild != null)
                            queue.Enqueue(node.rightChild);
                    }
                }
            }
            return nearestObjects;
        }

        public GameObject NearestNeighbor(GameObject gameObject)
        {
            return NearestNeighbor(gameObject.transform.position, gameObject);
        }

        /// <summary>
        /// This will return the game object that is closest to the
        /// given position.
        /// </summary>
        /// <param name="position">The position to find the nearest game object.</param>
        /// <returns>The nearest game object.</returns>
        public GameObject NearestNeighbor(Vector3 position, GameObject exclude = null)
        {
            if (size == 0)
                return null;
            return NearestNeighbor(position, root, 0, exclude).item;
        }

        /// <summary>
        /// This algorithm searches for the nearest game object to the position by
        /// recursing down the tree by comparing axis position. When it hits the leaf
        /// node, it sets that node to the current best. It will then traverse back up
        /// the tree and check to see if the node in the other sub-tree is closer to the
        /// position than the current best object. If it is, then it will search the sub-
        /// tree for the new best closest.
        /// </summary>
        /// <param name="position">The position to find the closest game object to.</param>
        /// <param name="node">The current node that is being examined.</param>
        /// <param name="axis">The axis to check (x = 0, y = 1)</param>
        /// <returns>The closest game object.</returns>
        private Node NearestNeighbor(Vector3 position, Node node, int axis, GameObject exclude)
        {
            Node nextNode;
            Node curBestNode;
            Node otherNode;

            if (node.IsLeafNode())
                return node;

            if (position[axis] < node.item.transform.position[axis])
            {
                if (node.leftChild == null)
                    return node;
                nextNode = node.leftChild;
                otherNode = node.rightChild;
            }
            else
            {
                if (node.rightChild == null)
                    return node;
                nextNode = node.rightChild;
                otherNode = node.leftChild;
            }

            curBestNode = NearestNeighbor(position, nextNode, (axis + 1) % NUM_AXIS, exclude);

            if (node.item != exclude && Vector3.SqrMagnitude(node.item.transform.position - position) < Vector3.SqrMagnitude(curBestNode.item.transform.position - position))
                curBestNode = node;

            if (otherNode != null && Vector3.SqrMagnitude(otherNode.item.transform.position - node.item.transform.position) < Vector3.SqrMagnitude(curBestNode.item.transform.position - position))
                curBestNode = NearestNeighbor(position, otherNode, (axis + 1) % NUM_AXIS, exclude);

            return curBestNode;
        }

        /// <summary>
        /// Given a List of game objects, this will initialize the KD Tree.
        /// </summary>
        /// <param name="elements">A List of game objects</param>
        private void CreateTree(List<GameObject> elements)
        {
            size = 0;
            int axis = 0;
            while (elements.Count > 0)
            {
                GameObject median = Median(elements, axis);
                elements = CopyListWithout(elements, median);
                Add(median);
                axis = (axis + 1) % NUM_AXIS;
            }
        }

        /// <summary>
        /// This will do a shallow copy of the list without the specified object.
        /// </summary>
        /// <param name="elements">The list to copy.</param>
        /// <param name="exception">The element to leave out.</param>
        /// <returns>The list without the specified game object.</returns>
        private List<GameObject> CopyListWithout(List<GameObject> elements, GameObject exception)
        {
            var copy = new List<GameObject>();

            foreach (GameObject item in elements)
            {
                if (item != exception)
                    copy.Add(item);
            }

            return copy;
        }

        /// <summary>
        /// Given a list of game objects, this will return the median game object
        /// based on axis (x = 0, y = 1).
        /// </summary>
        /// <param name="elements">List of game objects</param>
        /// <param name="axis">axis to compare median.</param>
        /// <returns>The game object that is median.</returns>
        private GameObject Median(List<GameObject> elements, int axis)
        {
            elements = SortBasedOnAxis(elements, axis);
            return elements[elements.Count / 2];
        }

        /// <summary>
        /// This sorts a list of game objects based on the position on a
        /// given axis (x = 0, y = 1).
        /// </summary>
        /// <param name="elements">A list of game objects</param>
        /// <param name="axis">The axis to compare the position.</param>
        /// <returns>The sorted list.</returns>
        private List<GameObject> SortBasedOnAxis(List<GameObject> elements, int axis)
        {
            if (elements.Count <= 1)
                return elements;
            int pivot = UnityEngine.Random.Range(0, elements.Count);
            var less = new List<GameObject>();
            var greater = new List<GameObject>();

            foreach (GameObject element in elements)
            {
                if (element == elements[pivot])
                    continue;
                else if (element.transform.position[axis] < elements[pivot].transform.position[axis])
                    less.Add(element);
                else
                    less.Add(element);
            }

            less = SortBasedOnAxis(less, axis);
            greater = SortBasedOnAxis(greater, axis);

            int elementsIndex = 0;
            for (int index = 0; index < less.Count; index++)
            {
                elements[elementsIndex] = less[index];
                elementsIndex++;
            }
            elementsIndex++;
            for (int index = 0; index < greater.Count; index++)
            {
                elements[elementsIndex] = greater[index];
                elementsIndex++;
            }

            return elements;
        }
    }
}