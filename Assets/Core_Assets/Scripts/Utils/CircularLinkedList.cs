﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// The Circular Linked List is an extension off of C#'s linked list so that if the
    /// programmer wants to go to the first or last node once it has reached the end or
    /// beginning it can do so without creating a new class.
    /// </summary>
    public static class CircularLinkedList
    {
        /// <summary>
        /// This will return the next node in the list, if it has reached the end of the list
        /// then next will be the beginning of the list.
        /// </summary>
        /// <param name="current">The current node.</param>
        /// <returns>The next node.</returns>
        public static LinkedListNode<T> NextOrFirst<T>(this LinkedListNode<T> current)
        {
            return current.Next ?? current.List.First;
        }

        /// <summary>
        /// This will return the previous node in teh list, if it has reached the beginning
        /// of the list, then it will return the last node in the list.
        /// </summary>
        /// <param name="current">The current node.</param>
        /// <returns>The previous node.</returns>
        public static LinkedListNode<T> PrevOrLast<T>(this LinkedListNode<T> current)
        {
            return current.Previous ?? current.List.Last;
        }
    }
}
