﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Utils
{
    /// <summary>
    /// GameObject doesn't have some helper methods that would make development time faster. Any helper function
    /// that effects the game object and can be used in different spots will go here. 
    /// </summary>
    public static class GameObjectUtils
    {
        /// <summary>
        /// Gets the child transform of the parent. If the child doesn't exist,
        /// then it will return null.
        /// </summary>
        public static Transform GetChild(GameObject parent, string childName)
        {
            childName = childName.ToLower();
            Transform child = null;

            foreach (Transform ch in parent.transform)
            {
                if (ch.name.ToLower() == childName)
                    child = ch;
            }

            return child;
        }


        /// <summary>
        /// Returns an array of all children that match the name
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="childName"></param>
        /// <returns>Transorm Array</returns>
        public static List<Transform> GetChildren(GameObject parent, string childName)
        {
            childName = childName.ToLower();
            List<Transform> children = new List<Transform>();

            foreach (Transform ch in parent.transform)
            {
                if (ch.name.ToLower() == childName)
                {
                    children.Add(ch);
                }
            }
            return children;
        }
    }
}
