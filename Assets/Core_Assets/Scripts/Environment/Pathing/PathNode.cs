﻿using UnityEngine;
using System;
using System.Collections.Generic;

using Assets.Scripts.Utils;

namespace Assets.Scripts.Environment.Pathing
{
    /// <summary>
    /// The public facing attributes for a path node that
    /// will tell an uncollected minion how to get to it.
    /// 
    /// A path node has two states: Linear and Random. Linear
    /// will take the min value in the changeSpeedRange and
    /// tell the minions to go that fast. Random will choose
    /// a vale at random between the two values and set the 
    /// minion speed. The minion can also move to the target
    /// z position which will allow the uncollected minions
    /// to move behind other objects.
    /// </summary>
    [System.Serializable]
    public struct PathNodeData
    {
        public enum PathNodeState
        {
            LINEAR,
            RANDOM
        }

        public PathNodeState state;
        public Range changeSpeedRange;
        public float targetZ;
    }

    /// <summary>
    /// This is used store the data for a path node.
    /// </summary>
    public class PathNode : MonoBehaviour
    {
        [SerializeField]
        private PathNodeData pathNodeData;

        /// <summary>
        /// Return the path node data struct to get
        /// the attributes for the path node.
        /// </summary>
        public PathNodeData info
        {
            get { return pathNodeData; }
        }
    }
}
