﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Assets.Scripts.Utils;
using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.LevelManager;

namespace Assets.Scripts.Environment.Pathing
{
    /// <summary>
    /// This spawner will create uncollected swarms units at its position.
    /// </summary>
    public class UncollectedSwarmSpawner : MonoBehaviour
    {
        /// <summary>
        /// There are two basic Path options for uncollected swarms:
        /// Cycle and Backtrack. Cycle will cycle through the path nodes
        /// and when it reaches the end, then goes towards teh beginning
        /// again. Backtracking will go backwards in the path once it reaches
        /// the end.
        /// </summary>
        public enum Pathing
        {
            CYCLE,
            BACKTRACK,
            SINGLE
        }
        
        [SerializeField]
        private float startSpeed;
        [SerializeField]
        private int minNumMinions;
        [SerializeField]
        private int maxNumMinions;
        [SerializeField]
        private float minDurationToSpawn;
        [SerializeField]
        private float maxDurationToSpawn;

        [SerializeField]
        private bool invulnerable;
        [SerializeField]
        private Pathing howToPath;
        [SerializeField]
        private List<GameObject> path;
        
        private float timeTilSpawn;
        private float secondsToNextSpawn;

        
        
        /// <summary>
        /// This will initialize internal timer.
        /// </summary>
        void Awake()
        {
            timeTilSpawn = 0;
            secondsToNextSpawn = GetNextTimeToSpawn();
        }

        void Start()
        {
            LevelManager.LevelManager manager = (LevelManager.LevelManager) FindObjectOfType(typeof(LevelManager.LevelManager));
            if (manager == null)
                Debug.LogError("Level Manager Does Not Exist");
        }

        /// <summary>
        /// This will generate a new batch of minions when the time has elapsed.
        /// </summary>
        void Update()
        {
            timeTilSpawn += Time.deltaTime;
            if (timeTilSpawn > secondsToNextSpawn)
            {
                timeTilSpawn = 0;
                if(LevelManager.LevelManager.Instance.CanSwarmSpawnerSpawn())
                { 
                    secondsToNextSpawn = GetNextTimeToSpawn();
                    SpawnSwarm(GetNumMinionsToSpawn());
                }
            }
        }

        /// <summary>
        /// Returns the next time to pass until spawner spawns a new
        /// swarm.
        /// </summary>
        /// <returns>Time to spawn.</returns>
        private float GetNextTimeToSpawn()
        {
            return (float)UnityEngine.Random.Range(minDurationToSpawn, maxDurationToSpawn);
        }

        /// <summary>
        /// Return the number of minions to spawn.
        /// </summary>
        /// <returns>The number of minions to spawn.</returns>
        private int GetNumMinionsToSpawn()
        {
            MinionManager minionManager = MinionManager.Instance;
            float generatedProb = UnityEngine.Random.value;
            float probabilityToSpawn = 1 - (minionManager.swarmSize / minionManager.maxCapacity);
            int minNumberToSpawn = (int)System.Math.Floor(UnityEngine.Random.value * 3);
            int numMinionsToSpawn = UnityEngine.Random.Range(minNumMinions, maxNumMinions);
            int minionsToSpawn = generatedProb < probabilityToSpawn ? numMinionsToSpawn : minNumberToSpawn;
            return minionsToSpawn;
        }

        /// <summary>
        /// This will create n number of uncollected swarms in a radius around the spawner.
        /// </summary>
        /// <param name="numSwarm">The number of swarm to spawn.</param>
        public void SpawnSwarm(int numSwarm)
        {
            MinionManager minionManager = MinionManager.Instance;
            for (int minionIndex = 0; minionIndex < numSwarm; minionIndex++)
            {
                Vector2 position = (Vector2)transform.position + Random.insideUnitCircle * 5;
                minionManager.CreateUncollectedMinion(position, new LinkedList<GameObject>(path), howToPath, startSpeed, invulnerable);
            }
        }
    }
}