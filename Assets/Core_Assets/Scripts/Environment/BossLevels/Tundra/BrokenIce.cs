﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Entities.BossLevels 
{
    public class BrokenIce : MonoBehaviour
    {
        
        //How long the broken ice stays on screen
        [SerializeField]
        private float totExist;
        private float existance;
        private SpriteRenderer iceSprite;
        
        // Use this for initialization
        void Start () 
        {
            existance = totExist;
            iceSprite = GetComponent<SpriteRenderer>();
        }
        
        // Update is called once per frame
        void Update () 
        {
            Color color = iceSprite.color;
            if (existance < 0f)
            {
                Destroy(this.gameObject); 
            }
            else 
            {
                existance -= Time.deltaTime; 
                color.a = existance/totExist;
                iceSprite.color = color;
            }
        }
    }
}
