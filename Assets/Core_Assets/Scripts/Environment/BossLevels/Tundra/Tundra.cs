﻿using UnityEngine;
using System.Collections;

using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Effects;
using Assets.Scripts.UI;
using Assets.Scripts.Utils;
using System.Collections.Generic;
using Assets.Scripts.Projectiles;

namespace Assets.Scripts.Entities.BossLevels
{
    public class Tundra : Entity
    {

        //Health of the tundra
        public float iceHealth = 300;

        //Game object of the ball used to throw at the shorca. 
        [SerializeField]
        private GameObject ballPrefab;
        [SerializeField]
        private int numBalls = 5;
        [SerializeField]
        private GameObject activatedProjectileEmitter;
        

        private IceBallManager tundraList;


        void Start()
        {
            this.attributes = new EntityAttributes();
            this.health = iceHealth;
            this.tundraList = IceBallManager.Instance;
            if (activatedProjectileEmitter != null)
                activatedProjectileEmitter.GetComponent<ProjectileEmitter>().enabled = false;
        }

        ///<summary>
        /// This function spawns ice balls from children named
        /// SpawnLoc.
        ///</summary>
        private void SpawnBalls()
        {
            List<Vector2> spawnLoc = new List<Vector2>();
            foreach (Transform trans in this.transform)
            {
                if (trans.name == "SpawnLoc")
                {
                    spawnLoc.Add(trans.position);
                }
            }

            int curSpawner = 0;
            for (int i = 0; i < numBalls; i++)
            {
                GameObject tundra = (GameObject)Instantiate(ballPrefab, spawnLoc[curSpawner++ % spawnLoc.Count], Quaternion.identity);
                tundraList.CreateIceBall(tundra);
            }
        }


        /// <summary>
        /// When the minion loses damage, it will start its fade by
        /// changing color.
        /// </summary>
        /// <param name="damage"></param>
        ///<returns>If the damage applied killed the entity.</returns>
        public override bool LoseHealth(float damage)
        {
            if (iceHealth <= damage)
            {
                SpawnBalls();
                if (activatedProjectileEmitter != null)
                    activatedProjectileEmitter.GetComponent<ProjectileEmitter>().enabled = true;
                Destroy(this.gameObject);
                return true;
            }
            return false;
        }
    }
}
