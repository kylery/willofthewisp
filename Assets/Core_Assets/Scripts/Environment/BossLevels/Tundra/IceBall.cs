﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Entities.BossLevels
{
    public sealed class IceBall : Entity
    {
        [SerializeField]
        private GameObject ballPrefab;
        [SerializeField]
        private int numPieces;
        [SerializeField]
        private float nonLethalDelay;
        private float curNonLethalDelay;
        [SerializeField]
        private GameObject bubbleBurstParticleSystem;

        /// <summary>
        /// Keeps track of whether or not the ice balls are Lethal.
        /// </summary>
        public bool Lethal 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            curNonLethalDelay = nonLethalDelay;
            Lethal = false;
            Color newColor = new Color(0.5f, 0.5f, 1f);
            gameObject.GetComponent<SpriteRenderer>().color = newColor;

            if (bubbleBurstParticleSystem != null)
                Instantiate(bubbleBurstParticleSystem, transform.position, Quaternion.identity);
        }

        void Update()
        {
            //Non Lethal Timer
            if(!Lethal)
            { 
                curNonLethalDelay -= Time.deltaTime;
                if( curNonLethalDelay <= 0)
                {
                    Lethal = true;
                    curNonLethalDelay = nonLethalDelay;
                    Color newColor = Color.white;
                    gameObject.GetComponent<SpriteRenderer>().color = newColor;
                }
            }

        }

        ///<summary>
        /// This function breaks the iceball into smaller chunks which
        /// dissapear over time.
        ///</summary>
        void Destroy()
        {
            for(int i = 0; i < numPieces; i++)
            {
                Vector2 dir = Random.insideUnitCircle;
                GameObject pieceOfIce = (GameObject)Instantiate(ballPrefab, this.transform.position, Quaternion.identity);
                pieceOfIce.rigidbody2D.velocity = dir*40;
                pieceOfIce.transform.up = dir;
            }
            Destroy(this.gameObject);
        }
    }
}
