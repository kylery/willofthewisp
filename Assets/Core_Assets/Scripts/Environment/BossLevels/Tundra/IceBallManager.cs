﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Entities.BossLevels
{
    public sealed class IceBallManager : MonoBehaviour 
    {
        private static IceBallManager instance;
		
        public int maxCapacity = 100;

        private IceBallManager tundraList;
		
        /// <summary>
        /// The global instance of the IceBall Manager.
        /// </summary>
        public static IceBallManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = GameObject.Find("IceBall_Manager").GetComponent<IceBallManager>();
                    if (instance == null)
                    {
                        UnityEngine.Debug.LogError("Ice Ball Manager does not exist");
                    }
                }
                return instance;
            }
        }

		void Start () 
		{
            this.tundraList = GameObject.FindGameObjectWithTag("IceBallManager").GetComponent<IceBallManager>();
		}

        /// <summary>
        /// Creates the ice ball and acts like a parent for clarity reasons. 
        /// </summary>
        /// <param name="tundra">Tundra.</param>
        public void CreateIceBall(GameObject tundra)
        {
            tundra.transform.parent = tundraList.transform;
        }
	}
}
