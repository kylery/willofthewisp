﻿using UnityEngine;
using System.Collections;
using System;

using Assets.Scripts.Utils;
using Assets.Scripts.Effects;
using Assets.Scripts.Environment.Pathing;
using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Entities.Player;

namespace Assets.Scripts.LevelManager
{ 
    /// <summary>
    /// Level Manager
    /// </summary>
    public abstract class LevelManager : MonoBehaviour 
    {
        protected static LevelManager instance;
        public static LevelManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (LevelManager)FindObjectOfType(typeof(LevelManager));
                    if (instance == null)
                        Debug.LogError("Level Manager does not exist");
                }
                return instance;
            }
        }
        
        [SerializeField]
        protected int maxUncollectedSwarmOnScreen;
        [SerializeField]
        protected GameObject[] corners;
        [SerializeField]
        protected GameObject levelCenter;
        [SerializeField]
        protected GameObject[] viewBoxes;

        protected bool levelBeaten = false;
        private bool frozen = false;

        [SerializeField]
        private float winDelayOnDeath;
        private float curWinDelayOnDeath;

        protected GameObject playerReticule;
        protected GameObject[] bosses;

        protected bool hasWonOnce = false;

        /// <summary>
        /// Return the gameobjects that contain the positions of each corner
        /// of the level.
        /// </summary>
        public GameObject[] Corners
        {
            get { return corners; }
        }

        /// <summary>
        /// Return the gameobject that contains the position of the center
        /// of the level.
        /// </summary>
        public GameObject CenterOfLevel
        {
            get { return levelCenter; }
        }

        /// <summary>
        /// Start the viewboxes animation that remove them from the level.
        /// </summary>
        public void MoveViewBoxes()
        {
            bool didTop = false;
            foreach (GameObject go in viewBoxes)
            {
                GameObject _go = go;
                if(_go == null)
                {
                    if(didTop)
                    {
                        _go = GameObject.Find("ViewBoxBottom");
                        if (_go == null)
                            continue;
                    }
                    else
                    {
                        _go = GameObject.Find("ViewBoxTop");
                        didTop = true;
                        if (_go == null)
                            continue;
                    }
                }
                DebugUtils.Assert(_go != null, "Viewboxes not set in Level Manager.");
                MoveViewBox viewBox = _go.GetComponent<MoveViewBox>();
                DebugUtils.Assert(viewBox != null, "View Box must have Move View Box component.");
                viewBox.StartMoving();
            }
        }

        /// <summary>
        /// Determines if Swarm Spawners can spawn based upon how many uncollected minions are on screen in comparison
        /// to the max uncollected swarm on screen
        /// </summary>
        /// <returns>true if yes, false if no</returns>
        public bool CanSwarmSpawnerSpawn()
        {
            GameObject[] minions = GameObject.FindGameObjectsWithTag("Minion");
            int count = 0;
            foreach(GameObject minion in minions)
            {
                if(minion.layer == LayerMask.NameToLayer("UncollectedMinion"))
                {
                    count++;
                }
            }

            if (count >= maxUncollectedSwarmOnScreen)
                return false;
            return true;
        }

        protected virtual void Start()
        {
            Resources.UnloadUnusedAssets();
            Resources.LoadAll("General");
            System.GC.Collect();
            playerReticule = PlayerManager.Instance.Player;
            bosses = GameObject.FindGameObjectsWithTag("Boss");
            curWinDelayOnDeath = winDelayOnDeath;
        }

        protected virtual void Update()
        {
            if (!frozen && IsBossDestroyed())
            {
                LevelWin();
            }

            if (!frozen && IsSwarmDestroyed() && !hasWonOnce)
            {
                LevelLose();
            }

            foreach (GameObject boss in bosses)
            {
                if (boss == null)
                {
                    DestroyBoss(boss);
                }
            }

        }

        /// <summary>
        /// End the level as a victory
        /// </summary>
        public virtual void LevelWin()
        {
            hasWonOnce = true;
            //Delay the winning screen if applicable
            if(!levelBeaten)
            {
                curWinDelayOnDeath -= Time.deltaTime;
                if(curWinDelayOnDeath <= 0)
                {
                    levelBeaten = true;
                    GameManager.Instance.UpdateGameBossDefeated(Application.loadedLevelName);
                }
            }
        }
        /// <summary>
        /// End the level as a failure
        /// </summary>
        public virtual void LevelLose()
        {
            if(!levelBeaten)
            {
                GameManager.Instance.GameOver();
                levelBeaten = true;
            }
        }

        public void DestroyBoss(GameObject boss)
        {
            GameObject[] newBosses = new GameObject[bosses.Length - 1];
            int newBossesIndex = 0;
            int bossesIndex = 0;

            foreach (GameObject go in bosses)
            {
                if (boss != go)
                {
                    newBosses[newBossesIndex++] = bosses[bossesIndex];
                }
                bossesIndex++;
            }

            bosses = newBosses;
        }
        
        /// <summary>
        /// Block all attempts to end the level (win or lose)
        /// </summary>
        public virtual void FreezeLevelTermination()
        {
            frozen = true;
        }
        /// <summary>
        /// Allow the level to be ended
        /// </summary>
        public virtual void UnFreezeLevelTermination()
        {
            frozen = false;
        }

        private bool IsBossDestroyed()
        {
            return bosses.Length == 0;
        }

        private bool IsSwarmDestroyed()
        {
            bool swarmDestroyed = MinionManager.Instance.swarmSize == 0;
            return swarmDestroyed;
        }
    }
}
