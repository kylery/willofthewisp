﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities.Player;
using Assets.Scripts.Projectiles;

namespace Assets.Scripts.LevelManager
{
    /// <summary>
    /// Level Manager For Turtle
    /// </summary>
    public class ShorcaLevelManager : LevelManager
    {
        protected override void Start()
        {
            base.Start();
            Resources.LoadAll("Shorca");
        }

        // Update is called once per frame
        protected override void Update()
        {
            base.Update();
        }
    }
}
