﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities.Player;
using Assets.Scripts.Projectiles;
using Assets.Scripts.Entities.Bosses;
using Assets.Scripts.Utils;

namespace Assets.Scripts.LevelManager
{ 
    /// <summary>
    /// Level Manager For Turtle
    /// </summary>
    public class TurtleLevelManager : LevelManager 
    {
        

        // For adjusting fire fall target switching delay
        [SerializeField]
        private float fireAtPlayerDelayForNotMoving;
        private float curFireAtPlayerDelayForNotMoving;
        private Vector2 savedPlayerPosition;
        private GameObject[] volcanoes;

        private Target curTarget;
        private enum Target
        {
            PLAYER,
            BOSS
        }

        void Awake()
        {
            curTarget = Target.BOSS;
            curFireAtPlayerDelayForNotMoving = fireAtPlayerDelayForNotMoving;
        }

	    // Use this for initialization
	    protected override void Start () 
        {
            base.Start();
            Resources.LoadAll("Turtle");
   
            if(playerReticule == null)
                Debug.LogError("Player object doesnt not exist");
            else
                savedPlayerPosition = (Vector2)playerReticule.transform.position;

            DebugUtils.Assert(bosses.Length > 0, "There are no bosses at the beginning of the level.");

            volcanoes = GameObject.FindGameObjectsWithTag("Volcano");
	    }
	
	    protected override void Update () 
        {
            VolcanoTargetUpdate();

            base.Update();
	    }

        /// <summary>
        /// Handles Update For Volcanoes
        /// </summary>
        private void VolcanoTargetUpdate()
        {
            //Determines target of volcanoes
            // If Boss
            if (curTarget == Target.BOSS)
            {
                //If player hasn't moved
                if (savedPlayerPosition == (Vector2)playerReticule.transform.position)
                {
                    // Reduce Timer till timer is 0, then set target of volcanoes to player
                    curFireAtPlayerDelayForNotMoving -= Time.deltaTime;
                    if (curFireAtPlayerDelayForNotMoving <= 0)
                    {
                        MakeAllVolcanoesTarget(Target.PLAYER);
                        curFireAtPlayerDelayForNotMoving = fireAtPlayerDelayForNotMoving;
                    }
                }
                else
                {
                    //Reset timer and resave player position
                    curFireAtPlayerDelayForNotMoving = fireAtPlayerDelayForNotMoving;
                    savedPlayerPosition = (Vector2)playerReticule.transform.position;
                }
            }
            // If Player
            else if (curTarget == Target.PLAYER)
            {
                // If position has changed
                if (savedPlayerPosition != (Vector2)playerReticule.transform.position)
                {
                    MakeAllVolcanoesTarget(Target.BOSS);
                }
            }
            else
            {
                Debug.LogError("Unknown Target");
            }
        }


        /// <summary>
        /// Make all volcanoes target the given target
        /// </summary>
        /// <param name="target">Target</param>
        private void MakeAllVolcanoesTarget(Target target)
        {
            //Interpret target
            GameObject objTarget;
            if (target == Target.PLAYER)
                objTarget = playerReticule;
            else
                if (bosses.Length > 0)
                    objTarget = bosses[(int)Mathf.Floor(UnityEngine.Random.value * bosses.Length)];
                else
                    return;

            // Update state
            curTarget = target;

            //Make all volcano projectiles target it
            foreach(GameObject volc in volcanoes)
            {
                ProjectileEmitter emitter = volc.GetComponent<ProjectileEmitter>();
                if(emitter != null)
                    emitter.generalTarget = objTarget;
            }
        }
    }
}
