﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts.Utils;
using Assets.Scripts.Entities.Player;
using Assets.Scripts.Cameras;
using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Effects;
using Assets.Scripts.UI;
using Assets.Scripts.Environment.BossLevels.Trophy;

namespace Assets.Scripts.LevelManager
{
    /// <summary>
    /// The struct will hold the transitional camera bounds so that
    /// when the player switches rooms, the right camera bounds will
    /// always be used for best effect.
    /// </summary>
    [Serializable]
    internal struct CameraBounds
    {
        public GameObject mainMenuCameraBound;
        public GameObject hybridCameraBound;
        public GameObject trophyCameraBound;
    }

    /// <summary>
    /// The struct will hold the transitional walls so that when
    /// the player switches rooms, the right walls will always be
    /// used for best effect.
    /// </summary>
    [Serializable]
    internal struct Walls
    {
        public GameObject mainMenuWall;
        public GameObject hybridWall;
        public GameObject trophyWall;
    }

    /// <summary>
    /// The controller will deal with transition from the main menu to the
    /// trophy room and vice versa. 
    /// </summary>
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField]
        private GameObject menuSpawn;
        [SerializeField]
        private GameObject trophySpawn;
        [SerializeField]
        private CameraBounds cameraBounds;
        [SerializeField]
        private Walls walls;

        [SerializeField]
        private GameObject turtleChallenges;
        [SerializeField]
        private GameObject shorcaChallenges;
        [SerializeField]
        private GameObject mainMenuController;

        [SerializeField]
        private List<GameObject> treasureChests;

        [SerializeField]
        private GameObject selectController;

        /// <summary>
        /// The controller can either be in the trophy room or main menu. All other
        /// events it will be transitioning between rooms.
        /// </summary>
        private enum MainMenuState
        {
            MAIN_MENU,
            TRANSITION,
            TROPHY
        }

        private MainMenuState state;
        private bool transitioningDown;
        private MinionManager minionManager;
        private GameManager gameManager;

        /// <summary>
        /// Initialize the controller.
        /// </summary>
        void Awake()
        {
            transitioningDown = true;
            state = MainMenuState.MAIN_MENU;
        }

        /// <summary>
        /// Get references from other game objects.
        /// </summary>
        void Start()
        {
            minionManager = MinionManager.Instance;
            gameManager = GameManager.Instance;

            if(gameManager.IsFirstPlayThrough)
            {
                selectController.GetComponent<SelectControllerMenuController>().ShowMenuType(MenuPane.MenuType.CONTROLLER_SELECT);
            }
        }

        /// <summary>
        /// Wait for the user to activate the transition. When the player is transitioning,
        /// the player and swarm will be teleported to the right room while the camera catches
        /// up showing a transition pan. The player won't be able to move during this time. The
        /// pan ends when the camera is close enough to the player so that the camera is completely
        /// in the new room.
        /// </summary>
        void Update()
        {
            bool panelShowing = GameObject.FindGameObjectWithTag("Panels") != null;
            if (!panelShowing)
            {
                if (InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.X) || Input.GetKeyUp(KeyCode.T))
                {
                    if (state != MainMenuState.TRANSITION)
                    {
                        TransitionPlayer();
                        PlayerManager.Instance.Player.GetComponent<Player>().FreezePlayer();
                    }
                }

                bool keyUp = Input.GetKeyUp(KeyCode.Escape) || Input.GetKeyUp(KeyCode.Backspace);
                if((InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.B) || keyUp) &&
                   !turtleChallenges.activeSelf && !shorcaChallenges.activeSelf)
                {
                    mainMenuController.GetComponent<EmbeddedMenu>().ShowMainMenu();
                }
            }
            switch (state)
            {
                case MainMenuState.TRANSITION:
                    const float MIN_DISTANCE_FROM_PLAYER = 10f;

                    float distance;
                    Action transition;
                    if (transitioningDown)
                    {
                        distance = Vector2.Distance(Camera.main.transform.position, trophySpawn.transform.position);
                        transition = new Action(ChangeToTrophy);
                    }
                    else
                    {
                        distance = Vector2.Distance(Camera.main.transform.position, menuSpawn.transform.position);
                        transition = new Action(ChangeToMainMenu);
                    }

                    if (distance <= MIN_DISTANCE_FROM_PLAYER)
                    {
                        transition();
                        PlayerManager.Instance.Player.GetComponent<Player>().UnfreezePlayer();
                    }
                    break;
            }
        }

        /// <summary>
        /// Sets the camera bound and walls for the trophy room.
        /// </summary>
        private void ChangeToTrophy()
        {
            state = MainMenuState.TROPHY;
            TurnOffAllCamerasAndWalls();
            ShowTrophy();
            Camera.main.GetComponent<Camera2DFollow>().UpdateCameraBounds();
            foreach (GameObject treasureChest in treasureChests)
            {
                treasureChest.GetComponent<TreasureChest>().Unlock();
            }
        }

        /// <summary>
        /// Sets the camera bound and walls for the main menu.
        /// </summary>
        private void ChangeToMainMenu()
        {
            state = MainMenuState.MAIN_MENU;
            TurnOffAllCamerasAndWalls();
            ShowMainMenu();
            Camera.main.GetComponent<Camera2DFollow>().UpdateCameraBounds();
            Camera.main.GetComponent<Camera2DFollow>().enabled = false;
            Camera.main.GetComponent<Camera2DFollow>().ClampCamera();
        }

        /// <summary>
        /// Determines which room to transition to towards and cleans up the
        /// necessary resources. It will also move the player and swarm to 
        /// their correct position.
        /// </summary>
        private void TransitionPlayer()
        {
            Vector3 playerPosition = PlayerManager.Instance.Player.transform.position;
            Vector3 newPosition;
            if (state == MainMenuState.MAIN_MENU)
            {
                Camera.main.GetComponent<Camera2DFollow>().enabled = true;
                transitioningDown = true;
                newPosition = trophySpawn.transform.position;
            }
            else
            {
                transitioningDown = false;
                newPosition = menuSpawn.transform.position;
            }

            newPosition.z = playerPosition.z;
            PlayerManager.Instance.Player.transform.position = newPosition;

            state = MainMenuState.TRANSITION;

            TurnOffAllCamerasAndWalls();
            ShowHybrid();
            Camera.main.GetComponent<Camera2DFollow>().UpdateCameraBounds();

            minionManager.RepositionSwarm(2);
        }

        /// <summary>
        /// Turn off all camera bounds and walls.
        /// </summary>
        private void TurnOffAllCamerasAndWalls()
        {
            cameraBounds.mainMenuCameraBound.SetActive(false);
            cameraBounds.hybridCameraBound.SetActive(false);
            cameraBounds.trophyCameraBound.SetActive(false);

            walls.mainMenuWall.SetActive(false);
            walls.hybridWall.SetActive(false);
            walls.trophyWall.SetActive(false);
        }

        /// <summary>
        /// Show only the main menu camera bounds and walls.
        /// </summary>
        private void ShowMainMenu()
        {
            cameraBounds.mainMenuCameraBound.SetActive(true);
            walls.mainMenuWall.SetActive(true);
        }

        /// <summary>
        /// Show only the camera bound and walls for the transitional state.
        /// </summary>
        private void ShowHybrid()
        {
            cameraBounds.hybridCameraBound.SetActive(true);
            walls.hybridWall.SetActive(true);
        }

        /// <summary>
        /// Show only the trophy camera bounds and walls.
        /// </summary>
        private void ShowTrophy()
        {
            cameraBounds.trophyCameraBound.SetActive(true);
            walls.trophyWall.SetActive(true);
        }
    }
}
