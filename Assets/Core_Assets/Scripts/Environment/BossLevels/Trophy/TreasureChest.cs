﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts.Effects;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Environment.BossLevels.Trophy
{
    /// <summary>
    /// The treasure chest is the spawner for the trophies that are unlocked by
    /// the player. It is generic and can be used for any of the levels.
    /// </summary>
    public class TreasureChest : MonoBehaviour
    {
        /// <summary>
        /// Each level can only be attributed to either the
        /// turtle or the orca.
        /// </summary>
        private enum Boss
        {
            TURTLE,
            ORCA
        }

        /// <summary>
        /// The Turtle and Orca have a base level and three additional challenges.
        /// This along with the Boss will attribute which level will activate the trophy.
        /// </summary>
        private enum Level
        {
            BASE,
            CHALLENGE_1,
            CHALLENGE_2,
            CHALLENGE_3
        }

        [SerializeField]
        private Boss boss;
        [SerializeField]
        private Level level;
        [SerializeField]
        private Sprite chestClosed;
        [SerializeField]
        private Sprite chestOpened;
        [SerializeField]
        private List<GameObject> trophyPrefabs;
        [SerializeField]
        private float timeBetweenDrop;
        [SerializeField]
        private GameObject notifier;
        [SerializeField]
        private float ejectVelocity;

        private float curDropTime;
        private GameManager gameManager;
        private string levelString;
        private SpriteRenderer treasureChestRenderer;
        private int trophyIndex;
        private bool unlocking;

        void Awake()
        {
            curDropTime = 0;
            levelString = GetLevelString();
            treasureChestRenderer = GetComponent<SpriteRenderer>();
            trophyIndex = 0;
            unlocking = false;
        }

        /// <summary>
        /// This will check to see if the trophy is already unlocked. If it is,
        /// then the chest will eject the troophy for the player when they come
        /// down.
        /// </summary>
        void Start()
        {
            gameManager = GameManager.Instance;

            if (gameManager.LevelBeaten(levelString))
            {
                if (!gameManager.TrophyUnlocked(levelString))
                {
                    notifier.GetComponent<ParticleNotifier>().Notify();
                }
                else
                {
                    for (int index = 0; index < trophyPrefabs.Count; index++)
                    {
                        CreateTrophy(index);
                    }
                    treasureChestRenderer.sprite = chestOpened;
                }
            }
        }

        /// <summary>
        /// When the chest is in the process of unlocking, it will eject each trophy assigned
        /// to it in set intervals until all trophies have been ejected from the chest. 
        /// </summary>
        void Update()
        {
            if (unlocking)
            {
                treasureChestRenderer.sprite = chestOpened;
                if (curDropTime > 0)
                {
                    curDropTime -= Time.deltaTime;
                }
                else
                {
                    if (trophyIndex < trophyPrefabs.Count)
                    {
                        CreateTrophy(trophyIndex);
                        trophyIndex++;
                        curDropTime = timeBetweenDrop;
                    }
                    else
                    {
                        gameManager.UnlockTrophy(levelString);
                        notifier.GetComponent<ParticleNotifier>().StopNotification();
                        unlocking = false;
                    }
                }
            }
        }

        /// <summary>
        /// This will unlock the chest only if the chest can be unlocked.
        /// </summary>
        public void Unlock()
        {
            if (!gameManager.TrophyUnlocked(levelString) && gameManager.LevelBeaten(levelString))
            {
                unlocking = true;
            }
        }

        /// <summary>
        /// This will create a trophy gameobject and set it in an upwards trajectory to emit it
        /// from the chest.
        /// </summary>
        /// <param name="index">The index in the trophy list.</param>
        private void CreateTrophy(int index)
        {
            GameObject trophy = (GameObject)GameObject.Instantiate(trophyPrefabs[index], gameObject.transform.position, Quaternion.identity);
            trophy.GetComponent<Rigidbody2D>().velocity = new Vector2(0, ejectVelocity);
        }

        /// <summary>
        /// Using both the Boss and Level type, this will return the string of the
        /// level that this chest represents.
        /// </summary>
        private string GetLevelString()
        {
            if (boss == Boss.TURTLE)
            {
                switch (level)
                {
                    default:
                        return Constants.TURTLE_BOSS_LEVEL;
                    case Level.CHALLENGE_1:
                        return Constants.TURTLE_CHALLENGE_ONE_LEVEL;
                    case Level.CHALLENGE_2:
                        return Constants.TURTLE_CHALLENGE_TWO_LEVEL;
                    case Level.CHALLENGE_3:
                        return Constants.TURTLE_CHALLENGE_THREE_LEVEL;
                }
            }
            else
            {
                switch (level)
                {
                    default:
                        return Constants.SHORCA_BOSS_LEVEL;
                    case Level.CHALLENGE_1:
                        return Constants.SHORCA_CHALLENGE_ONE_LEVEL;
                    case Level.CHALLENGE_2:
                        return Constants.SHORCA_CHALLENGE_TWO_LEVEL;
                    case Level.CHALLENGE_3:
                        return Constants.SHORCA_CHALLENGE_THREE_LEVEL;
                }
            }
        }
    }
}
