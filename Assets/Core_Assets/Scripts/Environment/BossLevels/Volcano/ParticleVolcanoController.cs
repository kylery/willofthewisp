﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities.Swarm;

public class ParticleVolcanoController : MonoBehaviour 
{
    public float duration;
    private float curDuration;

    private bool on;
    private ParticleSystem system;// system spraying the spout
    private ParticleSystem systemIndicator;//Underlying particle system for indication
    public float emissionRate;

    //Sounds for the volcano
    [SerializeField]
    private AudioClip eruptClip;
    private AudioSource sound;

	void Start () 
    {
        on = false;
        system = GetComponent<ParticleSystem>();
        systemIndicator = (ParticleSystem)GetComponentInChildren(typeof(ParticleSystem));
        system.emissionRate = 0;
        systemIndicator.Stop();
        curDuration = duration;
        sound = GetComponent<AudioSource>();
    }
	
	void Update () 
    {
        if(on)
        {
            curDuration -= Time.deltaTime;
            //Fade the song of the volcanos.
            sound.volume -= 0.0005f;
            if(curDuration <= 0)
            {
                TurnVolcanoOff();
            }
            if(sound.volume <= 0.0f)
            {
                sound.Stop();
            }
        }
	}
   
    private void TurnVolcanoOnOrProlong()
    {
        on = true;
        systemIndicator.Play();

        //Play the song of the volcanos.
        sound.clip = eruptClip;
        sound.Play();

        curDuration = duration;
        system.emissionRate = emissionRate;
    }

    private void TurnVolcanoOff()
    {
        on = false;
        systemIndicator.Stop();
        system.emissionRate = 0;
    }

    /// <summary>
    /// This will activate the volcano to fire when a charger type
    /// gameobject hits it.
    /// </summary>
    /// <param name="coll">The info of the collider.</param>
    void OnTriggerEnter2D(Collider2D coll)
    {
        Transform parent = coll.transform.parent;
        if (coll.gameObject.tag == "Charge" || (parent != null && parent.gameObject.tag == "Charge"))
        {
            if (parent != null)
                GameObject.Destroy(parent.gameObject);
            else
                GameObject.Destroy(coll.gameObject);
            TurnVolcanoOnOrProlong();
        }
    }

    /// <summary>
    /// On Particle collsiion with collected minion. kill it
    /// </summary>
    /// <param name="other"></param>
    void OnParticleCollision(GameObject other)
    {
        if (other.layer == LayerMask.NameToLayer("CollectedMinion"))
        {
            other.transform.parent.GetComponent<Minion>().Kill();
        }
    }

}
