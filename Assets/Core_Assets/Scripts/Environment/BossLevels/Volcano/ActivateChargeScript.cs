﻿using UnityEngine;
using System.Collections;
using System.Linq;

using Assets.Scripts.UI;
using Assets.Scripts.Environment;
using Assets.Scripts.Entities.Swarm;

namespace Assets.Scripts.Entities.BossLevels
{
    public sealed class ActivateChargeScript : Entity 
    {
		[SerializeField]
		private GameObject chargeDeath;
	    [SerializeField]
	    private float chargeSpeed = 1f;
        [SerializeField]
	    private Destination destinationBehavior = Destination.PROBABILISTIC_VOLCANO;
	    [SerializeField]
	    private GameObject destinationObject;
        [SerializeField]
        public float minionToHealthMultiplier = 3;
        [SerializeField]
        public int minimumChargeHealth = 10;
        [SerializeField]
        public int maximumChargeHealth = 25;
        [SerializeField]
        public float chargeRotateOffset = 2f;

	    private Rigidbody2D body;
        private GameObject targetVolcano;

		public enum Destination
		{
			CLOSEST_VOLCANO,
            RANDOM_VOLCANO,
            PROBABILISTIC_VOLCANO,
            PREDETERMINED 
		}

        void Awake()
        {
            //determine the health of the charge based on number of minions the player has
            health = MinionManager.Instance.swarmSize * minionToHealthMultiplier;

            //make sure the health isn't too large or too small based on set min and max
            health = health > maximumChargeHealth ? maximumChargeHealth : health;
            health = health < minimumChargeHealth ? minimumChargeHealth : health;

            body = GetComponent<Rigidbody2D>();

            attributes = new EntityAttributes();

            switch (destinationBehavior)
            {
                case Destination.CLOSEST_VOLCANO:
                    DetermineClosestVolcano();
                    break;
                case Destination.PROBABILISTIC_VOLCANO:
                    DetermineProbVolcano();
                    break;
                case Destination.PREDETERMINED:
                    SetDestinationObject(destinationObject);
                    break;
                default:
                case Destination.RANDOM_VOLCANO:
                    DetermineRandomVolcano();
                    break;
            }
        }

        // Use this for initialization
        void Start () 
        {
            
		}

        /// <summary>
        /// Set the object this charge should track
        /// </summary>
        /// <param name="destinationObject">The object to follow</param>
        public void SetDestinationObject(GameObject destinationObject)
        {
            targetVolcano = destinationObject;
        }
		
        /// <summary>
        /// This will move the Charge towards its destination.
        /// </summary>
        void FixedUpdate()
        {
            attributes.trajectory = (targetVolcano.transform.position - transform.position).normalized * chargeSpeed;
            body.MovePosition ((Vector2)transform.position + (attributes.trajectory * Time.deltaTime));
            rigidbody2D.MoveRotation(rigidbody2D.rotation + chargeSpeed * Time.deltaTime + chargeRotateOffset);
		}

        /// <summary>
        /// Assigns closest volcano variable
        /// </summary>
        private void DetermineClosestVolcano()
        {
            GameObject[] volcanoes = GameObject.FindGameObjectsWithTag ("Volcano");
            if(volcanoes.Length > 0)
            {
                targetVolcano = volcanoes[0];
                float shortestDistance = (targetVolcano.transform.position - transform.position).sqrMagnitude;
                foreach( GameObject volcano in volcanoes)
                {
                    float distance = (volcano.transform.position - transform.position).sqrMagnitude;
                    if( distance < shortestDistance)
                    {
                        shortestDistance = distance;
                        targetVolcano = volcano;
                    }
                }
            }
        }

        /// <summary>
        /// Assigns random volcano variable
        /// </summary>
        private void DetermineRandomVolcano()
        {
            GameObject[] volcanoes = GameObject.FindGameObjectsWithTag("Volcano");
            if(volcanoes.Length > 0)
            {
                int index = (int)Mathf.Floor(UnityEngine.Random.value * volcanoes.Length);
                targetVolcano = volcanoes[index];
            }
        }

        /// <summary>
        /// This will use a probabilistic model in which the model will favor the
        /// volcano that is the furthest away from the volcano. The spread will use
        /// a geometric series based on the defined values (.25, .5, or .75 spread).
        /// The spread is the concept that the first number in the series will be a
        /// percentage of 1, and the next will be the same percentage of the difference.
        /// </summary>
        private void DetermineProbVolcano()
        {
            const float PROB_SPREAD = .75f;

            GameObject[] volcanoes = GameObject.FindGameObjectsWithTag("Volcano");
            if (volcanoes.Length > 0)
            {
                volcanoes = volcanoes.OrderByDescending(volcano => Vector2.Distance(volcano.transform.position, transform.position)).ToArray();
                float prob = UnityEngine.Random.value;
                for (int i = 0; i < volcanoes.Length; i++)
                {
                    if (i == volcanoes.Length - 1)
                    {
                        targetVolcano = volcanoes[i];
                    }
                    else
                    {
                        if(prob < Mathf.Pow(PROB_SPREAD, (float)(i + 1)))
                        {
                            targetVolcano = volcanoes[i];
                            break;
                        }
                    }
                }
            }
        }
        

        /// <summary>
        /// Lost Health function for Charge
        /// </summary>
        /// <param name="damage"></param>
        /// <returns></returns>
		public override bool LoseHealth(float damage)
		{
			health -= damage;
			if (health <= 0)
			{
				Instantiate (chargeDeath, this.transform.position, Quaternion.identity);
				Destroy(gameObject);
				return true;
			}
			return false;
		}
	}
}
