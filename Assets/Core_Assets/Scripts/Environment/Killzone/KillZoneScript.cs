﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities.Swarm;

namespace Assets.Scripts.Environment
{
	public sealed class KillZoneScript : MonoBehaviour
    {
		public float damage = 9001;
        public bool permanent = false;
        public float killZoneLifeTime = 1.0f;

        private float killDelayTimer ;

        /// <summary>
        /// Kills minions entering the killZone
        /// </summary>
        /// <param name="col">Col.</param>
        void OnTriggerEnter2D(Collider2D col)
        {
            if(enabled)
            {
                if(col.gameObject.tag == "Minion")
                {
                    Minion m = col.gameObject.GetComponent<Minion>();
                    m.LoseHealth(damage);
                }
            }
        }

        /// <summary>
        /// Kills minions entering the killZone
        /// </summary>
        /// <param name="col">Col.</param>
        void OnCollisionEnter2D(Collision2D col)
        {
            if(enabled)
            {
                OnTriggerEnter2D(col.collider);
            }
        }

        /// <summary>
        /// Start this instance.
        /// </summary>
        void Start()
        {
            this.killDelayTimer = killZoneLifeTime;
        }

        /// <summary>
        /// Update this instance.
        /// </summary>
		void Update()
		{
            if(!permanent)
            {
                killDelayTimer -= Time.deltaTime;
                if(killDelayTimer <= 0)
                {
                  Destroy(gameObject);
                }
            }
		}
	}
}
