﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities;

namespace Assets.Scripts.Environment
{
    /// <summary>
    /// Very simple script for destructible walls.
    /// </summary>
    public class DestructibleWall : Entity
    {
        [SerializeField]
        private float initialHealth;

        /// <summary>
        /// Assign basic health and attributes.
        /// </summary>
        public void Start()
        {
            this.health = initialHealth;
            this.attributes = new EntityAttributes();
        }
    }
}
