using UnityEngine;
using System.Timers;
using System.Collections;
using Assets.Scripts.Cameras;
using Assets.Scripts.Entities.Player;

// For usage apply the script directly to the element you wish to apply parallaxing
// Based on Brackeys 2D parallaxing script http://brackeys.com/
public class Parallax : MonoBehaviour {
	
	private Transform cam; 		// Camera reference (of its transform)
	Vector3 previousCamPos;		// Camera position of last update
	
	[Range (1f,11f)]        
	public float ParallaxScalar = 5f;		// Scalar for parallax offset

	void Awake () {
		cam = Camera.main.transform;
        previousCamPos = cam.position;	
	}
	
	void Update () {
            float parallaxX = ((previousCamPos.x - cam.position.x) * 0.15f) * -(1 + (transform.position.z * 0.001f * ParallaxScalar));
            float parallaxY = ((previousCamPos.y - cam.position.y) * 0.05f) * -(1 + (transform.position.z * 0.001f * ParallaxScalar));

            //The new vector position
            Vector3 targetPosition = new Vector3(transform.position.x + parallaxX,
                                                 transform.position.y + parallaxY,
                                                 transform.position.z);

            //Update position
            transform.position = targetPosition;

            //Update previous position to be the current position
			previousCamPos = cam.position;	
	}

}