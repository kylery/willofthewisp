﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Environment
{
    /// <summary>
    /// 
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public sealed class Tiling : MonoBehaviour
    {

        public int offsetX = 2;					// offset to avoid errors

        //checks to help with instantiation
        public bool hasCloneRight = false;
        public bool hasCloneLeft = false;

        public bool reverseScale = false;  		// used if the sprite is not tileable

        private float spriteWidth = 0f;			// width of the sprite
        private UnityEngine.Camera cam;
        private Transform myTransform;

        void Awake()
        {
            cam = UnityEngine.Camera.main;
            myTransform = transform;
        }

        /// <summary>
        /// Initialize the tile's sprite.
        /// </summary>
        void Start()
        {
            SpriteRenderer sRenderer = GetComponent<SpriteRenderer>();
            spriteWidth = sRenderer.sprite.bounds.size.x;
        }

        /// <summary>
        /// 
        /// </summary>
        void Update()
        {
            //if no left clone or right clone exist
            if (!hasCloneLeft || !hasCloneRight)
            {
                // calculate the camera's extend (half the width) of what the cam sees in world coords
                float camHorizontalExtend = cam.orthographicSize * Screen.width / Screen.height;

                //calculate the x pos where the cam can see the edge of the sprite
                float edgeVisiblePosRight = (myTransform.position.x + spriteWidth / 2) - camHorizontalExtend;
                float edgeVisiblePosLeft = (myTransform.position.x - spriteWidth / 2) + camHorizontalExtend;

                if (cam.transform.position.x >= (edgeVisiblePosRight - offsetX) && !hasCloneRight)
                {
                    MakeClone(1);
                    hasCloneRight = true;
                }
                else if (cam.transform.position.x <= edgeVisiblePosLeft + offsetX && !hasCloneLeft)
                {
                    MakeClone(-1);
                    hasCloneLeft = true;
                }
            }
        }

        /// <summary>
        /// Creates a cloned sprite on the required side (indicated with rightOrLeft)
        /// </summary>
        /// <param name="rightOrLeft">1 if on right, -1 if on left</param>
        void MakeClone(int rightOrLeft)
        {
            // calculating the new position for the clone sprite
            Vector3 newPosition = new Vector3(myTransform.position.x + spriteWidth * rightOrLeft, myTransform.position.y, myTransform.position.z);
            // instantiate clone sprite and store it
            Transform clone = Instantiate(myTransform, newPosition, myTransform.rotation) as Transform;

            //if not tileable, reverse x size to flip the next tile to prevent ugly seam
            if (reverseScale)
            {
                clone.localScale = new Vector3(clone.localScale.x * (-1), clone.localScale.y, clone.localScale.z);
            }

            clone.parent = myTransform.parent;
            if (rightOrLeft > 0)
            {
                clone.GetComponent<Tiling>().hasCloneLeft = true;
            }
            else
            {
                clone.GetComponent<Tiling>().hasCloneRight = true;
            }
        }
    }
}