﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Projectiles;

namespace Assets.Scripts.Environment
{
	public class KillIndicatorScript : MonoBehaviour 
    {
		// Variables to store for the killZone
		public GameObject killPrefab;
        public float indicatorLifeTime = 1.0f;

		// Timer for the indicator
		private float indicatorTimer;

        void Start()
        {
            this.indicatorTimer = indicatorLifeTime;
        }

		// Update is called once per frame
		void Update () 
        {
			indicatorTimer -= Time.deltaTime;
			if(indicatorTimer <= 0)
			{
				SpawnKillZone();
			}
		}
		
		private void SpawnKillZone()
		{
			Instantiate (killPrefab,transform.position, transform.localRotation);
			Destroy (gameObject);
	    }
	}
}