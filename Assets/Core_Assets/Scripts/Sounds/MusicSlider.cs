﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts.Utils;

public class MusicSlider : MonoBehaviour {

	private static Slider musicSlider;
	private MusicController musicController;
	
	void Start()
	{
		musicSlider = GetComponent<Slider> ();
		DebugUtils.Assert (musicSlider != null, "The Music slider couldn't be found");
		musicController = MusicController.Instance;
		DebugUtils.Assert (musicController != null, "The Music Controller couldn't be found");
		musicSlider.value = musicController.GetSliderVolume();
	}
	
	void Update()
	{
		musicController = MusicController.Instance;
		if (musicController == null)
			return;
		musicController.SetSliderVolume (musicSlider.value);
	}
}
