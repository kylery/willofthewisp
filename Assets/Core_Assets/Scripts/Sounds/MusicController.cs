﻿using UnityEngine;
using System.Collections;
using Assets.Scripts;

/*Designers must put a music clip inside of the audio source inspector in Unity. 
*/
public class MusicController : MonoBehaviour {
	
	private AudioSource mainSource;
    private AudioSource source;

    //A boolean flag to check to see if the MainMusic Prefab is created or not. 
    private bool create = false;

    private static MusicController instance;

    //Places where you could put audio clips onto for the inspector
    [SerializeField]
    private AudioClip menuClip;
    [SerializeField]
    private AudioClip menuIntroClip;
    [SerializeField]
    private float menuVolume = 1.0f;

    //Turtles and challenges 
    [SerializeField]
    private AudioClip turtleClip;
    [SerializeField]
    private AudioClip turtleIntroClip;
    [SerializeField]
    private float turtleVolume = 1.0f;
    [SerializeField]
    private AudioClip turtleChallengeClip1;
    [SerializeField]
    private AudioClip turtleIntroChallengeClip1;
    [SerializeField]
    private float turtleChallengeVolume1 = 1.0f;
    [SerializeField]
    private AudioClip turtleChallengeClip2;
    [SerializeField]
    private AudioClip turtleIntroChallengeClip2;
    [SerializeField]
    private float turtleChallengeVolume2 = 1.0f;
    [SerializeField]
    private AudioClip turtleChallengeClip3;
    [SerializeField]
    private AudioClip turtleIntroChallengeClip3;
    [SerializeField]
    private float turtleChallengeVolume3 = 1.0f;

    //Shorca and challeneges
    [SerializeField]
    private AudioClip shorcaClip;
    [SerializeField]
    private AudioClip shorcaIntroClip;
    [SerializeField]
    private float shorcaVolume = 1.0f;
    [SerializeField]
    private AudioClip shorcaChallengeClip1;
    [SerializeField]
    private AudioClip shorcaIntroChallengeClip1;
    [SerializeField]
    private float shorcaChallengeVolume1 = 1.0f;
    [SerializeField]
    private AudioClip shorcaChallengeClip2;
    [SerializeField]
    private AudioClip shorcaIntroChallengeClip2;
    [SerializeField]
    private float shorcaChallengeVolume2 = 1.0f;
    [SerializeField]
    private AudioClip shorcaChallengeClip3;
    [SerializeField]
    private AudioClip shorcaIntroChallengeClip3;
    [SerializeField]
    private float shorcaChallengeVolume3 = 1.0f;

    [SerializeField]
    private AudioClip creditsClip;
    [SerializeField]
    private AudioClip creditsIntroClip;
    [SerializeField]
    private float creditsVolume = 1.0f;

    [SerializeField]
    private AudioClip victoryClip;
    [SerializeField]
    private AudioClip victoryIntroClip;
    [SerializeField]
    private float victoryVolume = 1.0f;
    [SerializeField]
    private AudioClip gameOverClip;
    [SerializeField]
    private AudioClip gameOverIntroClip;
    [SerializeField]
    private float gameOverVolume = 1.0f;

    private float sliderVolume = 1.0f;

	// Use this for initialization
    void Start()
    {
        source = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<AudioSource>();
        
    }

    /// <summary>
    /// The current instance of the controller.
    /// </summary>
    public static MusicController Instance
    {
        get
        {
            if(instance == null)
            {
                instance = (MusicController)FindObjectOfType(typeof(MusicController));
                if (instance == null)
                    UnityEngine.Debug.LogError("Music Controller does not exist");
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
    }

    /// <summary>
    /// This will initialize the game at a high-level.
    /// </summary>
    void Awake()
    {
        mainSource = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<AudioSource>();
        if(mainSource == null)
        {
            UnityEngine.Debug.LogError("There is no main music prefab");
        }

        if (FindObjectsOfType(typeof(MusicController)).Length > 1)
        {
            DestroyImmediate(gameObject);
        }
        //Keeps the music persist through the main menu to the level select and vice versa. 
        if (Application.loadedLevelName.Equals(Constants.MAIN_MENU_LEVEL))
        {
            //If the Prefab isn't created, don't destroy the object.
            if (!create)
            {
                DontDestroyOnLoad(mainSource.gameObject);
                create = true;
            }
            //If the audio clip isn't the menu clip, play the menu clip. 
            if(mainSource.clip != menuClip)
            {
                mainSource.clip = menuIntroClip;
                mainSource.loop = false;
                mainSource.volume = menuVolume;
            }
            mainSource.Play();
        }
        else if(Application.loadedLevelName.Equals(Constants.CREDITS_LEVEL))
        {
            mainSource.clip = creditsIntroClip;
            mainSource.loop = false;
            mainSource.volume = creditsVolume;
            mainSource.Play();
        }
        //If the level is the turtle boss, change music to the turtle clip. 
        else if(Application.loadedLevelName.Equals(Constants.TURTLE_BOSS_LEVEL))
        {
            mainSource.clip = turtleIntroClip;
            mainSource.loop = false;
            mainSource.volume = turtleVolume;
            mainSource.Play();
        }
        else if(Application.loadedLevelName.Equals(Constants.TURTLE_CHALLENGE_ONE_LEVEL))
        {
            mainSource.clip = turtleIntroChallengeClip1;
            mainSource.loop = false;
            mainSource.volume = turtleChallengeVolume1;
            mainSource.Play();
        }
        else if (Application.loadedLevelName.Equals(Constants.TURTLE_CHALLENGE_TWO_LEVEL))
        {
            mainSource.clip = turtleIntroChallengeClip2;
            mainSource.loop = false;
            mainSource.volume = turtleChallengeVolume2;
            mainSource.Play();
        }
        else if (Application.loadedLevelName.Equals(Constants.TURTLE_CHALLENGE_THREE_LEVEL))
        {
            mainSource.clip = turtleIntroChallengeClip3;
            mainSource.loop = false;
            mainSource.volume = turtleChallengeVolume3;
            mainSource.Play();
        }

        //If the level is the shorca boss, change the music to the shorca clip. 
        else if(Application.loadedLevelName.Equals(Constants.SHORCA_BOSS_LEVEL))
        {
            mainSource.clip = shorcaIntroClip;
            mainSource.loop = false;
            mainSource.volume = shorcaVolume;
            mainSource.Play();
        }
        else if(Application.loadedLevelName.Equals(Constants.SHORCA_CHALLENGE_ONE_LEVEL))
        {
            mainSource.clip = shorcaIntroChallengeClip1;
            mainSource.loop = false;
            mainSource.volume = shorcaChallengeVolume1;
            mainSource.Play();
        }
        else if (Application.loadedLevelName.Equals(Constants.SHORCA_CHALLENGE_TWO_LEVEL))
        {
            mainSource.clip = shorcaIntroChallengeClip2;
            mainSource.loop = false;
            mainSource.volume = shorcaChallengeVolume2;
            mainSource.Play();
        }
        else if (Application.loadedLevelName.Equals(Constants.SHORCA_CHALLENGE_THREE_LEVEL))
        {
            mainSource.clip = shorcaIntroChallengeClip3;
            mainSource.loop = false;
            mainSource.volume = shorcaChallengeVolume3;
            mainSource.Play();
        }
    }

    //The thing below isn't compatible with the sound slider. 
    void Update()
    {
        string level = Application.loadedLevelName;

        if (mainSource.clip.Equals(victoryIntroClip))
        {
            if (!mainSource.isPlaying)
            {
                mainSource.clip = victoryClip;
                mainSource.volume = victoryVolume;
                mainSource.loop = true;
                mainSource.Play();
            }
        }
        else if (mainSource.clip.Equals(gameOverIntroClip))
        {
            if (!mainSource.isPlaying)
            {
                mainSource.clip = gameOverClip;
                mainSource.volume = gameOverVolume;
                mainSource.loop = true;
                mainSource.Play();
            }
        }

        switch(level)
        {
            case Constants.MAIN_MENU_LEVEL:
                if(!mainSource.isPlaying)
                {
                    mainSource.clip = menuClip;
                    mainSource.loop = true;
                    mainSource.Play();
                }
                mainSource.volume = menuVolume * sliderVolume;
                break;
            case Constants.TURTLE_BOSS_LEVEL:
                if(!mainSource.isPlaying)
                {
                    mainSource.clip = turtleClip;
                    mainSource.loop = true;
                    mainSource.Play();
                }
                mainSource.volume = turtleVolume * sliderVolume;
                break;
            case Constants.TURTLE_CHALLENGE_ONE_LEVEL:
                if(!mainSource.isPlaying)
                {
                    mainSource.clip = turtleChallengeClip1;
                    mainSource.loop = true;
                    mainSource.Play();
                }
                mainSource.volume = turtleChallengeVolume1 * sliderVolume;
                break;
            case Constants.TURTLE_CHALLENGE_TWO_LEVEL:
                if (!mainSource.isPlaying)
                {
                    mainSource.clip = turtleChallengeClip2;
                    mainSource.loop = true;
                    mainSource.Play();
                }
                mainSource.volume = turtleChallengeVolume2 * sliderVolume;
                break;
            case Constants.TURTLE_CHALLENGE_THREE_LEVEL:
                if (!mainSource.isPlaying)
                {
                    mainSource.clip = turtleChallengeClip3;
                    mainSource.loop = true;
                    mainSource.Play();
                }
                mainSource.volume = turtleChallengeVolume3 * sliderVolume;
                break;
            case Constants.SHORCA_BOSS_LEVEL:
                if (!mainSource.isPlaying)
                {
                    mainSource.clip = shorcaClip;
                    mainSource.loop = true;
                    mainSource.Play();
                }
                mainSource.volume = shorcaVolume * sliderVolume;
                break;
            case Constants.SHORCA_CHALLENGE_ONE_LEVEL:
                if (!mainSource.isPlaying)
                {
                    mainSource.clip = shorcaChallengeClip1;
                    mainSource.loop = true;
                    mainSource.Play();
                }
                mainSource.volume = shorcaChallengeVolume1 * sliderVolume;
                break;
            case Constants.SHORCA_CHALLENGE_TWO_LEVEL:
                if (!mainSource.isPlaying)
                {
                    mainSource.clip = shorcaChallengeClip2;
                    mainSource.loop = true;
                    mainSource.Play();
                }
                mainSource.volume = shorcaChallengeVolume2 * sliderVolume;
                break;
            case Constants.SHORCA_CHALLENGE_THREE_LEVEL:
                if (!mainSource.isPlaying)
                {
                    mainSource.clip = shorcaChallengeClip3;
                    mainSource.loop = true;
                    mainSource.Play();
                }
                mainSource.volume = shorcaChallengeVolume3 * sliderVolume;
                break;
            case Constants.CREDITS_LEVEL:
                if (!mainSource.isPlaying)
                {
                    mainSource.clip = creditsClip;
                    mainSource.loop = true;
                    mainSource.Play();
                }
                mainSource.volume = creditsVolume * sliderVolume;
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Used for other scripts in case the music needs to be played or not. 
    /// </summary>
	public void PlayMusic()
	{
        source = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<AudioSource>();
		if(this.source == null)
		{
            UnityEngine.Debug.LogError("There is no music manager");
		}
		this.source.Play();
	}

    /// <summary>
    /// Used for other scripts in case the music needs to be stopped.
    /// </summary>
	public void StopMusic()
	{
        source = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<AudioSource>();

        if (this.source == null)
        {
            UnityEngine.Debug.LogError("There is no music manager");
        }
		this.source.Stop();
	}

    /// <summary>
    /// Used for other scripts to pause the music. 
    /// </summary>
	public void PauseMusic()
	{
        source = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<AudioSource>();
        if (this.source == null)
        {
            UnityEngine.Debug.LogError("There is no music manager");
        }
        this.source.Pause();
	}

    public void VolumeMusic(float volume)
    {
        source = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<AudioSource>();
        if(this.source == null)
        {
            UnityEngine.Debug.LogError("There is no music manager");
        }
        source.volume = volume * sliderVolume;
    }

    public void SetSliderVolume(float volume)
    {
        sliderVolume = volume;
    }

	public float GetSliderVolume()
	{
		return sliderVolume;
	}
	
	public void PlayGameOver()
    {
        source = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<AudioSource>();
        if(this.source == null)
        {
            UnityEngine.Debug.LogError("There is no music manager");
            return;
        }
        source.clip = gameOverIntroClip;
        source.loop = false;
        source.volume = gameOverVolume * sliderVolume;
        PlayMusic();
    }

    public void PlayVictory()
    {
        source = GameObject.FindGameObjectWithTag("MusicManager").GetComponent<AudioSource>();
        if (this.source == null)
        {
            UnityEngine.Debug.LogError("There is no music manager");
            return;
        }
        source.clip = victoryIntroClip;
        source.loop = false;
        source.volume = victoryVolume * sliderVolume;
        PlayMusic();
    }
}
