﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Scripts.Utils;

public class SoundController : MonoBehaviour {

	private AudioSource sound;

    private static float soundTimer = 0.0f;
    //private static List<float> soundTimer;
    private List<float> timerList;

    private AudioClip soundClip;

    private float sliderVolume = 1.0f;

    private List<AudioSource> soundList;
    private List<bool> isPlayingList;
    private bool isPlaying = false;
    private int counter = 0;

	// Use this for initialization
	void Start () 
	{
		sound = this.GetComponent<AudioSource>();
        soundList = new List<AudioSource>();
        isPlayingList = new List<bool>();
        timerList = new List<float>();
        //soundTimer = new List<float>();
    }

    private static SoundController instance;

    /// <summary>
    /// The current instance of the sound controller.
    /// </summary>
    public static SoundController Instance
    {
        get
        {
            if(instance == null)
            {
                instance = (SoundController)FindObjectOfType(typeof(SoundController));
                if (instance == null)
                {
                    UnityEngine.Debug.LogError("Sound Controller does not exist");
                }
                DontDestroyOnLoad(instance.gameObject);
            }
            return instance;
        }
    }

    void Update()
    {

        //foreach(float time in timerList)
        //{
        //    soundTimer = time - Time.deltaTime;
        //}

        //soundTimer[counter] = timerList[counter] - Time.deltaTime;
        //for (int t = 0; t < soundList.Count; t++ )
        //{
        //    if(soundTimer[t] <= 0f)
        //    {
        //        soundList[t].Stop();
        //        soundList.RemoveAt(t);
        //        timerList.RemoveAt(t);
        //        soundTimer.RemoveAt(t);
        //        Destroy(this.gameObject.GetComponent<AudioSource>());
        //        counter--;
        //    }

        //}

        soundTimer -= Time.deltaTime;
        if (soundTimer <= 0f)
        {
            for (int i = 0; i < isPlayingList.Count; i++)
            {
                if (isPlayingList[i])
                {
                    if(soundList[i] == null)
                    {
                        isPlayingList.RemoveAt(i);
                        counter--;
                    }
                    else
                    {
                        soundList[i].Stop();
                        soundList.RemoveAt(i);
                        isPlayingList.RemoveAt(i);
                        Destroy(gameObject.GetComponent<AudioSource>());
                        counter--;
                    }
                    
                }
            }
            Destroy(gameObject.GetComponent<AudioSource>());
        }
    }
       

    void Awake()
    {
        if (FindObjectsOfType(typeof(SoundController)).Length > 1)
        {
            DestroyImmediate(gameObject);
        }
        else
            DontDestroyOnLoad(this.gameObject);
    }

    /// <summary>
    /// Plays the sound from the audio source using PlayOneShot.
    /// </summary>
    /// <param name="clip"></param> The sound that will be played.
    /// <param name="time"></param> How long it'll take for the sound to play
	public void PlaySound(AudioClip clip, float time)
	{
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<AudioSource>();

		if(clip == null)
		{
			UnityEngine.Debug.LogError("There is no sound clip");
		}
        //soundTimer.Add(time);
        soundTimer = time;
        timerList.Add(time);

        soundList[counter].clip = clip;
        isPlayingList.Add(isPlaying);

        isPlayingList[counter] = true;
        soundList[counter].Play();
        counter++;
	}

    public void PlaySound()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<AudioSource>();
        if (sound == null)
        { }
        else
        {
            if (sound == null)
            {
                UnityEngine.Debug.LogError("There is no sound clip");
            }
            for (int i = 0; i < soundList.Count; i++)
            {
                soundList[i].Play();
            }
        }
 
    }

	public void StopSound()
	{
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<AudioSource>();
        if (sound == null)
        {
            UnityEngine.Debug.LogError("there is no sound controller");
        }
        for(int i = 0; i < soundList.Count; i++)
        {
            soundList[i].Stop();
        }
	}

	public void VolumeSound(float volume)
	{
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<AudioSource>();
        if (sound == null)
        {
            UnityEngine.Debug.LogError("there is no sound controller");
        }
        for (int i = 0; i < soundList.Count; i++ )
        {
            soundList[i].volume = volume * sliderVolume;
        }
    }

    public void PitchSound(float pitch)
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<AudioSource>();
        if (sound == null)
        {
            UnityEngine.Debug.LogError("there is no sound controller");
        }
        for (int i = 0; i < soundList.Count; i++)
        {
            soundList[i].pitch = pitch;
        }
    }

    public void PauseSound()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<AudioSource>();
        if (sound == null)
        {
            UnityEngine.Debug.LogError("there is no sound controller");
        }
        else
        {
            for(int i = 0; i < soundList.Count; i++)
            {
                soundList[i].Pause();
            }
        }
            //this.sound.Pause();
    }

    public void SetSliderVolume(float volume)
    {
        sliderVolume = volume;
    }

	public float GetSliderVolume()
	{
		return sliderVolume;
	}

    public void CreateAudioSource()
    {
        AudioSource sound = gameObject.AddComponent<AudioSource>();
        sound.rolloffMode = AudioRolloffMode.Linear;
        sound.volume = 1.0f;
        sound.spread = 360;
        sound.panLevel = 0;

        soundList.Add(sound);
    }
}
