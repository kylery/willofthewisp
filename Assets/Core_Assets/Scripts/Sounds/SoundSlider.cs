using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Scripts.Utils;

public class SoundSlider : MonoBehaviour {

	private static Slider sound_slider;
    private AudioSource[] soundList;
    private List<float> soundListVolumes;
    private int count = 0;
	void Start()
	{
		sound_slider = GetComponent<Slider> ();
		DebugUtils.Assert (sound_slider != null, "The Sound FX slider couldn't be found");
        count = 0;
        soundList = FindObjectsOfType<AudioSource>();
        soundListVolumes = new List<float>();
        foreach ( AudioSource volume in soundList)
        {
            //Gets the original value of the volumes.
            if (volume.gameObject != GameObject.FindGameObjectWithTag("MusicManager"))
            {
                soundListVolumes.Add(volume.volume);
            }
        }
        sound_slider.value = 1.0f;
	}

	void Update()
	{
        count = 0;
        foreach (AudioSource sound in soundList)
        {
            if(sound.gameObject != GameObject.FindGameObjectWithTag("MusicManager"))
            {
                sound.volume = sound_slider.value * soundListVolumes[count];
                count++;
            }
            if(count >= soundList.Length)
            {
                count = 0;
            }
        }
	}
}