using UnityEngine;
using System.Collections;

using Assets.Scripts.Entities.Player;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Cameras
{
    /// <summary>
    /// This will give instructions to the camera of how it will follow the player. When
    /// the player is moving around, it will follow the player. When the player stops, then
    /// it will ease to the player's position.
    /// </summary>
    public class Camera2DFollow : MonoBehaviour
    {

        public Transform target;
        public Transform background;
        public float damping = 0.6f;
        public float lookAheadFactor = 6;
        public float lookAheadReturnSpeed = 0.4f;
        public float lookAheadMoveThreshold = 0.25f;
        public float zoomSpeed = 5f;

        float offsetZ;
        Vector3 lastTargetPosition;
        Vector3 currentVelocity;
        Vector3 lookAheadPos;

        // Camera Bound calculation variables
        private float minX, minY, maxX, maxY;

        /// <summary>
        /// This will initialize the camera's initial values.
        /// </summary>
        void Start()
        {
            //if no target given, use player
            if (target == null)
                target = PlayerManager.Instance.Player.GetComponent<Transform>();
            //if target still null, bail
            if (target == null)
                return;

            if (background != null)
                background.position = new Vector3(transform.position.x, transform.position.y, background.position.z);
            
            lastTargetPosition = target.position; //initialize last position
            offsetZ = (transform.position - target.position).z; //need a zed for the Vector3.SmoothDamp

            UpdateCameraBounds();
        }

        /// <summary>
        /// This will move the camera accordingly to follow the player at a pace
        /// that it will follow the player. When the player stops moving, then
        /// the camera will give the slowing down effect to ease into the player's
        /// position.
        /// </summary>
        void FixedUpdate()
        {
            //if no target given, use player
            if (target == null)
                target = PlayerManager.Instance.Player.GetComponent<Transform>();

            //The target not existing shouldn't exit gracefully.
            DebugUtils.Assert(target != null, "Target does not exist.");

            // only update lookahead pos if accelerating or changed direction
            float xMoveDelta = (target.position - lastTargetPosition).x; //how much has target moved?

            //update if they've moved more than the threshold
            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

            if (updateLookAheadTarget)
            {
                lookAheadPos = lookAheadFactor * Vector3.right * Mathf.Sign(xMoveDelta);
            }
            else
            {
                lookAheadPos = Vector3.MoveTowards(lookAheadPos, Vector3.zero, Time.deltaTime * lookAheadReturnSpeed);
            }

            //where to move to
            Vector3 aheadTargetPos = target.position + lookAheadPos + Vector3.forward * offsetZ;
            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref currentVelocity, damping);

            //Limits camera movement based upon bounds
            newPos.x = Mathf.Clamp(newPos.x, minX, maxX);
            newPos.y = Mathf.Clamp(newPos.y, minY, maxY);

            //move
            transform.position = newPos;

            //update last position
            lastTargetPosition = target.position;

            if (background != null)
                background.position = new Vector3(transform.position.x, transform.position.y, background.position.z);
        }

        /// <summary>
        /// This will force the camera to clamp within the bounds.
        /// </summary>
        public void ClampCamera()
        {
            Vector3 newPos = transform.position;

            //Limits camera movement based upon bounds
            newPos.x = Mathf.Clamp(newPos.x, minX, maxX);
            newPos.y = Mathf.Clamp(newPos.y, minY, maxY);

            //move
            transform.position = newPos;
        }

        /// <summary>
        /// It may be necessary to update camera bounds by switching through several. This
        /// is will find the camera bounds and update itself to compensate.
        /// </summary>
        public void UpdateCameraBounds()
        {
            //Caculates Camera bound dimensions
            float vertExt = Camera.main.orthographicSize;
            float horzExt = vertExt * (((float)Screen.width) / ((float)Screen.height));
            GameObject boundsObject = GameObject.FindGameObjectWithTag("CameraBounds");

            if (boundsObject == null)
            {
                Debug.LogError("No CameraBounds Tag is defined");
            }

            // Obtains bounds sprite
            Vector3 boundsVector = boundsObject.GetComponent<SpriteRenderer>().bounds.size;

            // Sets bounds
            minX = horzExt - (boundsVector.x / 2.0f) + boundsObject.transform.position.x;
            maxX = (boundsVector.x / 2.0f) - horzExt + boundsObject.transform.position.x;
            minY = vertExt - (boundsVector.y / 2.0f) + boundsObject.transform.position.y;
            maxY = (boundsVector.y / 2.0f) - vertExt + boundsObject.transform.position.y;
        }
    }
}
