﻿using System;
using UnityEngine;

namespace Assets.Scripts.Cameras
{
    /// <summary>
    /// This will allow any object in the game to shake the
    /// camera.
    /// </summary>
    public sealed class CameraShake : MonoBehaviour
    {
        [SerializeField]
        private float shakeIntensity;
        [SerializeField]
        private float shakeFrequency;

        private float shakeDuration;

        [SerializeField]
        private AudioClip cameraShakeClip;
        AudioSource sound;

        private static bool soundPlaying = false;

        void Awake()
        {
            sound = gameObject.GetComponent<AudioSource>();
        }

        /// <summary>
        /// This will handle the shaking.
        /// </summary>
        void FixedUpdate()
        {
            if(shakeDuration > 0)
            {
                if(soundPlaying)
                {
                    soundPlaying = false;
                    sound.clip = cameraShakeClip;
                    sound.Play();
                }


                float shakeY = shakeIntensity * Mathf.Sin(Time.time * shakeFrequency);
                float shakeX = shakeIntensity * Mathf.Sin(Time.time * shakeFrequency);
                Vector2 trajectory = new Vector2(shakeX, shakeY);
                transform.position += (Vector3)(trajectory * Time.deltaTime);
                shakeDuration -= Time.deltaTime;
            }
        }

        /// <summary>
        /// This will start the shaking by allowing the caller
        /// to determine how long the camera should shake for.
        /// </summary>
        /// <param name="shakeDuration">How lon ghte camera should shake.</param>
        public void ShakeCamera(float shakeDuration)
        {
            soundPlaying = true;
            this.shakeDuration = shakeDuration;
        }

        /// <summary>
        /// This determines if the camera is currently shaking.
        /// </summary>
        /// <returns>Is the camera shaking.</returns>
        public bool CurrentlyShaking()
        {
            return !(shakeDuration <= 0.025f);
        }
    }
}
