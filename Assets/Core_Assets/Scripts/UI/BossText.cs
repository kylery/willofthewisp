﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// This will display the name of the boss in the intro animation. 
    /// </summary>
    public class BossText : MonoBehaviour
    {
        [SerializeField]
        private float fadeOutSpeed;
        [SerializeField]
        private float jumpInTime;

        private float curJumpInTime;
        private UnityEngine.UI.Image bossText;
        private bool fadingOut;

        void Awake()
        {
            curJumpInTime = jumpInTime;
            bossText = GetComponent<UnityEngine.UI.Image>();
            bossText.color = Color.clear;
            fadingOut = false;
        }

        void FixedUpdate()
        {
            if (!fadingOut)
            {
                curJumpInTime -= Time.deltaTime;
                if (curJumpInTime <= 0)
                {
                    bossText.color = Color.white;
                    fadingOut = true;
                }
            }
            else
            {
                Color newColor = bossText.color;
                newColor.a -= fadeOutSpeed;
                bossText.color = newColor;
                if (bossText.color.a <= 0)
                {
                    gameObject.SetActive(false);
                }
            }
        }
    }
}
