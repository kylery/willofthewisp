﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts.Entities.Player;
using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Utils;

namespace Assets.Scripts.UI
{
    [Serializable]
    struct KeyboardControllerPanel
    {
        public Texture2D keyboardPanel;
        public Texture2D controllerPanel;
    }

    /// <summary>
    /// This deals with the logic of swiping through many tutorial cars that allow the user to
    /// go forward or backwards.
    /// </summary>
    public class SwipePanels : MonoBehaviour
    {
        private enum SwipeState
        {
            NORMAL,
            SWIPE
        }

        [SerializeField]
        [Range(0, 1)]
        private float percentScreen;
        [SerializeField]
        private float rateOfAlpha;
        [SerializeField]
        private float angularChange;
        [SerializeField]
        private List<KeyboardControllerPanel> panels;

        private bool playOnExit;
        private string levelToPlay;
        private SwipeState state;
        private Player player;
        private int curPanelIndex;
        private bool goingForward;
        private GameObject[] menuButtons;
        private float curAngle;
        private float curAlpha;
        private Vector2 pivot;
        private GameManager gameManager;

        /// <summary>
        /// Initializes the Panels by determining how it should handle the exit if it should
        /// stay on the same level or load the boss on exit.
        /// </summary>
        /// <param name="playOnExit">If the game should play the boss on exit.</param>
        /// <param name="levelToPlay">The boss to play on exit.</param>
        public void Init(bool playOnExit, string levelToPlay = null)
        {
            this.playOnExit = playOnExit;
            this.levelToPlay = levelToPlay;
        }

        /// <summary>
        /// Initialize the swipe panels.
        /// </summary>
        void Awake()
        {
            state = SwipeState.NORMAL;
            curPanelIndex = 0;
            curAngle = 0;
            curAlpha = 1f;
            pivot = new Vector2(Screen.width + Screen.width / 2, Screen.height / 2);
        }

        /// <summary>
        /// Get references to other game objects when the swipe panel first starts.
        /// </summary>
        void Start()
        {
            player = PlayerManager.Instance.Player.GetComponent<Player>();
            player.FreezePlayer();
            menuButtons = GameObject.FindGameObjectsWithTag("MenuButton");
            foreach (GameObject go in menuButtons)
            {
                EmbeddedMenuButton button = go.GetComponent<EmbeddedMenuButton>();
                button.enabled = false;
            }
            
            MinionManager.Instance.MoveSwarmBack();
            gameManager = GameManager.Instance;
        }

        /// <summary>
        /// Update the panels based on if its in its normal state
        /// or if its swiping to the next panel.
        /// </summary>
        void Update()
        {
            switch (state)
            {
                case SwipeState.NORMAL:
                    NormalUpdate();
                    break;
                case SwipeState.SWIPE:
                    // There are no logical updates for swipe.
                    break;
                default:
                    Debug.LogError("Swipe State needs to be a valid state.");
                    break;
            }
        }

        void FixedUpdate()
        {
            switch (state)
            {
                case SwipeState.NORMAL:
                    // There are no physics update for normal.
                    break;
                case SwipeState.SWIPE:
                    SwipeFixedUpdate();
                    break;
                default:
                    Debug.LogError("Swipe State needs to be a valid state.");
                    break;
            }
        }

        /// <summary>
        /// Normal will just display the panels and wait for the user to input either A or
        /// B on the controller. A will bring the user to the next panel while B will bring
        /// the user to the previous panel.
        /// </summary>
        private void NormalUpdate()
        {
            if (InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.A) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
            {
                if (curPanelIndex < panels.Count - 1)
                {
                    curPanelIndex++;
                    state = SwipeState.SWIPE;
                    goingForward = true;
                }
                else
                {
                    GameObject.Destroy(gameObject);
                }
            }
            else if (InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.B) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow))
            {
                if (curPanelIndex > 0)
                {
                    const float START_ANGLE = 8;

                    curPanelIndex--;
                    state = SwipeState.SWIPE;
                    goingForward = false;
                    curAngle = START_ANGLE;
                    curAlpha = 0;
                }
            }
            else if (InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.Y) || Input.GetKeyUp(KeyCode.E) || Input.GetKeyUp(KeyCode.Escape))
            {
                GameObject.Destroy(gameObject);
            }
        }

        private void SwipeFixedUpdate()
        {
            const float END_SWIPE_THRESHOLD = 0.1f;

            bool changeToNormal = false;
            if (goingForward)
            {
                curAngle += (angularChange * Mathf.Deg2Rad) % 360;
                curAlpha -= rateOfAlpha * Time.deltaTime;

                if (curAlpha <= END_SWIPE_THRESHOLD)
                    changeToNormal = true;
            }
            else
            {
                curAngle -= (angularChange * Mathf.Deg2Rad) % 360;
                curAlpha += rateOfAlpha * Time.deltaTime;

                if (curAlpha >= 1 - END_SWIPE_THRESHOLD)
                    changeToNormal = true;
            }

            if (changeToNormal)
            {
                state = SwipeState.NORMAL;
                curAlpha = 1;
                curAngle = 0;
            }
        }

        /// <summary>
        /// This will display the panel and will handle how to display both panels as well
        /// as rotate one of the panels when the panels are in the swiping state.
        /// </summary>
        void OnGUI()
        {
            const int BUFFER = 10;

            Texture2D curPanel = gameManager.UsingController ? panels[curPanelIndex].controllerPanel : panels[curPanelIndex].keyboardPanel;
            Rect drawWindow = new Rect(BUFFER, BUFFER, Screen.width - BUFFER, Screen.height - BUFFER);

            if (state == SwipeState.SWIPE)
            {
                Texture2D nextTexture;
                Rect nextPanelWindow = new Rect(BUFFER, BUFFER, Screen.width - BUFFER, Screen.height - BUFFER);
                if (goingForward)
                {
                    nextTexture = gameManager.UsingController ? panels[curPanelIndex].controllerPanel : panels[curPanelIndex].keyboardPanel;
                    curPanel = gameManager.UsingController ? panels[curPanelIndex - 1].controllerPanel : panels[curPanelIndex - 1].keyboardPanel;
                }
                else
                {
                    nextTexture = gameManager.UsingController ? panels[curPanelIndex + 1].controllerPanel : panels[curPanelIndex + 1].keyboardPanel;
                    curPanel = gameManager.UsingController ? panels[curPanelIndex].controllerPanel : panels[curPanelIndex].keyboardPanel;
                }

                GUI.DrawTexture(nextPanelWindow, nextTexture, ScaleMode.ScaleToFit);
            }

            Matrix4x4 svMatrix = GUI.matrix;
            Color newColor = GUI.color;
            newColor.a = curAlpha;
            GUI.color = newColor;

            GUIUtility.RotateAroundPivot(curAngle, pivot);
            GUI.DrawTexture(drawWindow, curPanel, ScaleMode.ScaleToFit);
            GUI.matrix = svMatrix;
        }

        /// <summary>
        /// When the panels are being destroyed, this will clean up the resources and restore
        /// player state. It will also decide what to do if the player should go to the boss
        /// level or stay in the same level.
        /// </summary>
        void OnDestroy()
        {
            foreach (GameObject go in menuButtons)
            {
                if (go != null)
                {
                    EmbeddedMenuButton button = go.GetComponent<EmbeddedMenuButton>();
                    button.enabled = true;
                }
            }
            if (player != null)
            {
                player.UnfreezePlayer();
                MinionManager.Instance.MoveSwarmForward();
            }

            if (playOnExit)
            {
                GameManager.Instance.ChangeLevel(levelToPlay);
            }
        }
    }
}
