﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Entities.Player;
using Assets.Scripts.Utils;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// When the swarm get smaller than 5 minions, then this will create an indicator that shows the player
    /// where the remaining swarm are.
    /// </summary>
    public class SwarmIdentifier : MonoBehaviour
    {
        private static readonly int SWARM_COUNT_TO_SHOW = 5;

        private Player player;
        private int swarmCount;
        private Camera mainCamera;

        /// <summary>
        /// This will initialize the swarm count.
        /// </summary>
        void Awake()
        {
            // we do max value, because it triggers finding swarm when it is
            // below a threshold.
            swarmCount = Int32.MaxValue;
        }

        /// <summary>
        /// This will get the current instance of the player and the camrea.
        /// </summary>
        void Start()
        {
            player = PlayerManager.Instance.Player.GetComponent<Player>();
            mainCamera = Camera.main;
        }

        /// <summary>
        /// This will update the current swarm count.
        /// </summary>
        void LateUpdate()
        {
            swarmCount = MinionManager.Instance.swarmSize;
        }

        /// <summary>
        /// This will draw the indicator that will tell where the remaining minions are.
        /// </summary>
        void OnGUI()
        {
            if (swarmCount < SWARM_COUNT_TO_SHOW)
            {
                const float IDENTIFIER_MAGNITUDE = 8;

                List<Vector2> minionPositions = MinionManager.Instance.swarmPositions;
                Vector2 playerPosition = player.transform.position;

                foreach (Vector2 position in minionPositions)
                {
                    Vector2 direction = position - playerPosition;
                    direction.Normalize();
                    direction *= IDENTIFIER_MAGNITUDE;
                    Vector2 playerScreenPos = mainCamera.WorldToViewportPoint(playerPosition);
                    Vector2 targetScreenPos = mainCamera.WorldToViewportPoint(playerPosition + direction);
                    GLUtils.DrawLine(playerScreenPos, targetScreenPos);
                }
            }
        }
    }
}
