﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Assets.Scripts;
using Assets.Scripts.Utils;
using UnityEngine.UI;
using Assets.Scripts.UI;

public class HideOnPause : MonoBehaviour {

    [SerializeField]
    private List<GameObject> ObjToHideList;
    private Dictionary<GameObject, bool> ObjToHideDict;
    private bool lastFramePaused = false;

	// Use this for initialization
	void Start () {
        ObjToHideDict = new Dictionary<GameObject, bool>();

        //ADD DEFAULTS
        GameObject _go = GameObject.Find("BossName");
        if(_go != null)
        {
            ObjToHideList.Add(_go);
        }

		_go = GameObject.Find("Nameplate");
		if(_go != null)
		{
			ObjToHideList.Add(_go);
		}
        _go = GameObject.Find("Canvas");
        if (_go != null)
        {
            foreach (var t in GameObjectUtils.GetChildren(_go, "BossHealth"))
            {
                if (t == null)
                    continue;
                ObjToHideList.Add(t.gameObject);
            }
        }

        //Add the list to the dict default false
        foreach(GameObject go in ObjToHideList)
        {
            if (go == null)
            {
                continue;
            }
            ObjToHideDict.Add(go, false);
        }

	}

    private bool AddToDict(GameObject go)
    {
        if( go == null)
        {
            return false;
        }
        bool exists = false;
        ObjToHideDict.TryGetValue(go, out exists);
        if (!exists)
        {
            ObjToHideDict.Add(go, false);
            return true;
        }
        return false;
    }
	
	// Update is called once per frame
	void Update () {
	    if(GameManager.Instance.isPaused && !lastFramePaused)
        {
            foreach(GameObject go in ObjToHideList)
            {
                if(go != null)
                {
                    if (go.activeSelf)
                    {
                        ObjToHideDict[go] = go.activeSelf;
                        go.SetActive(false);
                    }
                }
            }
        }
        if(!GameManager.Instance.isPaused && lastFramePaused)
        {
            foreach (GameObject go in ObjToHideList)
            {
                if (go != null)
                {
                    if (ObjToHideDict[go])
                    {
                        go.SetActive(true);
                        ObjToHideDict[go] = false;
                    }
                }
            }
        }
        lastFramePaused = GameManager.Instance.isPaused;
	}
}
