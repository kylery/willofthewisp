using UnityEngine;
using System.Timers;
using System.Collections;

using Assets.Scripts.Utils;
using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Entities.Player;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// Provides a HUD GUI display that represents the entity's health.
    /// To get the Healthbar to render, a texture and a material need
    /// to be provided.
    /// </summary>
    public sealed class SwarmDamageAndCount : MonoBehaviour
    {
        //public variables
        public bool show = true;                    //determines whether or not to draw the the Healthbar
        public int 	dangerouslyLowMinionCount = 10; //the number of minions to indicate dangerous level

        public GlowEffect lostMinions = new GlowEffect (Color.red, 2.0f, 0.65f, GlowEffect.GlowStyle.FADE);
        public GlowEffect addedMinions = new GlowEffect (Color.green, 2.0f, 0.65f, GlowEffect.GlowStyle.FADE);
        public GlowEffect dangerLowMinions = new GlowEffect (Color.red, 0.75f, 0.65f, GlowEffect.GlowStyle.PULSE);
        public GlowEffect maxMinions = new GlowEffect (Color.yellow, 1.5f, 0.65f, GlowEffect.GlowStyle.CONSTANT);
		
        //private
        private int old_num_units = -1;				//num of units from previous update
        private int num_units = 0;					//current num of units
        private bool activePulse = false;			//pulse effect state
        private Vector2 minionGlowBaseScale = new Vector2(0f, 0f);

        private float FADE_TIME = 2.0f;
        private Color FADE_COLOR = Color.blue;
		
        private Timer pulseTimer; //the timer for how long to display the color change for added or lost minions
		
        // Use this for initialization
        void Start()
        {
	        pulseTimer = new Timer ();
	        pulseTimer.Elapsed += (source, args) =>
	        {
    	        activePulse = false;
	        };
			
	        //assert the minion manager exists
	        DebugUtils.Assert(MinionManager.Instance != null, "MinionManager not found. Is it defined?");
        }
		
        void Awake()
        {
            old_num_units = num_units;
        }
		
		// Update is called once per frame
		void Update()
		{
			if (show)
			{
				//current num of swarm units
				num_units = MinionManager.Instance.swarmSize;
				
				//if in danger
				if(num_units > old_num_units)
				{
					if( num_units >= MinionManager.Instance.maxCapacity )
					{
						BeginEffect(maxMinions);
					}
					else
					{
						BeginEffect(addedMinions);
					}
				}
				//if a negative change
				else if (num_units < old_num_units)
				{
					if (num_units <= dangerouslyLowMinionCount)
					{
						BeginEffect(dangerLowMinions);
					}
					else
					{
						BeginEffect(lostMinions);
					}
				}
				// if a positive change
				else if (!activePulse)
				{
					if (num_units <= dangerouslyLowMinionCount)
					{
						BeginEffect(dangerLowMinions);
					}
					//if maxed out
					else if( num_units >= MinionManager.Instance.maxCapacity )
					{
						BeginEffect(maxMinions);
					}
				}
				//update old num of units
				old_num_units = num_units;
			}
		}
		
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FixedHealthbar"/> is show (displayed).
        /// </summary>
        /// <value><c>true</c> if show; otherwise, <c>false</c>.</value>
        public bool Show
        {
            set { show = value; }
            get { return show; }
        }

        private void BeginEffect(GlowEffect effect)
        {
			if(!effect.enabled)
			{
				return;
			}
            switch (effect.style) 
            {
            case (GlowEffect.GlowStyle.FADE):
                ActivateGlowFader(effect);
                break;
            case GlowEffect.GlowStyle.PULSE:
                ActivatePulseFader(effect);
                break;
            case GlowEffect.GlowStyle.CONSTANT:
                ActivateConstantGlow(effect);
                break;
            }
        }

        private void ActivateConstantGlow (GlowEffect effect)
        {
            FADE_COLOR = effect.color;
            clearGlow ();
            foreach (Transform minion in MinionManager.Instance.transform)
            {
                if (minion != null && minion.GetComponent<Minion>().isCollected)
                {
					Transform glow = minion.FindChild("Piranha_glow");
                    minionGlowBaseScale.x = (minionGlowBaseScale.x == 0f) ? glow.localScale.x : minionGlowBaseScale.x;
                    minionGlowBaseScale.y = (minionGlowBaseScale.y == 0f) ? glow.localScale.y : minionGlowBaseScale.y;
                    glow.localScale = new Vector3(effect.scale * minionGlowBaseScale.x, effect.scale * minionGlowBaseScale.y, 1);
                    glow.renderer.enabled = true;
                }
            }
        }
		
        /// <summary>
        /// Activates the glow fader for each active minion.  It will fade over the specified timeInterval
        /// to the specified color.
        /// </summary>
        /// <param name="timeInterval">Time interval.</param>
        /// <param name="glowColor">Glow color.</param>
        private void ActivateGlowFader(GlowEffect effect)
        {
            FADE_COLOR = effect.color;
            FADE_TIME = effect.time;
            clearGlow ();
            foreach (Transform minion in MinionManager.Instance.transform) 
            {
                if (minion != null && minion.GetComponent<Minion>().isCollected)
                {
					Transform glow = minion.FindChild("Piranha_glow");
                    minionGlowBaseScale.x = (minionGlowBaseScale.x == 0f) ? glow.localScale.x : minionGlowBaseScale.x;
                    minionGlowBaseScale.y = (minionGlowBaseScale.y == 0f) ? glow.localScale.y : minionGlowBaseScale.y;
                    glow.localScale = new Vector3(effect.scale * minionGlowBaseScale.x, effect.scale * minionGlowBaseScale.y, 1);
                    glow.renderer.enabled = true;
                    StartCoroutine("FadeTo", glow); //fade out over set time
                }
            }
        }


        private void ActivatePulseFader(GlowEffect effect)
        {
            FADE_COLOR = effect.color; // set the new effect color
            FADE_TIME = effect.time; // set the the new effect time
            clearGlow (); // clear the current effect

            //lock the pulse effect
            activePulse = true;
            pulseTimer.Interval = effect.time * 1000;
            pulseTimer.Start ();

            //iterate each minion activating their glow effect
            foreach (Transform minion in MinionManager.Instance.transform) 
            {
                if (minion != null && minion.GetComponent<Minion>().isCollected)
                {
					Transform glow = minion.FindChild("Piranha_glow");
                    minionGlowBaseScale.x = (minionGlowBaseScale.x == 0f) ? glow.localScale.x : minionGlowBaseScale.x;
                    minionGlowBaseScale.y = (minionGlowBaseScale.y == 0f) ? glow.localScale.y : minionGlowBaseScale.y;
                    glow.renderer.material.color = FADE_COLOR;
                    glow.localScale = new Vector3(effect.scale * minionGlowBaseScale.x, effect.scale * minionGlowBaseScale.y, 1);
                    glow.renderer.enabled = true;
                    StartCoroutine("FadeTo", glow); // fade out
                }
            }
        }


        /// <summary>
        /// Clears the glow effect.
        /// </summary>
        private void clearGlow()
        {
            // stops any current process of the FadeTo effect
            StopCoroutine("FadeTo");
            foreach (Transform minion in MinionManager.Instance.transform) 
            {
                if (minion != null && minion.GetComponent<Minion>().isCollected)
                {
					Transform glow = minion.FindChild("Piranha_glow");
                    glow.renderer.material.color = FADE_COLOR;
                    glow.renderer.enabled = false;
                }
            }
        }

		/// <summary>
		/// Fades the alpha of the specified transform item to the given
		/// alpha value over the given duration of time.
		/// </summary>
		/// <returns>The to.</returns>
		/// <param name="item">Item.</param>
		/// <param name="aValue">A value.</param>
		/// <param name="aTime">A time.</param>
		private IEnumerator FadeTo(Transform item)
		{
			float r = FADE_COLOR.r;
			float g = FADE_COLOR.g;
			float b = FADE_COLOR.b;
			float alpha = FADE_COLOR.a;
			float aTime = FADE_TIME;
			for (float t = 0.0f; t < aTime; t += Time.deltaTime / aTime)
			{
				Color newColor = new Color(r, g, b, Mathf.Lerp(alpha,0.0f,t));
				if(item != null) { item.renderer.material.color = newColor; }
				yield return null;
			}
			if(item != null)
			{
				item.renderer.enabled = false;
				item.renderer.material.color = new Color(r,g,b,1);
			}
		}
	}

    [System.Serializable]
    public sealed class GlowEffect 
    {
        public Color color;
        public float time;
        public float scale;
        public bool  enabled = true;

        public enum GlowStyle 
        {
            FADE,
            PULSE,
            CONSTANT
        }

        public GlowStyle style;

        public GlowEffect(Color c, float t, float _scale, GlowStyle s)
        {
            color = c;
            time = t;
            scale = _scale;
            style = s;
        }
    }
}
