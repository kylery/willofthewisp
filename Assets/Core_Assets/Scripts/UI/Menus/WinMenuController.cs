using UnityEngine;
using Assets.Scripts.Utils;
using UnityEngine.UI;
using Assets.Scripts.UI;

public class WinMenuController : MenuController
{
	private static WinMenuController instance;
	
	public void SetChildrenActive(bool active)
	{
		foreach(Transform child in transform)
		{
			child.gameObject.SetActive(active);
		}
	}
	
	/// <summary>
	/// The current instance of the Pause MenuController.
	/// </summary>
	public static WinMenuController Instance
	{
		get
		{
			if (instance == null)
			{
				instance = (WinMenuController)FindObjectOfType(typeof(WinMenuController));
				if (instance == null)
					UnityEngine.Debug.LogError("Game Over MenuController does not exist");
				DontDestroyOnLoad(instance.gameObject);
			}
			return instance;
		}
	}
}