﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Utils;
using System.Linq;

namespace Assets.Scripts.UI
{
    // <summary>
    // Script for the backdrop menu animition.
    // </summary>
    public class MenuAnimation : MonoBehaviour
    {
        private RectTransform[] locations;
        private ParticleSystem[] emitters;

        /// <summary>
        /// Find all locations and emitters for the menu animation
        /// </summary>
        void Awake()
        {
          //Note this may be improved by moving the rect transform to the emitters
          //themselves and somehow updating the world position based off of the UI
          //position.  It does NOT work to simply change the transform of the
          //emitters to rect transforms.
          Transform locs = transform.Find("Locations");
          locations = new RectTransform[locs.childCount];
          for(int i = 0; i < locs.childCount; i++)
          {
              locations[i] = locs.GetChild(i).GetComponent<RectTransform>();
          }
          emitters = transform.Find("Emitters").GetComponentsInChildren<ParticleSystem>();
          DebugUtils.Assert(locations.Length == emitters.Length, "The number of UI locations and particle emitters are not the same!\n"+
                                                                 "Locations: " + locations.Length + "\n"+
                                                                 "Emitters: " + emitters.Length);
        }

        /// <summary>
        /// When enabled reposition the menu particle emmiters
        /// <summary>
        public void OnEnable()
        {
            for(uint i = 0; i < locations.Length; i++)
            {
                Vector3[] corners = new Vector3[4];
                locations[i].GetComponent<RectTransform>().GetWorldCorners(corners);

                //sum and divide
                Vector3 center = Vector3.zero;
                foreach(Vector3 v in corners) { center += v; }
                center /= 4;

                //take world screen to world position
                center = Camera.main.ScreenToWorldPoint(center);
                emitters[i].transform.position = new Vector3(center.x, center.y, emitters[i].transform.position.z);
            }
        }

        /// <summary>
        /// Manually simulate the particle effects
        /// </summary>
        void Update()
        {
            foreach(ParticleSystem p in emitters)
            {
                p.Simulate(Time.unscaledDeltaTime, true, false);
            }
        }
    }
}
