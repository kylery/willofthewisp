﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// This embedded button is for the bosses that brings up the challenges
    /// and boss level. 
    /// </summary>
    public sealed class EmbeddedBossButton : EmbeddedMenuButton
    {
        [SerializeField]
        private float moveRight;
        [SerializeField]
        private float moveUp;

        /// <summary>
        /// When the button is active, then it will show the
        /// challenges.
        /// </summary>
        private enum ButtonState
        {
            ACTIVE,
            NON_ACTIVE
        }

        private ButtonState state;

        /// <summary>
        /// Is the boss button active.
        /// </summary>
        public bool isActive
        {
            get { return state == ButtonState.ACTIVE; }
        }

        /// <summary>
        /// Set the initial state of the boss button.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            state = ButtonState.NON_ACTIVE;
        }

        /// <summary>
        /// Move the button up. There is a chance that the button can be moved down by setting
        /// the boolean.
        /// </summary>
        /// <param name="goingUp">Should the button move up</param>
        public void MoveUp(bool goingUp = true)
        {
            float up;
            if (goingUp)
                up = moveUp;
            else
                up = -moveUp;
            transform.position += new Vector3(0, up, 0);
        }

        /// <summary>
        /// Move the button right. There is a chance that the button can be moved left by setting
        /// the boolean.
        /// </summary>
        /// <param name="goingUp">Should the button move right</param>
        public void MoveRight(bool goingRight = true)
        {
            float right;
            if (goingRight)
                right = moveRight;
            else
                right = -moveRight;
            transform.position += new Vector3(right, 0, 0);
        }

        /// <summary>
        /// Set the button state to active.
        /// </summary>
        public void Activate()
        {
            SetAppearOnHighlightActive(false);
            state = ButtonState.ACTIVE;
        }

        /// <summary>
        /// Set the button state to non-active.
        /// </summary>
        public void Deactivate()
        {
            Unhighlight();
            state = ButtonState.NON_ACTIVE;
        }

        /// <summary>
        /// Handle the highlighting of the boss when the player enters the
        /// hit box.
        /// </summary>
        protected override void OnTriggerEnter2D(Collider2D coll)
        {
            if (state == ButtonState.NON_ACTIVE)
                base.OnTriggerEnter2D(coll);
        }

        /// <summary>
        /// Handle when the player leaves the hit box.
        /// </summary>
        protected override void OnTriggerExit2D(Collider2D coll)
        {
            if(state == ButtonState.NON_ACTIVE)
                base.OnTriggerExit2D(coll);
        }

        /// <summary>
        /// This is the same as OnTriggerEnter2D except it will handle the case where
        /// the user uses the mouse.
        /// </summary>
        protected override void OnMouseEnter()
        {
            if (state == ButtonState.NON_ACTIVE)
                base.OnMouseEnter();
        }

        /// <summary>
        /// This is the same as OnTriggerExit2D except it will handle the case where
        /// the user uses the mouse.
        /// </summary>
        protected override void OnMouseExit()
        {
            if (state == ButtonState.NON_ACTIVE)
                base.OnMouseExit();
        }

        /// <summary>
        /// Highlight the button.
        /// </summary>
        protected override void Highlight()
        {
            if (!highlighted)
                MoveRight();

            base.Highlight();
        }

        /// <summary>
        /// De-highlight the button.
        /// </summary>
        public override void Unhighlight()
        {
            if (!highlighted)
                return;

            base.Unhighlight();

            MoveRight(false);
        }
    }
}
