using UnityEngine;
using Assets.Scripts.Utils;
using UnityEngine.UI;
using Assets.Scripts.UI;

public class GameOverMenuController : MenuController
{
	private static GameOverMenuController go_instance;

	public void SetChildrenActive(bool active)
	{
		foreach(Transform child in transform)
		{
			child.gameObject.SetActive(active);
		}
	}

	/// <summary>
	/// The current instance of the Pause MenuController.
	/// </summary>
	public static GameOverMenuController Instance
	{
		get
		{
			if (go_instance == null)
			{
				go_instance = (GameOverMenuController)FindObjectOfType(typeof(GameOverMenuController));
				if (go_instance == null)
					UnityEngine.Debug.LogError("Game Over MenuController does not exist");
				DontDestroyOnLoad(go_instance.gameObject);
			}
			return go_instance;
		}
	}
}