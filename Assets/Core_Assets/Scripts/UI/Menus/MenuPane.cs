using UnityEngine;
using Assets.Scripts.Utils;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Assets.Scripts;
using Assets.Scripts.UI;
using System.Collections.Generic;
using System.Timers;

public class MenuPane : MonoBehaviour {
	private GameObject menu;							//the underlying menu GameObject
	private List<Selectable> selectables = new List<Selectable>();	//the selectable UI components on this pane
	private Selectable selected;							//the currently selected button
	private MenuController mc;							//the controller of this menu pane
	
	private Timer AllowMoveTimer;						//timer control between movements
	private bool allowedToMove;							//the timer flag


	//The type of menu
	public enum MenuType
	{
		INDEX,
		RESTART_CONFIRM,
		MAINMENU_CONFIRM,
		QUIT_CONFIRM,
		OPTIONS,
        CONTROLLER_SELECT
	}
	public MenuType type;

	/// <summary>
	/// Initialize this menu pane.
	/// </summary>
	public void Initialize()
	{
		menu = transform.gameObject;
		allowedToMove = true;
		AllowMoveTimer = new Timer ();
		AllowMoveTimer.Elapsed += (source, args) =>
		{
			allowedToMove = true;
		};
		SetupSelectables ();
        ApplyMenuTheme();
	}

	/// <summary>
	/// Gets or sets the menu controller.
	/// </summary>
	/// <value>The MenuController.</value>
	public MenuController Controller
	{
		get 
		{ 
			if (mc == null)
			{
				mc = GetComponentInParent<MenuController> ();
			}
			return mc;
		}
		set 
		{
			mc = value;
		}
	}

	/// <summary>
	/// Gets the underlying menu object.
	/// </summary>
	/// <value>The menu.</value>
	public GameObject Menu
	{
		get 
		{
			if(menu == null)
				menu = transform.gameObject;
			return menu;
		}
	}


	/// <summary>
	/// Gets the list of UI components.
	/// </summary>
	/// <value>The selectables.</value>
	public List<Selectable> Selectables
	{
		get {return selectables;}
	}


	/// <summary>
	/// Gets or sets the selected button.
	/// </summary>
	/// <value>The selected.</value>
	public Selectable Selected
	{
		get 
		{ 
			if(selected == null)
			{
                selected = selectables[0];
				selected.Select();
			}
			return selected;
		}
		set
		{
            if(selected != null)
                DehighlightSelected();
			selected = value;
            HighlightSelected();
			selected.Select();
		}
	}


	/// <summary>
	/// Gets a value indicating whether button selection can change.
	/// </summary>
	/// <value><c>true</c> if button selection can change; otherwise, <c>false</c>.</value>
	public bool CanMove
	{
		get
		{
			return allowedToMove;
		}
	}

    /// <summary>
    /// Highlights the selected menu item
    /// </summary>
    private void DehighlightSelected()
    {

        Transform highlight = Selected.transform.GetChild(1);
        DebugUtils.Assert(highlight != null, "Highlight game object couldn't be established for Selected menu item.");

        highlight.gameObject.SetActive(false);
    }


    /// <summary>
    /// Highlights the selected menu item
    /// </summary>
    private void HighlightSelected()
    {
        Transform highlight = Selected.transform.GetChild(1);
        DebugUtils.Assert(highlight != null, "Highlight game object couldn't be established for Selected menu item.");

        highlight.gameObject.SetActive(true);
    }


	/// <summary>
	/// Clears the button list.
	/// </summary>
	private void ClearSelectables()
	{
		selectables = new List<Selectable> ();
	}


	/// <summary>
	/// Adds a list of UI selectable components to the current list of selectables.
	/// </summary>
	/// <param name="_selectables">_selectables.</param>
	private void AddSelectables(List<Button> _selectables)
	{
		foreach(Selectable s in _selectables)
		{
			selectables.Add(s);
		}
	}


	/// <summary>
	/// Adds a button to the list of selectables.
	/// </summary>
	/// <param name="b">The selectable to add.</param>
	private void AddSelectable(Selectable s)
	{
		selectables.Add (s);
	}


	/// <summary>
	/// Starts the timer.
	/// </summary>
	private void StartTimer()
	{
		allowedToMove = false;
		AllowMoveTimer.Interval = 150;
		AllowMoveTimer.Start ();
	}


	/// <summary>
	/// Sets the menu (GameObject) to be active and highlights the first button.
	/// </summary>
	/// <param name="active">If set to <c>true</c> active.</param>
	public void SetMenuActive(bool active)
	{
		Menu.SetActive (active);
        if (type == MenuType.CONTROLLER_SELECT)
        {
            Selectable kb = null, gc = null;  //keyboard button, game controller button
            foreach (Selectable s in selectables)
            {
                if (s.name == "KeyboardButton")
                {
                    kb = s;
                }
                else if (s.name == "ControllerButton")
                {
                    gc = s;
                }
            }
            if (GameManager.Instance.UsingController && gc != null)
            {
                Selected = gc;
            }
            else if (!GameManager.Instance.UsingController && kb != null)
            {
                Selected = kb;
            }
            else
            {
                Selected = selectables[0];
            }
        }
        else
        {
            Selected = selectables[0];
        }
    }


	/// <summary>
	/// Moves the button selection to the next nearest button in the up direction.
	/// </summary>
	public void MoveUp()
	{
		if (!allowedToMove)
			return;
		
		Selectable nearest = null;
		foreach(Selectable s in selectables)
		{
			if(s == Selected || (s.transform.position.y <= (Selected.transform.position.y)))
			{
				continue;
			}
			if(nearest == null)
			{
				nearest = s;
				continue;
			}
			if (Mathf.Abs (s.transform.position.y - Selected.transform.position.y) < Mathf.Abs (nearest.transform.position.y - Selected.transform.position.y))
			{
				nearest = s;
			}
		}
		if (nearest == null)
		{
			nearest = Selected;
		}
		Selected = nearest;
		StartTimer ();
	}


	/// <summary>
	/// Moves the button selection to the next nearest button in the down direction.
	/// </summary>
	public void MoveDown()
	{
		if (!allowedToMove)
			return;

		Selectable nearest = null;
		foreach(Selectable s in selectables)
		{
			if(s == Selected || s.transform.position.y >= (Selected.transform.position.y))
			{
				continue;
			}
			if(nearest == null)
			{
				nearest = s;
				continue;
			}
			if (Mathf.Abs (s.transform.position.y - Selected.transform.position.y) < Mathf.Abs (nearest.transform.position.y - Selected.transform.position.y))
			{
				nearest = s;
			}
		}
		if (nearest == null)
		{
			nearest = Selected;
		}
		Selected = nearest;
		StartTimer ();
	}


	/// <summary>
	/// Moves the button selection to the next nearest button in the right direction.
	/// </summary>
	public void MoveRight()
	{
		if (!allowedToMove)
			return;

		//Slider
		if (Selected.GetType() == typeof(Slider))
		{
			Slider s = (Slider)Selected;
			s.value += s.maxValue * 0.125f;
		}
		else //button, checkbox, etc.
		{
			Selectable nearest = null;
			foreach(Selectable s in selectables)
			{
				if(s == Selected || s.transform.position.x <= (Selected.transform.position.x))
				{
					continue;
				}
				if(nearest == null)
				{
					nearest = s;
					continue;
				}
				if (Mathf.Abs (s.transform.position.y - Selected.transform.position.y) < Mathf.Abs (nearest.transform.position.y - Selected.transform.position.y))
				{
					nearest = s;
				}
			}
			if (nearest == null)
			{
				nearest = Selected;
			}
			Selected = nearest;
		}
		StartTimer ();
	}


	/// <summary>
	/// Moves the button selection to the next nearest button in the left direction.
	/// </summary>
	public void MoveLeft()
	{
		if (!allowedToMove)
			return;

		//Slider
		if (Selected.GetType() == typeof(Slider))
		{
			Slider s = (Slider)Selected;
			s.value -= s.maxValue * 0.125f;
		}
		else //button, checkbox, etc.
		{
			Selectable nearest = null;
			foreach(Selectable s in selectables)
			{
				if(s == Selected || s.transform.position.x >= (Selected.transform.position.x))
				{
					continue;
				}
				if(nearest == null)
				{
					nearest = s;
					continue;
				}
				if (Mathf.Abs (s.transform.position.y - Selected.transform.position.y) < Mathf.Abs (nearest.transform.position.y - Selected.transform.position.y))
				{
					nearest = s;
				}
			}
			if (nearest == null)
			{
				nearest = Selected;
			}
			Selected = nearest;
		}
		StartTimer ();
	}


	/// <summary>
	/// Sets up the button list.
	/// </summary>
	private void SetupSelectables() 
	{
		ClearSelectables (); 			//clear the list
		
		SearchForSelectablesInChildren (Menu);
	}

	private void SearchForSelectablesInChildren(GameObject parent)
	{
		for(int i = 0; i < parent.transform.childCount; i ++)
		{
			Transform child = parent.transform.GetChild(i);
			Selectable s = child.GetComponent<Selectable>();				//get the button
			ButtonType bType = child.GetComponent<ButtonType>();			//get the button type
			
			if(s != null)
			{
				AddSelectable(s);							//add it to the selectable list
				if((s.GetType() == typeof(Button)) && bType != null)
				{
					Button b = (Button)s;
					RegisterButtonEvent(b, bType);				//Register on click events
				}
			}
			else if(child.childCount > 0)
			{
				SearchForSelectablesInChildren(child.gameObject);
			}
		}
	}


	/// <summary>
	/// Applies the theme established by the UIManager
	/// </summary>
	private void ApplyMenuTheme()
	{
		Sprite s = UIManager.Instance.GetPauseMenuThemeSprite();
        if (s != null)
        {
            GetComponent<Image>().sprite = s;
        }
	}


	/// <summary>
	/// Registers the button events for the button based, on the given button type
	/// </summary>
	/// <param name="b">The button to which to apply the event.</param>
	/// <param name="buttonType">Button type.</param>
	private void RegisterButtonEvent(Button b, ButtonType buttonType)
	{
		switch (buttonType.type) 
		{
		case ButtonType.TYPE.BACK:
			b.onClick.AddListener(() => { 
				Controller.MenuBack();
			});
			break;
		case ButtonType.TYPE.SHOW_RESTART:
			b.onClick.AddListener(() => {
				Controller.ShowMenuType(MenuType.RESTART_CONFIRM);
			});
			break;
		case ButtonType.TYPE.SHOW_OPTIONS:
			b.onClick.AddListener(() => {
				Controller.ShowMenuType(MenuType.OPTIONS);
			});
			break;
        case ButtonType.TYPE.SHOW_CONTROLLER_SELECT:
            b.onClick.AddListener(() =>
            {
                Controller.ShowMenuType(MenuType.CONTROLLER_SELECT);
            });
            break;
		case ButtonType.TYPE.SHOW_MAINMENU:
			b.onClick.AddListener(() => {
				Controller.ShowMenuType(MenuType.MAINMENU_CONFIRM);
			});
			break;
		case ButtonType.TYPE.SHOW_QUIT:
			b.onClick.AddListener(() => {
				Controller.ShowMenuType(MenuType.QUIT_CONFIRM);
			});
			break;
		case ButtonType.TYPE.RESTART_CONFIRM:
			b.onClick.AddListener(() => {
				Controller.RestartLevel();
			});
			break;
		case ButtonType.TYPE.QUIT_CONFIRM:
			b.onClick.AddListener(() => {
				Controller.QuitGame();
			});
			break;
		case ButtonType.TYPE.MAINMENU_CONFIRM:
			b.onClick.AddListener(() => {
				Controller.QuitToMainMenu();
			});
			break;
        case ButtonType.TYPE.KEYBOARD:
            b.onClick.AddListener(() =>
            {
                GameManager.Instance.SetUsingController(false);
                Controller.MenuBack();
            });
            break;
        case ButtonType.TYPE.CONTROLLER:
            b.onClick.AddListener(() =>
            {
                GameManager.Instance.SetUsingController(true);
                Controller.MenuBack();
            });
            break;
		}
	}
}