using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Assets.Scripts.Utils;
using Assets.Scripts.Entities.Player;
using Assets.Scripts;

public class ReticleToggle : MonoBehaviour
{
	private Toggle reticle_toggle;
    private Player playerScript;

	//Initializes the toggle value to that of the emitter attached to the player
	private void Awake ()
	{
		reticle_toggle = GetComponent<Toggle> ();
	}

    void Start()
    {
        GameObject player = PlayerManager.Instance.Player;
		DebugUtils.Assert (reticle_toggle != null, "The reticle toggle couldn't be found");
		DebugUtils.Assert (player != null, "The player couldn't be found");
        playerScript = player.GetComponent<Player>();
        DebugUtils.Assert(playerScript != null, "Player should have player script attached.");
		reticle_toggle.isOn = playerScript.ReticleOn;
    }

	//Sets the player particle emitter to the value of the toggle 
	private void Update ()
	{
		GameObject player = PlayerManager.Instance.Player;
		if (player == null)
			return;
        if (reticle_toggle.isOn)
            playerScript.TurnOnCursor();
        else
            playerScript.TurnOffCursor();
	}
}

