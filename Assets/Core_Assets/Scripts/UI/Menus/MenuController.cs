using UnityEngine;
using Assets.Scripts.Utils;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Assets.Scripts;
using System.Collections.Generic;
using System.Timers;
using Assets.Scripts.UI;

public abstract class MenuController : MonoBehaviour 
{
	protected Stack<MenuPane> ActiveMenuStack;	//The menu stack

	protected Dictionary<MenuPane.MenuType, MenuPane> sub_menus; //the menus controlled by this controller
	
	protected bool displayed = false;				//is this controller currently displaying antyhing

    //the input manager is needed to override joystick control of the menu
    protected InputManager inputManager;

    [SerializeField]
    protected GameObject menuAnimation;

    protected float JoystickSensitvity = 0.7f; //How hard the player has to push the joystick in any direction to be effective


	// Use this for initialization
	protected virtual void Start () 
    {
		//need to know where the canvas is 
		GameObject canvas = GameObject.FindGameObjectWithTag ("Canvas"); 
		DebugUtils.Assert (canvas != null, "There is no UI Canvas, and thus no need for MenuControllers.");

        //get the input manager
        inputManager = InputManager.Instance;

		//make sure the parent of the menu controller is the UI canvas
		if (transform.parent != canvas.transform)
		{
			transform.SetParent(canvas.transform);
		}

		//nothing currently displayed
		displayed = false;

		//The stack is empty
		ActiveMenuStack = new Stack<MenuPane> ();

        //Initialize the empty menu list
        sub_menus = new Dictionary<MenuPane.MenuType, MenuPane>();

        //The sub menus should be children of the controller
        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject go = transform.GetChild(i).gameObject;

            //if the current object is a menu pane
            MenuPane mp = go.GetComponent<MenuPane>();

            // if null, next
            if (mp == null) continue;

            //setup the sub menu dictionary based on menu pane types
            switch (mp.type)
            {
                case MenuPane.MenuType.INDEX:
                    sub_menus[MenuPane.MenuType.INDEX] = mp;
                    break;
                case MenuPane.MenuType.OPTIONS:
                    sub_menus[MenuPane.MenuType.OPTIONS] = mp;
                    break;
                case MenuPane.MenuType.CONTROLLER_SELECT:
                    sub_menus[MenuPane.MenuType.CONTROLLER_SELECT] = mp;
                    break;
                case MenuPane.MenuType.RESTART_CONFIRM:
                    sub_menus[MenuPane.MenuType.RESTART_CONFIRM] = mp;
                    break;
                case MenuPane.MenuType.MAINMENU_CONFIRM:
                    sub_menus[MenuPane.MenuType.MAINMENU_CONFIRM] = mp;
                    break;
                case MenuPane.MenuType.QUIT_CONFIRM:
                    sub_menus[MenuPane.MenuType.QUIT_CONFIRM] = mp;
                    break;
            }
        }

        //initialize the sub-menus
        InitializeSubMenus();
	}

    protected virtual void Update()
    {
		//if joystick value is LEFT
		if((inputManager.GetLeftJoystickVector().x < -(JoystickSensitvity) || Input.GetKeyUp(KeyCode.LeftArrow))
            && displayed && sub_menus.Count > 0)
		{
			MenuPane mp = ActiveMenuStack.Peek();		//top menu
			DebugUtils.Assert(mp != null, "There is no currently active menu pane");
			mp.MoveLeft();	//move left on the menu pane if possible
		}
		//if joystick value is RIGHT
        else if ((inputManager.GetLeftJoystickVector().x > (JoystickSensitvity)  || Input.GetKeyUp(KeyCode.RightArrow))
            && displayed && sub_menus.Count > 0)
		{
			MenuPane mp = ActiveMenuStack.Peek();		//top menu
			DebugUtils.Assert(mp != null, "There is no currently active menu pane");
			mp.MoveRight(); //move right on the menu pane if possible
		}
		//if joystick value is UP
        else if ((inputManager.GetLeftJoystickVector().y > (JoystickSensitvity) || Input.GetKeyUp(KeyCode.UpArrow))
            && displayed && sub_menus.Count > 0)
		{
			MenuPane mp = ActiveMenuStack.Peek();		//top menu
			DebugUtils.Assert(mp != null, "There is no currently active menu pane");
			mp.MoveUp(); //move up on the menu pane if possible
		}
		//if joystick value is DOWN
        else if ((inputManager.GetLeftJoystickVector().y < -(JoystickSensitvity) || Input.GetKeyUp(KeyCode.DownArrow)) 
            && displayed && sub_menus.Count > 0)
		{
			MenuPane mp = ActiveMenuStack.Peek();		//top menu
			DebugUtils.Assert(mp != null, "There is no currently active menu pane");
			mp.MoveDown(); //move down on the menu pane if possible
		}
        //else if ((Input.GetKeyUp(KeyCode.Space) || Input.GetKeyUp(KeyCode.KeypadEnter))&& displayed && sub_menus.Count > 0)
        //{
        //    Debug.Log("Space pressed in pause");
        //    MenuPane mp = ActiveMenuStack.Peek();		//top menu
        //    DebugUtils.Assert(mp != null, "There is no currently active menu pane");
        //    Debug.Log (mp.Selected.name);
        //    var pointer = new PointerEventData(EventSystem.current);
        //    ExecuteEvents.Execute(mp.Selected.gameObject, pointer, ExecuteEvents.submitHandler);
        //}
		
		//if back is pressed
        if ((inputManager.GetButtonUp(InputManager.ButtonTypes.B) || Input.GetKeyUp(KeyCode.Backspace)) && displayed && sub_menus.Count > 0)
		{
			MenuBack();  //go back
		}
    }


    /// <summary>
    /// Activate the menu backdrop animation
    /// </summary>
    public virtual void ShowMenuBackAnimation()
    {
        if(menuAnimation != null)
        {
            menuAnimation.SetActive(true);
        }
    }

    /// <summary>
    /// Deactivate the menu backdrop animation
    /// </summary>
    public virtual void HideMenuBackAnimation()
    {
        if (menuAnimation != null)
        {
            menuAnimation.SetActive(false);
        }
    }


	/// <summary>
	/// Shows the menu pane given.
	/// </summary>
	/// <param name="mp">Mp.</param>
	public void ShowMenu(MenuPane mp)
	{
		displayed = true;
		HideAllMenus ();
		DebugUtils.Assert (mp != null, "The menu '" + mp.name + "' isn't in this menu system.");
		mp.SetMenuActive (true);
		ActiveMenuStack.Push (mp);
	}

	/// <summary>
	/// Shows the menu of the given type
	/// </summary>
	/// <param name="mType">M type.</param>
	public void ShowMenuType(MenuPane.MenuType mType)
	{
		displayed = true;
		HideAllMenus ();
		MenuPane mp = sub_menus [mType];
		DebugUtils.Assert (mp != null, "The menu type '" + mType + "' isn't in this menu system.");
        mp.SetMenuActive(true);
		ActiveMenuStack.Push (mp);
	}

	/// <summary>
	/// Pops the menu stack, displaying the previous menu.
	/// </summary>
	public void MenuBack()
    {
		if(ActiveMenuStack.Count <= 1)
		{
			HideAllMenusAndResume();
			return;
		}
		ActiveMenuStack.Pop ();
		MenuPane mp = ActiveMenuStack.Pop ();
		ShowMenu (mp);
	}

	/// <summary>
	/// Executes necessary steps to unpause.
	/// </summary>
	public virtual void Unpause() 
    {
        HideMenuBackAnimation();
        ActiveMenuStack.Clear();
		displayed = false;
		HideAllMenus ();
	}

	/// <summary>
	/// Hides all menus.
	/// </summary>
	public void HideAllMenus() 
    {
		MenuPane[] menus = new MenuPane[sub_menus.Count];
		sub_menus.Values.CopyTo (menus, 0);
		for(int i = 0; i < menus.Length; i ++)
		{
            MenuPane mp = menus[i];
            if(mp.gameObject.activeSelf)
            {
                mp.gameObject.SetActive(false);
                //mp.gameObject.GetComponent<Image>().enabled = false;
            }
            //menus[i].SetActive(false);
		}
	}

	/// <summary>
	/// Hides all menus and resumes.
	/// </summary>
	public virtual void HideAllMenusAndResume() 
    {
        HideMenuBackAnimation();
        ActiveMenuStack.Clear();
		displayed = false;
		HideAllMenus ();
    	GameManager.Instance.ResumeGame ();
	}

	/// <summary>
	/// Restarts the level.
	/// </summary>
	public void RestartLevel()
    {
		HideAllMenusAndResume ();
		GameManager.Instance.ChangeLevel (Application.loadedLevelName);
	}

	/// <summary>
	/// Quits to main menu.
	/// </summary>
	public void QuitToMainMenu()
    {
		HideAllMenusAndResume();
		GameManager.Instance.ChangeLevel (Constants.MAIN_MENU_LEVEL);
	}

	/// <summary>
	/// Quits the game.
	/// </summary>
	public void QuitGame() 
    {
		Application.Quit ();
	}

    /// <summary>
    /// Initializes the sub menus.
    /// </summary>
    public void InitializeSubMenus()
    {
        MenuPane[] menus = new MenuPane[sub_menus.Count];
        sub_menus.Values.CopyTo(menus, 0);
        for (int i = 0; i < menus.Length; i++)
        {
            menus[i].Initialize();
        }
    }
}
