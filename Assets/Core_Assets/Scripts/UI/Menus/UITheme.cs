﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Utils;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// The UI Manager is communication from the UI to the main game that all the
    /// UI elements can access to gain information. Core Game should not access
    /// the UI, but the UI should get most of its info from Game Managaer. The UI
    /// Manager also contains useful prefabs so that UI elements don't have to deal
    /// with the storage.
    /// </summary>
    public class UITheme : MonoBehaviour
    {
        [SerializeField]
        private UIManager.THEME PauseMenuTheme = UIManager.THEME.ARCTIC; 	//default arctic

        private static UITheme instance = null;

        /// <summary>
        /// Current instance of the UI Theme, may be null.
        /// </summary>
        public static UITheme Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject UI_Theme = GameObject.Find("UI_Theme");
                    if(UI_Theme != null)
                    {
                        instance = UI_Theme.GetComponent<UITheme>();
                    }
                    //Don't check for null again, null allowed
                }
                return instance;
            }
        }

        public UIManager.THEME GetTheme()
        {
            return PauseMenuTheme;
        }
    }

}