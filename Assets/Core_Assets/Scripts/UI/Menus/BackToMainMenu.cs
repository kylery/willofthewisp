﻿﻿using UnityEngine;
using System.Collections;

using Assets.Scripts.Utils;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// A quick way to always get back to the main menu using the B button.
    /// </summary>
    public class BackToMainMenu : MonoBehaviour
    {
        void Update()
        {
            if (InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.B) || Input.GetKeyUp(KeyCode.Escape) || Input.GetKeyUp(KeyCode.Backspace))
                GameManager.Instance.ChangeLevel(Constants.MAIN_MENU_LEVEL);
        }
    }
}
