﻿using System;
using UnityEngine;

using Assets.Scripts.Utils;

namespace Assets.Scripts.UI.Menus
{
    /// <summary>
    /// A quick way to restart the level by pressing A.
    /// </summary>
    public class RestartLevel : MonoBehaviour
    {
        void Update()
        {
            if (InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.A) || Input.GetKeyUp(KeyCode.Space))
                GameManager.Instance.ChangeLevel(Application.loadedLevelName);
        }
    }
}
