﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Assets.Scripts.Utils;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// The embedded menu mirrors the menu system used in the canvas except it stays in the
    /// same position as the player can wander around the level. It also has functionality to
    /// detect if the player is hovering over the button to use as a cursor.
    /// </summary>
    public class EmbeddedMenu : MonoBehaviour
    {
        [SerializeField]
        private GameObject mainMenu;
        [SerializeField]
        private GameObject optionsMenu;
        [SerializeField]
        private GameObject playMenu;
        [SerializeField]
        private GameObject turtleChallenges;
        [SerializeField]
        private GameObject shorcaChallenges;
		[SerializeField]
		private GameObject challengePanel;
        [SerializeField]
        private AudioClip highlightClip;
        private AudioSource sound;

        private GameManager gameManager;
        private GameObject[] menus;

        /// <summary>
        /// This will set up the menu system.
        /// </summary>
        void Start()
        {
            playMenu.SetActive(true);
            ShowMainMenu();
            gameManager = GameManager.Instance;
            menus = transform.Cast<Transform>().Select(t => t.gameObject).ToArray();
            GetMenuButtons();

            sound = gameObject.GetComponent<AudioSource>();
        }

        /// <summary>
        /// This will hook up each button with a delegate.
        /// </summary>
        private void GetMenuButtons()
        {
            foreach (GameObject menu in menus)
            {
                for (int index = 0; index < menu.transform.childCount; index++)
                {
                    Transform button = menu.transform.GetChild(index);
                    switch (button.name.ToLower())
                    {
                        case "backbutton":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                ShowMainMenu();
                                button.GetComponent<EmbeddedMenuButton>().Unhighlight();
                            };
                            break;
                        case "turtle":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                EmbeddedBossButton bossButton = button.GetComponent<EmbeddedBossButton>();
                                if (!bossButton.isActive)
                                {
                                    HideOrca();
                                    bossButton.MoveUp();
                                    bossButton.Activate();
									ChangeBosstoChallengeLabel();
									ShowChallengePanel();
                                    ShowTurtleChallenges();
                                }
                            };
                            button.GetComponent<EmbeddedMenuButton>().onClickB = () =>
                            {
                                EmbeddedBossButton bossButton = button.GetComponent<EmbeddedBossButton>();
                                if (bossButton.isActive)
                                {
                                    ShowOrca();
                                    bossButton.MoveUp(false);
                                    bossButton.Deactivate();
									ChangeChallengetoBossLabel();
									HideChallengePanel();
                                    HideTurtleChallenges();
                                }
                            };
                            break;
                        case "orca":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                EmbeddedBossButton bossButton = button.GetComponent<EmbeddedBossButton>();
                                if (!bossButton.isActive)
                                {
                                    HideTurtle();
                                    bossButton.MoveUp();
                                    bossButton.Activate();
								    ChangeBosstoChallengeLabel();
									ShowChallengePanel();
								    ShowShorcaChallenges();
                                }
                            };
                            button.GetComponent<EmbeddedMenuButton>().onClickB = () =>
                            {
                                EmbeddedBossButton bossButton = button.GetComponent<EmbeddedBossButton>();
                                if (bossButton.isActive)
                                {
                                    ShowTurtle();
                                    bossButton.MoveUp(false);
                                    bossButton.Deactivate();
									ChangeChallengetoBossLabel();
									HideChallengePanel();
                                    HideOrcaChallenges();
                                }
                            };
                            break;
                        case "turtlechallenges":
                            TurtleChallenges(button);
                            break;
                        case "shorcachallenges":
                            ShorcaChallenges(button);
                            break;
                        case "playbutton":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                ShowPlayMenu();
                                button.GetComponent<EmbeddedMenuButton>().Unhighlight();
                            };
                            break;
                        case "creditbutton":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                ShowCredits();
                                button.GetComponent<EmbeddedMenuButton>().Unhighlight();
                            };
                            break;
                        case "optionsbutton":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                ShowOptionsMenu();
                                button.GetComponent<EmbeddedMenuButton>().Unhighlight();
                            };
                            break;
                        case "exitbutton":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                QuitGame();
                            };
                            break;
                    }
                }
            }
        }

        public void ShowMainMenu()
        {
            HideAllMenus();
            mainMenu.SetActive(true);
        }

        public void ShowOptionsMenu()
        {
            optionsMenu.GetComponent<SelectControllerMenuController>().ShowMenuType(MenuPane.MenuType.INDEX);
        }

        public void ShowPlayMenu()
        {
            HideAllMenus();
            playMenu.SetActive(true);
        }

        private void HideTurtleChallenges()
        {
            foreach(Transform button in turtleChallenges.transform)
            {
                button.gameObject.GetComponent<EmbeddedMenuButton>().Unhighlight();
				DisplayTrophies (button, false);
            }
            turtleChallenges.SetActive(false);
        }

        private void HideOrcaChallenges()
        {
            foreach (Transform button in shorcaChallenges.transform)
            {
                button.gameObject.GetComponent<EmbeddedMenuButton>().Unhighlight();
				DisplayTrophies (button, false);
            }
            shorcaChallenges.SetActive(false);
        }

		public void DisplayTrophies(Transform button, Boolean on)
		{
			button.gameObject.GetComponent<EmbeddedMenuButton>().Unhighlight();
			Transform mesh = GameObjectUtils.GetChild(button.gameObject, "Mesh");
			if (mesh != null)
			{
				if (gameManager.LevelBeaten (button.gameObject.GetComponent<EmbeddedMenuButton>().GetLevelString()))
				{
					Transform trophy = GameObjectUtils.GetChild(mesh.gameObject, "Trophy");
					if (trophy != null)
					{
						trophy.GetComponent<SpriteRenderer>().enabled = on;
					}
				}
			}
		}

		public void ShowChallengePanel()
		{
			if (challengePanel != null) 
			{
				challengePanel.SetActive(true);
			}
		}
		public void HideChallengePanel()
		{
			if (challengePanel != null) 
			{
				challengePanel.SetActive (false);
			}
		}

        public void HideAllMenus()
        {
            mainMenu.SetActive(false);
            playMenu.SetActive(false);
        }

        public void PlayTurtle()
        {
            gameManager.ChangeLevel(Constants.TURTLE_BOSS_LEVEL);
        }

        public void PlayShorca()
        {
            gameManager.ChangeLevel(Constants.SHORCA_BOSS_LEVEL);
        }

        public void ShowCredits()
        {
            gameManager.ChangeLevel(Constants.CREDITS_LEVEL);
        }

		public void ChangeBosstoChallengeLabel()
		{
			Transform BossLabel = playMenu.transform.Find ("BossLabel");
			Transform SelectChallengeLabel = playMenu.transform.Find ("SelectChallengeLabel");
			BossLabel.gameObject.SetActive (false);
			SelectChallengeLabel.gameObject.SetActive (true);
		}

		public void ChangeChallengetoBossLabel()
		{
			Transform BossLabel = playMenu.transform.Find ("BossLabel");
			Transform SelectChallengeLabel = playMenu.transform.Find ("SelectChallengeLabel");
			SelectChallengeLabel.gameObject.SetActive (false);
			BossLabel.gameObject.SetActive (true);
		}
		
        public void ShowTurtleChallenges()
        {
            turtleChallenges.SetActive(true);
			foreach(Transform button in turtleChallenges.transform)
			{
				DisplayTrophies (button, true);
			}
        }

        public void ShowShorcaChallenges()
        {
            shorcaChallenges.SetActive(true);
			foreach(Transform button in shorcaChallenges.transform)
			{
				DisplayTrophies (button, true);
			}
        }

        public void ShowTurtle()
        {
            Transform turtle = GameObjectUtils.GetChild(playMenu, "turtle");
            ShowBoss(turtle);
        }

        public void HideTurtle()
        {
            Transform turtle = GameObjectUtils.GetChild(playMenu, "turtle");
            HideBoss(turtle);
        }

        public void ShowOrca()
        {
            Transform shorca = GameObjectUtils.GetChild(playMenu, "orca");
            ShowBoss(shorca);
        }

        public void HideOrca()
        {
            Transform shorca = GameObjectUtils.GetChild(playMenu, "orca");
            HideBoss(shorca);
        }

        public void DeactivateTurtle()
        {
            Transform turtle = GameObjectUtils.GetChild(playMenu, "turtle");
            DeactivateBoss(turtle);
            HideTurtleChallenges();
        }

        public void DeactivateOrca()
        {
            Transform orca = GameObjectUtils.GetChild(playMenu, "orca");
            DeactivateBoss(orca);
            HideOrcaChallenges();
        }

        public void QuitGame()
        {
            Application.Quit();
        }

        /// <summary>
        /// Handle the buttons for the turtle boss depending on main level or challenges.
        /// </summary>
        public void TurtleChallenges(Transform challenge)
        {
            foreach (Transform button in challenge)
            {
                switch (button.name.ToLower())
                {
                    case "turtlebutton":
                        button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                        {
                            GameObject go = (GameObject)GameObject.Instantiate(UIManager.Instance.TurtleTutorialPrefab);
                            go.GetComponent<SwipePanels>().Init(true, Constants.TURTLE_BOSS_LEVEL);
                        };
                        button.GetComponent<EmbeddedMenuButton>().onClickY = () =>
                        {
                            PlayTurtle();
                        };
                        break;
                    case "turtlechallenge1":
                        button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                        {
                            gameManager.ChangeLevel(Constants.TURTLE_CHALLENGE_ONE_LEVEL);
                        };
                        break;
                    case "turtlechallenge2":
                        button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                        {
                            gameManager.ChangeLevel(Constants.TURTLE_CHALLENGE_TWO_LEVEL);
                        };
                        break;
                    case "turtlechallenge3":
                        button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                        {
                            gameManager.ChangeLevel(Constants.TURTLE_CHALLENGE_THREE_LEVEL);
                        };
                        break;
                    default:
                        if (Constants.TRIAL_MODE)
                        {
                            EmbeddedMenuButton menuButton = button.GetComponent<EmbeddedMenuButton>();
                            menuButton.TurnOnTrialMode();
                        }
                        break;
                }
            }
        }

        /// <summary>
        /// Handle the buttons for the shorca boss depending on main level or challenges.
        /// </summary>
        public void ShorcaChallenges(Transform challenge)
        {
            foreach (Transform button in challenge)
            {
                if (!Constants.TRIAL_MODE)
                {
                    switch (button.name.ToLower())
                    {
                        case "shorcabutton":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                GameObject go = (GameObject)GameObject.Instantiate(UIManager.Instance.ShorcaTutorialPrefab);
                                go.GetComponent<SwipePanels>().Init(true, Constants.SHORCA_BOSS_LEVEL);
                            };
                            button.GetComponent<EmbeddedMenuButton>().onClickY = () =>
                            {
                                PlayShorca();
                            };
                            break;
                        case "shorcachallenge1":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                gameManager.ChangeLevel(Constants.SHORCA_CHALLENGE_ONE_LEVEL);
                            };
                            break;
                        case "shorcachallenge2":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                gameManager.ChangeLevel(Constants.SHORCA_CHALLENGE_TWO_LEVEL);
                            };
                            break;
                        case "shorcachallenge3":
                            button.GetComponent<EmbeddedMenuButton>().onClickA = () =>
                            {
                                gameManager.ChangeLevel(Constants.SHORCA_CHALLENGE_THREE_LEVEL);
                            };
                            break;
                    }
                }
                else
                {
                    EmbeddedMenuButton menuButton = button.GetComponent<EmbeddedMenuButton>();
                    menuButton.TurnOnTrialMode();
                }
            }
        }

        private void DeactivateBoss(Transform boss)
        {
            EmbeddedBossButton bossButton = boss.GetComponent<EmbeddedBossButton>();
            if (bossButton.isActive)
            {
                bossButton.MoveUp(false);
                bossButton.Deactivate();
            }
        }

        private void HideBoss(Transform boss)
        {
            boss.gameObject.SetActive(false);
        }

        private void ShowBoss(Transform boss)
        {
            boss.gameObject.SetActive(true);
        }

        public void PlayHighlightSound()
        {
            if(sound == null)
            {
                Debug.LogError("There is no audio source attached to " + gameObject);
                return;
            }
            sound.clip = highlightClip;
            sound.Play();
        }
    }
}
