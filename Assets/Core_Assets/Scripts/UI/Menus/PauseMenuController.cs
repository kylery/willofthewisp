using UnityEngine;
using Assets.Scripts.Utils;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Assets.Scripts;
using System.Collections.Generic;
using System.Timers;
using Assets.Scripts.UI;

public class PauseMenuController : MenuController
{
	private static PauseMenuController pause_instance;

	// Use this for initialization
	protected override void Start () 
	{
        base.Start();
	}
	
	// Update is called once per frame
	protected override void Update () 
	{
		//if START was pressed on the GamePad and the game is currently paused
		if ((inputManager.GetButtonUp(InputManager.ButtonTypes.START) || Input.GetKeyUp(KeyCode.Escape)) && displayed)
		{
			//unpause
			Unpause();
		}

        //if START is pressed on the GamePad and the game is not currently paused
        else if ((inputManager.GetButtonUp(InputManager.ButtonTypes.START) || Input.GetKeyUp(KeyCode.Escape)) && !displayed && sub_menus.Count > 0)
        {
            displayed = true;						//we will be displaying a menu
            ShowMenuType(MenuPane.MenuType.INDEX);	//Show the index menu
            ShowMenuBackAnimation();
        }

        base.Update();
	}

	
	/// <summary>
	/// The current instance of the Pause MenuController.
	/// </summary>
	public static PauseMenuController Instance
	{
		get
		{
			if (pause_instance == null)
			{
				pause_instance = (PauseMenuController)FindObjectOfType(typeof(PauseMenuController));
				if (pause_instance == null)
					UnityEngine.Debug.LogError("Pause MenuController does not exist");
			}
			return pause_instance;
		}
	}
}
