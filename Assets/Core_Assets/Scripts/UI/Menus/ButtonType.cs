using UnityEngine;
using Assets.Scripts.Utils;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Assets.Scripts;
using System.Collections.Generic;
using System.Timers;

public class ButtonType : MonoBehaviour 
{
	public enum TYPE
	{
		BACK,
		MAINMENU_CONFIRM,
		RESTART_CONFIRM,
		QUIT_CONFIRM,
		SHOW_OPTIONS,
		SHOW_RESTART,
		SHOW_QUIT,
		SHOW_MAINMENU,
        KEYBOARD,
        CONTROLLER,
        SHOW_CONTROLLER_SELECT
	}
	public TYPE type;
}