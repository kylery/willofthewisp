﻿using UnityEngine;
using Assets.Scripts.Utils;
using UnityEngine.UI;
using Assets.Scripts.UI;
using Assets.Scripts.Entities.Player;

public class SelectControllerMenuPane : MenuPane
{
    Selectable last_selected;

    private bool first_update_done = false;

	// Use this for initialization
	void Start () {
        last_selected = Selected;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (this.gameObject.activeSelf && !first_update_done)
        {
            UpdateIcon();
            first_update_done = true;
        }

        // if the user moves the cursor
        if (Selected != last_selected  && gameObject.activeSelf)
        {
            UpdateIcon();
        }

        last_selected = Selected;
	}

    /// <summary>
    /// Updates the controller icon for the menu
    /// </summary>
    public void UpdateIcon()
    {
        Transform iconsParent = GameObjectUtils.GetChild(this.gameObject, "Icons");
        DebugUtils.Assert(iconsParent != null, "Icons game object not found for Controller Selection Menu");
        Transform keyboardIcon = GameObjectUtils.GetChild(iconsParent.gameObject, "KeyboardIcon");
        Transform xboxIcon = GameObjectUtils.GetChild(iconsParent.gameObject, "XboxIcon");
        DebugUtils.Assert(keyboardIcon != null, "Keyboard icon not found");
        DebugUtils.Assert(xboxIcon != null, "Keyboard icon not found");

        if (Selected.name == "KeyboardButton")
        {
            keyboardIcon.gameObject.SetActive(true);
            xboxIcon.gameObject.SetActive(false);
        }
        else
        {
            keyboardIcon.gameObject.SetActive(false);
            xboxIcon.gameObject.SetActive(true);
        }
    }
}
