﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts.Utils;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// An embedded button will do an action when the player cursor is
    /// over them. This will also have to handle collisions for minions.
    /// </summary>
    public class EmbeddedMenuButton : MonoBehaviour
    {
		private enum Boss
		{
			TURTLE,
			ORCA
		}

		private enum Level
		{
			BASE,
			CHALLENGE_1,
			CHALLENGE_2,
			CHALLENGE_3
		}

		[SerializeField]
		private Level level;
		[SerializeField]
		private Boss boss;
        [SerializeField]
        protected Sprite buttonSprite;
        [SerializeField]
        protected Sprite buttonHighlightSprite;
		[SerializeField]
		protected Sprite trophySprite;
		[SerializeField]
		protected Sprite trophyHighlightSprite;
        [SerializeField]
        protected List<GameObject> appearOnHighlight;
        [SerializeField]
        protected GameObject trialMode;


        private EmbeddedMenu eMenu;

        public delegate void OnClickA();
        public delegate void OnClickY();
        public delegate void OnClickB();
        public delegate void OnClickX();

        /// <summary>
        /// What the button should do when they've been clicked and the button
        /// is A.
        /// </summary>
        public OnClickA onClickA
        {
            protected get;
            set;
        }

        /// <summary>
        /// What the button should do when they've been clicked and the button
        /// is Y.
        /// </summary>
        public OnClickY onClickY
        {
            protected get;
            set;
        }

        /// <summary>
        /// What the button should do when they've been clicked and the button
        /// is B.
        /// </summary>
        public OnClickB onClickB
        {
            protected get;
            set;
        }

        /// <summary>
        /// What the button should do when they've been clicked and the button
        /// is X.
        /// </summary>
        public OnClickX onClickX
        {
            protected get;
            set;
        }

        protected SpriteRenderer spriteRenderer;
        protected bool highlighted;
        
        /// <summary>
        /// Set up the embedded button.
        /// </summary>
        protected virtual void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            highlighted = false;
            eMenu = GameObject.FindObjectOfType<EmbeddedMenu>();
        }

        /// <summary>
        /// Check for a button or mouse click and execute onClick if its highlighted.
        /// </summary>
        void Update()
        {
            if (GameObject.FindGameObjectsWithTag("Panels").Length > 0)
                return;

            if (highlighted)
            {
                if (InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.A) || Input.GetKeyUp(KeyCode.Space) || Input.GetMouseButtonDown(0))
                {
                    if (onClickA != null)
                    {
                        onClickA();
                    }
                }
                if (InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.Y) || Input.GetKeyUp(KeyCode.E))
                {
                    if (onClickY != null)
                    {
                        onClickY();
                    }
                }
                if (InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.B) || Input.GetKeyUp(KeyCode.Escape) || Input.GetKeyUp(KeyCode.Backspace))
                {
                    if (onClickB != null)
                    {
                        onClickB();
                    }
                }
                if (InputManager.Instance.GetButtonUp(InputManager.ButtonTypes.X))
                {
                    if (onClickX != null)
                    {
                        onClickX();
                    }
                }
            }
        }

        /// <summary>
        /// Highlight the button if the player is over it.
        /// </summary>
        protected virtual void OnTriggerEnter2D(Collider2D coll)
        {
            if (coll.gameObject.name == "PlayerTrigger")
            {
                Highlight();
            }
        }

        /// <summary>
        /// Unhighlight the button when the player leaves.
        /// </summary>
        /// <param name="coll"></param>
        protected virtual void OnTriggerExit2D(Collider2D coll)
        {
            if (coll.gameObject.name == "PlayerTrigger")
            {
                Unhighlight();
            }
        }

        protected virtual void OnMouseEnter()
        {
            Highlight();
        }

        protected virtual void OnMouseExit()
        {
            Unhighlight();
        }

        /// <summary>
        /// Highlight logic.
        /// </summary>
        protected virtual void Highlight()
        {
            if (GameObject.FindGameObjectsWithTag("Panels").Length > 0)
                return;

            if (onClickA != null)
            {
                eMenu.PlayHighlightSound();

                Transform mesh = GameObjectUtils.GetChild(gameObject, "Mesh");
                if (mesh != null)
                {
                    mesh.GetComponent<SpriteRenderer>().sprite = buttonHighlightSprite;
					if (GameManager.Instance.LevelBeaten( GetLevelString ()))
					{
						Transform trophy = GameObjectUtils.GetChild (mesh.gameObject, "Trophy");
						if (trophy != null)
						{
							trophy.GetComponent<SpriteRenderer>().sprite = trophyHighlightSprite;
						}
					}

                }
                else
                {
                    spriteRenderer.sprite = buttonHighlightSprite;
                }
                highlighted = true;
                SetAppearOnHighlightActive(true);
            }
        }

        /// <summary>
        /// Unhighlight logic.
        /// </summary>
        public virtual void Unhighlight()
        {
            if (onClickA != null)
            {
                Transform mesh = GameObjectUtils.GetChild(gameObject, "Mesh");
                if (mesh != null)
                {
                    mesh.GetComponent<SpriteRenderer>().sprite = buttonSprite;
					if (GameManager.Instance.LevelBeaten( GetLevelString ()))
					{
						Transform trophy = GameObjectUtils.GetChild (mesh.gameObject, "Trophy");
						if (trophy != null)
						{
							trophy.GetComponent<SpriteRenderer>().sprite = trophySprite;
						}
					}
                }
                else
                {
                    spriteRenderer.sprite = buttonSprite;
                }
                highlighted = false;

                SetAppearOnHighlightActive(false);
            }
        }

        public void SetAppearOnHighlightActive(bool setActive)
        {
            foreach (GameObject go in appearOnHighlight)
            {
                go.SetActive(setActive);
            }
        }

        /// <summary>
        /// There are some aspects of the game that should not be available in the
        /// trial version. This will let the user know that they can get more features
        /// if they buy the full version.
        /// </summary>
        public void TurnOnTrialMode()
        {
            trialMode.SetActive(true);
        }

		public string GetLevelString()
		{
			if (boss == Boss.TURTLE)
			{
				switch (level)
				{
				default:
					return Constants.TURTLE_BOSS_LEVEL;
				case Level.CHALLENGE_1:
					return Constants.TURTLE_CHALLENGE_ONE_LEVEL;
				case Level.CHALLENGE_2:
					return Constants.TURTLE_CHALLENGE_TWO_LEVEL;
				case Level.CHALLENGE_3:
					return Constants.TURTLE_CHALLENGE_THREE_LEVEL;
				}
			}
			else
			{
				switch (level)
				{
				default:
					return Constants.SHORCA_BOSS_LEVEL;
				case Level.CHALLENGE_1:
					return Constants.SHORCA_CHALLENGE_ONE_LEVEL;
				case Level.CHALLENGE_2:
					return Constants.SHORCA_CHALLENGE_TWO_LEVEL;
				case Level.CHALLENGE_3:
					return Constants.SHORCA_CHALLENGE_THREE_LEVEL;
				}
			}
		}

    }
}
