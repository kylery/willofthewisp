﻿using UnityEngine;
using Assets.Scripts.Utils;
using UnityEngine.UI;
using Assets.Scripts.UI;
using Assets.Scripts.Entities.Player;

public class SelectControllerMenuController : MenuController
{
    Player player;

    private bool displayedLastFrame = false;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        //Get the player
        player = PlayerManager.Instance.Player.GetComponent<Player>();
        DebugUtils.Assert(player != null, "Player not found");
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        //if START was pressed on the GamePad and the game is currently paused
        if ((inputManager.GetButtonUp(InputManager.ButtonTypes.START) || Input.GetKeyUp(KeyCode.Escape)) && displayed)
        {
            //unpause
            Unpause();
        }

        if (displayed && !displayedLastFrame && !player.IsPlayerFrozen())
        {
            player.FreezePlayer();
        }

        if (!displayed && displayedLastFrame && player.IsPlayerFrozen())
        {
            player.UnfreezePlayer();
        }

        displayedLastFrame = displayed;
    }
}
