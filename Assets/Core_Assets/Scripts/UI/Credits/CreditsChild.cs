﻿using UnityEngine;
using System.Collections;

namespace Assets.UI.Credits
{ 
public class CreditsChild : MonoBehaviour 
{
        public float upSpeed{get; set;}
	    
        void Start()
        {
            TextMesh text = GetComponent<TextMesh>();
            if (text != null)
                text.text = transform.name;
        }

	    void FixedUpdate () 
        {
            transform.position = transform.position + new Vector3(0, upSpeed * Time.deltaTime);
	    }
    }
}
