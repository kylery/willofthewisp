﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;


namespace Assets.UI.Credits
{ 

    public class CreditsController : MonoBehaviour 
    {

        public float scrollSpeed;
        public GameObject[] backgrounds;
        public float backgroundSwitchTime;
        public float backgroundTransitionTime;
        public float initialDelay;
        public float scrollDelay;
        public float endDelay;

        private CreditsChild[] children;

        private int curBackground;
        private float curAlpha;
        private float curBackgroundSwitchTime;
        private float curTransitionTime;
        private float curInitialTime;
        private float curScrollTime;
        private float curEndDelay;
        private bool isTransitioning;
        private State state;


        private enum State
        {
            Start,
            Middle,
            End
        }

	    // Use this for initialization
	    void Start () {
            isTransitioning = false;
            state = State.Start;
            curBackground = 0;
            curBackgroundSwitchTime = backgroundSwitchTime;
            curTransitionTime = backgroundTransitionTime;
            curInitialTime = initialDelay;
            curScrollTime = scrollDelay;
            curEndDelay = endDelay;

            //Loop through all child credit components and initialize
            children = transform.GetComponentsInChildren<CreditsChild>();
            InitializeChildren(children);
            curBackground = 0;
	    }

        /// <summary>
        /// Update
        /// </summary>
        void Update()
        {
            // For background fading
            if(backgrounds.Length > 0 && curBackground < backgrounds.Length-1)
            { 
                if(isTransitioning)
                {
                    curTransitionTime -= Time.deltaTime;
                    float ratio = Mathf.Clamp((curTransitionTime / backgroundTransitionTime),0.0f,1.0f);
                    SetAlpha(backgrounds[curBackground],ratio);
                    if(curTransitionTime <= 0)
                    {
                        isTransitioning = false;
                        curBackground++;
                        curTransitionTime = backgroundTransitionTime;
                    }
                }
                else
                {
                    curBackgroundSwitchTime -= Time.deltaTime;
                    if(curBackgroundSwitchTime <= 0)
                    {
                        isTransitioning = true;
                        curBackgroundSwitchTime = backgroundSwitchTime;
                    }
                }
            }

            //For Scroll speed
            switch(state)
            {
                case State.Start:
                    {
                        curInitialTime -= Time.deltaTime;
                        if (curInitialTime <= 0)
                        {
                            state = State.Middle;
                            ToggleChildren(children);
                        }
                        break;
                    }
                case State.Middle:
                    {
                        curScrollTime -= Time.deltaTime;
                        if(curScrollTime <= 0)
                        {
                            state = State.End;
                            ToggleChildren(children);
                        }
                        break;
                    }
                case State.End:
                    {
                        curEndDelay -= Time.deltaTime;
                        if(curEndDelay <=0)
                        {
                            GameManager.Instance.ChangeLevel(Constants.MAIN_MENU_LEVEL);
                        }
                        break;
                    }
                default:
                    break;

            }
        }

        /// <summary>
        /// Set alpha of a give object
        /// </summary>
        /// <param name="background"></param>
        /// <returns>Bool of whether or not the set worked</returns>
        private bool SetAlpha(GameObject background,float ratio)
        {
            SpriteRenderer render = background.GetComponent<SpriteRenderer>();
            if (render != null)
            {
                render.color = new Color(1.0f, 1.0f, 1.0f, ratio);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Initialize Children
        /// </summary>
        /// <param name="creditComponents"></param>
        private void InitializeChildren(CreditsChild[] creditComponents)
        {
            if(creditComponents != null)
            { 
                foreach(CreditsChild credchild in creditComponents )
                {
                     credchild.enabled = false;
                     credchild.upSpeed = scrollSpeed;
                }
            }
        }

        /// <summary>
        /// Toggle Children
        /// </summary>
        /// <param name="creditComponents"></param>
        private void ToggleChildren(CreditsChild[] creditComponents)
        {
            if(creditComponents != null)
            { 
                foreach (CreditsChild credchild in creditComponents)
                {
                    credchild.enabled = !credchild.enabled;
                }
            }
        }
    }
}