﻿using UnityEngine;
using System.Collections.Generic;

namespace Assets.Scripts.UI.SplashScreen
{
    /// <summary>
    /// Given a series of images, this will cycle through them fading them in and out.
    /// When the cycle is complete, it will go to the next level in the build (which
    /// should be the main menu).
    /// </summary>
    public class CycleSplashScreens : MonoBehaviour
    {
        public List<Texture2D> splashScreens;
        public Texture2D loadingScreen;
        public float rateOfChange;
        public float delay;

        private int curIndex;
        private Texture2D image;
        private float alpha;
        private float curDelayTime;
        private bool goToNextLevel;
        private bool transitioning;

        /// <summary>
        /// This will initialize the cycle to start at the first
        /// image.
        /// </summary>
        void Awake()
        {
            curIndex = 0;
            image = splashScreens[curIndex];
            alpha = 0.1f;
            goToNextLevel = false;
            transitioning = false;
        }

        /// <summary>
        /// This will change the alpha of the current image. When it reaches
        /// 1, then it will change direction to fade out. When it reaches 0, 
        /// then it will go to the next image in the cycle. When it ends the
        /// cycle it will go to the next level.
        /// </summary>
        void Update()
        {
            const float ALPHA_THRESHOLD = .1f;

            if (Input.anyKeyDown)
            {
                alpha = 1;
                goToNextLevel = true;
                image = loadingScreen;
            }

            if (transitioning)
            {
                curDelayTime -= Time.deltaTime;
                if (curDelayTime <= 0)
                {
                    transitioning = false;
                    alpha = ALPHA_THRESHOLD;
                }
            }
            else
            {
                alpha += rateOfChange * Time.deltaTime;
                if (alpha >= 1 - ALPHA_THRESHOLD)
                {
                    alpha = 1 - ALPHA_THRESHOLD;
                    rateOfChange *= -1;
                }
                if (alpha <= ALPHA_THRESHOLD)
                {
                    curIndex += 1;
                    if (curIndex >= splashScreens.Count)
                    {
                        alpha = 1;
                        goToNextLevel = true;
                        image = loadingScreen;
                    }
                    else
                    {
                        image = splashScreens[curIndex];
                        alpha = 0;
                        rateOfChange *= -1;
                        transitioning = true;
                        curDelayTime = delay;
                    }
                }
            }
        }

        private void LoadNextLevel()
        {
            Application.LoadLevel(Application.loadedLevel + 1);
        }

        /// <summary>
        /// This will display the image by scaling it to fit within the screen
        /// and be centered.
        /// </summary>
        void OnGUI()
        {
            float screenRatio = (float)Screen.width / (float)Screen.height;
            float textureRatio = (float)image.width / (float)image.height;

            float scaledHeight;
            float scaledWidth;
            if (textureRatio <= screenRatio)
            {
                // The scaled size is based on the height
                scaledHeight = Screen.height;
                scaledWidth = (Screen.height * textureRatio);
            }
            else
            {
                // The scaled size is based on the width
                scaledWidth = Screen.width;
                scaledHeight = (scaledWidth / textureRatio);
            }
            Color color = Color.white;
            color.a = alpha;
            GUI.color = color;
            float xPosition = (Screen.width - scaledWidth) / 2;
            var boxProperties = new Rect(xPosition, 0, scaledWidth, scaledHeight);
            GUI.DrawTexture(boxProperties, image);

            if (goToNextLevel)
                LoadNextLevel();
        }
    }
}