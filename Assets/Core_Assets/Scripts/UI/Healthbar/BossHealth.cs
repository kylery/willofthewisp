using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Assets.Scripts.Entities;
using Assets.Scripts.Utils;
using Assets.Scripts.Entities.Player;
using Assets.Scripts.Entities.Bosses;

namespace Assets.Scripts.UI
{
	/// <summary>
	/// Provides a HUD GUI display that represents the entity's health.
	/// To get the Healthbar to render, a texture and a material need
	/// to be provided.
	/// </summary>
	public sealed class BossHealth : MonoBehaviour
	{
		//public variables
		public Entity boss;  //The entity to which this healthbar belongs
		public Slider healthbar;
		public Color healthColor = Color.green;
		public Color damageColor = Color.red;
		public Sprite sliderHandle;
        public Color TurtleHealthbarDividerColor = Color.yellow;

        private Transform hb_line;

		private float inithealth;

        private RectTransform hb_rt;

        private static BossHealth instance;

        private RectTransform[] _divs;

        private Turtle turtle;
        private uint lastLimbCount;

        void Awake()
        {
			if (boss == null)
			{
				boss = GameObject.FindGameObjectWithTag("Boss").GetComponent<Entity>();
			}
			if(healthbar == null)
			{
				healthbar = transform.GetComponentInChildren<Slider>();
			}

			DebugUtils.Assert (boss != null, "No boss entity applied to BossHealth");
			DebugUtils.Assert (healthbar != null, "No healthbar entity applied to BossHealth");

            

			DebugUtils.Assert (healthbar.transform.childCount > 2, "Error: Healthbar slider doesn't have enough children.");
            DebugUtils.Assert(GameObjectUtils.GetChild(healthbar.gameObject, "Fill Area").childCount > 0, "Error: Healthbar slider Fill Area doesn't have enough children.");
            Transform background = GameObjectUtils.GetChild(healthbar.gameObject, "Background");
            Transform fill = GameObjectUtils.GetChild(healthbar.gameObject, "Fill Area").GetChild(0);

			DebugUtils.Assert(background.GetComponent<Image>() != null, "Error: Healthbar slider background doesn't have Image (script).");
			DebugUtils.Assert(fill.GetComponent<Image>() != null, "Error: Healthbar slider Fill doesn't have Image (script).");
			background.GetComponent<Image> ().color = damageColor;
			fill.GetComponent<Image> ().color = healthColor;


            Transform HandleSlideArea = GameObjectUtils.GetChild(healthbar.gameObject, "Handle Slide Area");
			DebugUtils.Assert (HandleSlideArea.childCount > 0, "Error: Healthbar Handle Slide Area doesn't have child.");
			Transform handle = GameObjectUtils.GetChild(HandleSlideArea.gameObject, "Handle");
			if (handle != null && sliderHandle != null) {
				handle.GetComponent<Image>().sprite = sliderHandle;
			}

            //get the healthbar rect transform
            hb_rt = healthbar.GetComponent<RectTransform>();
            //Get the the healthbar line object
            hb_line = GameObjectUtils.GetChild(gameObject, "hb_line");
        }


		void Start()
		{

            //initialize the health
			inithealth = boss.health;

			healthbar.value = boss.health / inithealth;


            /// Spawn dividers if not already spawned 
			turtle = boss.GetComponent<Turtle> ();
            _divs = new RectTransform[0];
            if (turtle != null)
            {
                lastLimbCount = turtle.startingLimbCount;
                _divs = new RectTransform[turtle.startingLimbCount];
                for (int i = 0; i < turtle.startingLimbCount - 1; i++)
                {
                    _divs[i] = SpawnDivider((i+1) / (float)turtle.startingLimbCount, 1f, 0.8f, TurtleHealthbarDividerColor);
                }
            }


            ApplyHealthbarUITheme();
		}

        /// <summary>
        /// Updates every frame
        /// </summary>
		void Update()
		{
			if(boss == null)
			{
				return;
			}
			if(inithealth < 0.1f)
			{
				inithealth = boss.health;
			}
			healthbar.value = boss.health / inithealth;


            /// TURTLE ONLY BELOW HERE

            if(turtle == null)
            {
                return;
            }

            uint currentLimbCount = turtle.startingLimbCount - turtle.destroyedCount;

            if(currentLimbCount < lastLimbCount && turtle.destroyedCount > 0)
            {
                if(currentLimbCount - 1 <= _divs.Length)
                {
                    RectTransform div = _divs[currentLimbCount - 1];
                    if(div != null)
                        div.gameObject.SetActive(false);
                }
            }
            lastLimbCount = currentLimbCount;
		}


        /// <summary>
        /// Returns the instance of the BossHealth GameObject 
		/// Note: This isn't very effective with multiple Bosses.
        /// </summary>
        public static BossHealth Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = GameObject.Find("BossHealth").GetComponent<BossHealth>();
                    if (instance == null)
                    {
                        Debug.LogError("BossHealth does not exist");
                    }
                }
                return instance;
            }
        }


        private void ApplyHealthbarUITheme()
        {
            Sprite skin = UIManager.Instance.GetHealthBarSkinThemeSprite();
			Image img;
            Transform skin_t = GameObjectUtils.GetChild(healthbar.gameObject, "HealthbarSkin");
            if(skin_t != null && skin != null)
            {
				img = skin_t.GetComponent<Image>();
				if (img != null)
				{
            		img.sprite = skin;
				}
            }

			// healthbar handle
			skin = UIManager.Instance.GetHealthbarHandleForBoss (boss);
			skin_t = GameObjectUtils.GetChild (healthbar.gameObject, "Handle Slide Area");
			if(skin == null || skin_t == null)
			{
				return;
			}
			skin_t = GameObjectUtils.GetChild (skin_t.gameObject, "Handle");
			if (skin_t == null)
			{
				return;
			}
			if(skin_t.GetComponent<RectTransform>() != null)
			{
				RectTransform rt = skin_t.GetComponent<RectTransform>();
				rt.sizeDelta = new Vector2(9, rt.sizeDelta.y); 
			}
			img = skin_t.GetComponent<Image>();
			if (img != null)
			{
				img.sprite = skin;
			}
        }


        /// <summary>
        /// Spawns a new healthbar divider.
        /// </summary>
        /// <param name="positionAsRatioOfHealthbar">float in Range [0, 1]. 0 = 0% health, 1 = 100% health</param>
        /// <returns></returns>
        public RectTransform SpawnDivider(float positionAsRatioOfHealthbar)
		{
			RectTransform line_rt = null;
           
            //GameObject newHBLine = (GameObject)Instantiate(hb_line, healthbar.transform.position, healthbar.transform.rotation);
            Transform newHBLine = (Transform)Instantiate(hb_line);

            // Set the parent to the healthbar
            newHBLine.SetParent(hb_rt, false);

            // Get the Rect Transform of the new divider
            line_rt = newHBLine.GetComponent<RectTransform>();

            // Validate
            DebugUtils.Assert(hb_rt != null, "healthbar doesn't have a Rect Transform");
            DebugUtils.Assert(line_rt != null, "healthbar divider doesn't have a Rect Transform");

            // the given position should be in range 0 to 1
            // 0 is healthbar at 0% health
            // 1 is healthbar at 100% health
            positionAsRatioOfHealthbar = positionAsRatioOfHealthbar < 0f ? 0f : positionAsRatioOfHealthbar;
            positionAsRatioOfHealthbar = positionAsRatioOfHealthbar > 1f ? 1f : positionAsRatioOfHealthbar;

            // position is width of the healthbar * the (%) passed in - half the width of the healthbar
            float X = hb_rt.sizeDelta.x *  positionAsRatioOfHealthbar - (hb_rt.sizeDelta.x / 2); 

            //Update the line position
            line_rt.localPosition = new Vector3(X, 0, 0);

            //enable
            newHBLine.gameObject.SetActive(true);

            return line_rt;
        }


        /// <summary>
        /// Spawns a new healthbar divider and scales its width and height
        /// to the given width and height multipliers.
        /// </summary>
        /// <param name="positionAsRatioOfHealthbar">float in Range [0, 1]. 0 = 0% health, 1 = 100% health</param>
        /// <param name="widthMultiplier">Scales the width of the divider</param>
        /// <param name="heightMultiplier">Scales the height of the divider</param>
        /// <returns></returns>
        public RectTransform SpawnDivider(float positionAsRatioOfHealthbar, float widthMultiplier, float heightMultiplier)
        {
			RectTransform line_rt = null;
      
            //Do the base function setting the position and gettig its rect transform
            line_rt = SpawnDivider(positionAsRatioOfHealthbar);
            
            //Scale to multipliers
            line_rt.localScale = new Vector3(line_rt.localScale.y * widthMultiplier, line_rt.localScale.y * heightMultiplier, 1);

            return line_rt;
        }


        /// <summary>
        /// Spawns a healthbar divider at the given position, scales the width
        /// and height to the given multiplier, and sets the color.
        /// </summary>
        /// <param name="positionAsRatioOfHealthbar">float in Range [0, 1]. 0 = 0% health, 1 = 100% health</param>
        /// <param name="widthMultiplier">Scales the width of the divider</param>
        /// <param name="heightMultiplier">Scales the height of the divider</param>
        /// <param name="colorOverlay">Sets the color overlay of the divider</param>
        /// <returns></returns>
        public RectTransform SpawnDivider(float positionAsRatioOfHealthbar, float widthMultiplier, float heightMultiplier, Color colorOverlay)
        {
            // Get the Rect Transform from the other spawn function, setting position and scaling
            RectTransform line_rt = SpawnDivider(positionAsRatioOfHealthbar, widthMultiplier, heightMultiplier);

            //Add color
            line_rt.GetComponent<Image>().color = colorOverlay;

            return line_rt;
        }
	}   
}
