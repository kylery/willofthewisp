﻿using UnityEngine;
using System.Collections;

using Assets.Scripts.UI;
using Assets.Scripts.Utils;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// When the player loses all their swarm, this will activate the game over sequence
    /// which will fade the screen out, but still show the level going. The player will
    /// then have the ability to restart the level or return to the main menu.
    /// </summary>
    public class GameOver : MonoBehaviour
    {
        [SerializeField]
        private GameObject background;
        [SerializeField]
        private float backgroundFadeInRate;
        [SerializeField]
        [Range(0, 1)]
        private float backgroundFinalOpacity;
        [SerializeField]
        private float labelFadeInRate;
        [SerializeField]
        [Range(0, 1)]
        private float backgroundOpacityToStartLabel;

        private GameObject canvas;
        private GameObject label;
        private SpriteRenderer backgroundSprite;
        private UnityEngine.UI.Text labelText;
        private bool hasMenu;

        /// <summary>
        /// Initialize the game over sequence by getting all the necessary componenets.
        /// </summary>
        void Start()
        {
            hasMenu = false;
            backgroundSprite = background.GetComponent<SpriteRenderer>();
            canvas = GameObject.FindGameObjectWithTag("Canvas");
            label = DisableCanvas();
            labelText = label.GetComponent<UnityEngine.UI.Text>();
            label.SetActive(true);
            UIUtils.SetTextAlpha(labelText, 0);
            ResizeBackgroundToScreen();
        }

        /// <summary>
        /// Update when each componenet should fade in and how fast they should fade in based on
        /// how faded in the component is.
        /// </summary>
        void Update()
        {
            if (backgroundSprite.color.a < backgroundFinalOpacity)
            {
                UIUtils.SetSpriteAlpha(backgroundSprite, backgroundSprite.color.a + backgroundFadeInRate * Time.deltaTime);
            }

            if (labelText.color.a < 1f && backgroundSprite.color.a >= backgroundOpacityToStartLabel)
            {
                UIUtils.SetTextAlpha(labelText, labelText.color.a + labelFadeInRate * Time.deltaTime);
            }

            if (labelText.color.a > 0.5)
            {
                if (!hasMenu)
                {
					//Looks like showing the menu is being handled in game manager now
                    hasMenu = true;
                }
            }
        }

        /// <summary>
        /// Make sure the background is properly centered on the camera.
        /// </summary>
        void FixedUpdate()
        {
            Vector3 position = Camera.main.transform.position;
            position.z = -1;
            transform.position = position;
        }

        /// <summary>
        /// Resize the background so that it fits the screen.
        /// </summary>
        private void ResizeBackgroundToScreen()
        {
            var scale = Vector3.one;

            float width = backgroundSprite.bounds.size.x;
            float height = backgroundSprite.bounds.size.y;

            float worldHeight = Camera.main.orthographicSize * 2.0f;
            float worldWidth = worldHeight * Screen.width / Screen.height;

            scale.x = worldWidth / width;
            scale.y = worldHeight / height;

            background.transform.localScale = scale;

            UIUtils.SetSpriteAlpha(backgroundSprite, 0);
        }

        /// <summary>
        /// Disable all UI components in the canvas. This will also return the canvas componenets
        /// that the game over sequence needs.
        /// </summary>
        /// <returns>The Game Over label.</returns>
        private GameObject DisableCanvas()
        {
            const string LABEL_NAME = "GameOverLabel";
            GameObject go = null;

            foreach (Transform child in canvas.transform)
            {
                if (child.name == LABEL_NAME)
                {
                    go = child.gameObject;
                }
                else
                {
                    child.gameObject.SetActive(false);
                }
            }

            return go;
        }
    }
}