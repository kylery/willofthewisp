﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.Utils;
using Assets.Scripts.UI;
using Assets.Scripts.Entities;
using Assets.Scripts.Entities.Bosses;

namespace Assets.Scripts.UI
{
    /// <summary>
    /// The UI Manager is communication from the UI to the main game that all the
    /// UI elements can access to gain information. Core Game should not access
    /// the UI, but the UI should get most of its info from Game Managaer. The UI
    /// Manager also contains useful prefabs so that UI elements don't have to deal
    /// with the storage.
    /// </summary>
    public class UIManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject turtleTutorialPrefab;
        [SerializeField]
        private GameObject shorcaTutorialPrefab;
        [SerializeField]
        private GameObject Arctic_PauseMenuPrefab;
        [SerializeField]
        private GameObject Tropical_PauseMenuPrefab;
        [SerializeField]
        private Sprite Arctic_HealthBarSkin;
        [SerializeField]
        private Sprite Tropical_HealthBarSkin;
		[SerializeField]
		private Sprite Shorca_HealthbarHandle;
		[SerializeField]
		private Sprite Turtle_HealthbarHandle;

		//enum of possible themes
		public enum THEME
		{
			ARCTIC,
			TROPICAL
		}

		private THEME PauseMenuTheme = THEME.ARCTIC; 	//default arctic
			
		private static Sprite themedSprite;
		
        private static UIManager instance;

        /// <summary>
        /// Current instance of the UI Manager.
        /// </summary>
        public static UIManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = GameObject.Find("UI_Manager").GetComponent<UIManager>();
                    if (instance == null)
                    {
                        Debug.LogError("UI Manager does not exist");
                    }
                }
                return instance;
            }
        }

        /// <summary>
        /// The prefab for the Turtle Tutorial Panels
        /// </summary>
        public GameObject TurtleTutorialPrefab
        {
            get { return turtleTutorialPrefab; }
        }

        /// <summary>
        /// The prefab for the Shorca Tutorial Panels.
        /// </summary>
        public GameObject ShorcaTutorialPrefab
        {
            get { return shorcaTutorialPrefab; }
        }

		/// <summary>
		/// The current instance of the Win MenuController.
		/// </summary>
		public THEME GetTheme()
		{
            UITheme theme = UITheme.Instance;
            if (theme != null)
            {
                PauseMenuTheme = theme.GetTheme();
            }
            else if (Application.loadedLevelName.ToLower().Contains("turtle"))
            {
                PauseMenuTheme = THEME.TROPICAL;
            }
			return PauseMenuTheme;
		}


		/// <summary>
		/// Gets the appropriate sprite for the established theme.
		/// </summary>
		/// <value>The themed sprite.</value>
		public Sprite GetPauseMenuThemeSprite()
		{
			//for the case of the main menu
			if(Instance.Arctic_PauseMenuPrefab == null || Instance.Tropical_PauseMenuPrefab == null)
			{
				return null;
			}

			//default arctic
			themedSprite = Instance.Arctic_PauseMenuPrefab.GetComponent<Image>().sprite;

			//if theme is tropical
			if (GetTheme() == THEME.TROPICAL)
			{
				themedSprite = Instance.Tropical_PauseMenuPrefab.GetComponent<Image>().sprite;
			}
			return themedSprite;
		}

        /// <summary>
        /// Gets the healthbar skin sprite.
        /// </summary>
        /// <returns>Sprite</returns>
        public Sprite GetHealthBarSkinThemeSprite()
        {
            //for the case of the main menu
            if (Instance.Arctic_PauseMenuPrefab == null || Instance.Tropical_PauseMenuPrefab == null)
            {
                return null;
            }

            //default arctic
            themedSprite = Instance.Arctic_HealthBarSkin;

            //if theme is tropical
            if (GetTheme() == THEME.TROPICAL)
            {
                themedSprite = Instance.Tropical_HealthBarSkin;
            }
            return themedSprite;
        }


		/// <summary>
		/// Gets the healthbar handle for boss.
		/// Could return null if the Sprite is null;
		/// </summary>
		/// <returns>The healthbar handle for boss.</returns>
		/// <param name="type">Type.</param>
		public Sprite GetHealthbarHandleForBoss(Entity boss)
		{
			//default to turtle if null
			if(boss == null)
			{
				return Instance.Turtle_HealthbarHandle;
			}
			if (boss.GetComponent<Turtle>() == null)
			{
				return Instance.Shorca_HealthbarHandle;
			}
			return Instance.Turtle_HealthbarHandle;
			
		}
    }
}
