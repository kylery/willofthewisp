﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities.BossLevels;

namespace Assets.Scripts.Projectiles
{
    ///<summary>
    ///  Class used to emit charges from.
    ///</summary>
    public class Emitter : MonoBehaviour
    {
        //Persistent firing variables
        [SerializeField]
        private bool persistentEmission = false;
        [SerializeField]
        private float startFireOffset = 5.0f;
        [SerializeField]
        private float reFireDelay = 2.0f;
        private float reFireTimer;
          
        // Object to emmit
        [SerializeField]
        private GameObject EmittedObjectPrefab;

        [SerializeField]
        private AudioClip emitterClip;
        [SerializeField]
        private float soundDuration = 1.0f;
        private float emitterDuration;

        private AudioSource sound;

        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            reFireTimer = startFireOffset;
            sound = gameObject.GetComponent<AudioSource>();
        }

        /// <summary>
        /// On Update
        /// </summary>
        void Update()
        {
            if(persistentEmission)
            { 
                reFireTimer -= Time.deltaTime;
                if (reFireTimer <= 0)
                {
                    reFireTimer = reFireDelay;
                    Emit();
                }
            }
            emitterDuration -= Time.deltaTime;
            if (emitterDuration <= 0)
            {
                sound.Stop();
            }

        }

        ///<summary>
        ///  Emits a object from location of this object in direction object
        ///  is facing.
        ///</summary>
		///<returns> emitted object </returns>
        public GameObject Emit()
        {
            sound.clip = emitterClip;
            emitterDuration = soundDuration;
            sound.Play();
            return (GameObject)Instantiate(EmittedObjectPrefab, transform.position, transform.rotation);
        }

        ///<summary>
        ///  Emits a object from location of this object in direction object
        ///  is facing.
        ///</summary>
		///<param name="destinationObject"> Object to shoot at</param>
		///<returns> emitted object </returns>
        public GameObject Emit(GameObject destinationObject)
        {
            GameObject proj = Emit();
            proj.GetComponent<ActivateChargeScript>().SetDestinationObject(destinationObject);
            return proj;
        }
    }
}
