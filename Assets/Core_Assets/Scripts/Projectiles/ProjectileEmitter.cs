﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Entities.BossLevels;
using Assets.Scripts.Projectiles;
using System.Collections.Generic;

namespace Assets.Scripts.Projectiles
{
    public class ProjectileEmitter : MonoBehaviour
    {
        //Persistent firing variables
        [SerializeField]
        private bool persistentEmission = false;
        [SerializeField]
        private float startFireOffset = 5.0f;
        [SerializeField]
        private float reFireDelay = 2.0f;
        private float reFireTimer;

        // Object to emmit
        [SerializeField]
        private ProjectileManager.ProjectileTypes emittedProjectileType;
        [SerializeField]
        public GameObject generalTarget;
        [Tooltip("To just directly fire at object. Set degrees to 0")]
        [SerializeField]
        private int degreeRange;
        [SerializeField]
        private int numberFired;
        [SerializeField]
        private int existingProjectileLimit;
        private int curProjectilesExisting;
        private List<GameObject> existingProjectiles;

        [SerializeField]
        private AudioClip fireBallClip;
        [SerializeField]
        private float fireBallDuration = 1.0f;

        // Emitter Indication
        [SerializeField]
        private bool showIndicatorAnimation = false;
        [SerializeField]
        private float indicationTime;
        [SerializeField]
        private GameObject indicator;
        private float curIndicationTime;
        private Vector2 startTrajectory;

        private float soundDuration;
        AudioSource sound;


        /// <summary>
        /// Start
        /// </summary>
        void Start()
        {
            reFireTimer = startFireOffset;
            existingProjectiles = new List<GameObject>();
            sound = gameObject.GetComponent<AudioSource>();
        }

        /// <summary>
        /// On Update
        /// </summary>
        void Update()
        {
            if (persistentEmission)
            {
                reFireTimer -= Time.deltaTime;
                if (reFireTimer <= 0)
                {
                    if(CountExistingProjectiles(existingProjectiles) < existingProjectileLimit)
                    {
                        existingProjectiles = RemoveNullProjectiles(existingProjectiles);//Update list
                        reFireTimer = reFireDelay;
                        EmitProjectiles();
                    }
                }
            }

            if (showIndicatorAnimation && curIndicationTime > 0)
            {
                curIndicationTime -= Time.deltaTime;
                if (curIndicationTime <= 0)
                {
                    indicator.SetActive(false);
                    FireProjectile();
                }
            }

            soundDuration -= Time.deltaTime;
            if(soundDuration <= 0)
            {
                sound.Stop();
            }
        }

        /// <summary>
        /// I hate this method honestly.
        /// 
        /// Until I can come up with a paradigm I better like(If I do), this will hold, performance shouldn't take a hit
        /// 
        /// Caculates the current existing projectile count by checking what projectiles still exist that were emitted from this emitter.
        /// returns that number. Also removes any instances that don't exist anymore or aren't a projectile .
        /// </summary>
        /// <param name="projectiles">List to check</param>
        /// <returns></returns>
        private int CountExistingProjectiles(List<GameObject> projectiles)
        {
            if(projectiles == null)
                return 0;

            //Count Projectiles
            int curCount = 0;
            for(int i = 0; i < projectiles.Count; i++)
            {
                GameObject listItem = projectiles[i];
                if(listItem == null)
                    continue;

                // Count if only has Projectile componenet
                Projectile listComponeent = listItem.GetComponent<Projectile>();
                if (listComponeent == null)
                    continue;
                else
                    curCount++;
            }
            return curCount;
        }

        /// <summary>
        /// I hate this method as well.
        /// 
        /// Removes null references from the list
        /// 
        /// Returns null if a null list is given
        /// 
        /// Returns an updated Projectile Game Object list with Projectile Objects that are Projectiles and are not null
        /// 
        /// </summary>
        /// <param name="projectiles"></param>
        /// <returns></returns>
        private List<GameObject> RemoveNullProjectiles(List<GameObject> projectiles)
        {
            if (projectiles == null)
                return null;

            //Count Projectiles
            List<GameObject> newList = new List<GameObject>();
            for (int i = 0; i < projectiles.Count; i++)
            {
                GameObject listItem = projectiles[i];
                if (listItem == null)
                    continue;

                // Count if only has Projectile componenet
                Projectile listComponeent = listItem.GetComponent<Projectile>();
                if (listComponeent == null)
                    continue;
                else
                    newList.Add(projectiles[i]);
            }

            return newList;
        }

        /// <summary>
        /// Emitts Projectiles
        /// </summary>
        public void EmitProjectiles()
        {
            for( int i = 0; i < numberFired; i++)
            {
                soundDuration = fireBallDuration;
                sound.clip = fireBallClip;
                sound.Play();
                EmitProjectile();
            }
        }

        /// <summary>
        /// Emitts Projectils based upon parameters
        /// </summary>
        /// <param name="generalTarget">target</param>
        /// <param name="degreeRange">degree range</param>
        /// <param name="numberEmitted">number emitted</param>
        public void EmitProjectiles(GameObject generalTarget, int degreeRange,int numberEmitted)
        {
            for (int i = 0; i < numberEmitted; i++)
            {
                EmitProjectile(generalTarget,degreeRange);
            }
        }

        ///<summary>
        ///  Emmits a projectile
        ///</summary>
        public void EmitProjectile()
        {
           startTrajectory = DetermineRandomProjectileTrajectory(generalTarget,degreeRange);
           StartIndicator();
        }

        /// <summary>
        /// Emits a projectile given parameters
        /// </summary>
        /// <param name="generalTarget">target</param>
        /// <param name="degreeRange">degree range of target</param>
        public void EmitProjectile(GameObject generalTarget, int degreeRange)
        {
            startTrajectory = DetermineRandomProjectileTrajectory(generalTarget, degreeRange);
            StartIndicator();
        }

        private void StartIndicator()
        {
            if (showIndicatorAnimation)
            {
                indicator.SetActive(true);
                curIndicationTime = indicationTime;
            }
            else
            {
                FireProjectile();
            }
        }

        private void FireProjectile()
        {
            existingProjectiles.Add(ProjectileManager.Instance.FireProjectile(emittedProjectileType, transform.position, startTrajectory));
        }

        /// <summary>
        /// Determines random projectiel trajectory given a target, and a degree range
        /// </summary>
        /// <param name="target">target to primairly target</param>
        /// <param name="degreeRange">degree range off of target</param>
        /// <returns></returns>
        private Vector3 DetermineRandomProjectileTrajectory(GameObject target, int degreeRange)
        {
            Vector2 trajectory = Vector2.right;
            if (target != null)
            {
                trajectory = ((Vector2)target.transform.position - (Vector2)transform.position).normalized;
                float degrees = UnityEngine.Random.Range(-1 * (degreeRange) / 2, degreeRange / 2);
                Utils.Math2d.RotateVector(ref trajectory, degrees);
            }
            return trajectory;
        }

    }
}
