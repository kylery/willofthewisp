﻿
#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using UnityEditor;
using Assets.Scripts.Projectiles;


[CustomEditor(typeof(Projectile))]
public class ProjectileScriptEditor : Editor
{
    public override void OnInspectorGUI()
    {
        Projectile myProjectile = (Projectile)target;

        // Velocity
        myProjectile.velocity = EditorGUILayout.FloatField("Velocity", myProjectile.velocity);
        
        // LifeTime
        myProjectile.hasLifeTime =  EditorGUILayout.Toggle("Has Life Time", myProjectile.hasLifeTime);
        bool hasLifeTime = myProjectile.hasLifeTime;
        if(hasLifeTime)
        { 
            EditorGUI.BeginDisabledGroup(hasLifeTime == false);
            EditorGUI.indentLevel++;
            myProjectile.lifeTime = EditorGUILayout.FloatField("LifeTime", myProjectile.lifeTime);
            //Blink
            myProjectile.hasBlinkIndication = EditorGUILayout.Toggle("Has Blink Indication", myProjectile.hasBlinkIndication);
            bool blink = myProjectile.hasBlinkIndication;
            EditorGUI.BeginDisabledGroup(blink == false);
            myProjectile.blinkIndicationLimit = EditorGUILayout.FloatField("Start Blinking Delay", myProjectile.blinkIndicationLimit);
            myProjectile.startingBlinkDelay = EditorGUILayout.FloatField("Blink Delay", myProjectile.startingBlinkDelay);
            myProjectile.blinkDegradingFactor = EditorGUILayout.Slider("Blink Degrading Factor",myProjectile.blinkDegradingFactor, 0.0f, 1.0f);
            myProjectile.minimumBlinkDelay = EditorGUILayout.FloatField("Minimum Blink Delay", myProjectile.minimumBlinkDelay);
            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel--;
            EditorGUI.EndDisabledGroup();
        }

        // Pathing
        myProjectile.pathingType = (Projectile.Pathing)EditorGUILayout.EnumPopup("Pathing", myProjectile.pathingType);
        Projectile.Pathing path = myProjectile.pathingType;
        
        if(path != Projectile.Pathing.NONE)
        {
            EditorGUI.BeginDisabledGroup(path == Projectile.Pathing.NONE);
            EditorGUI.indentLevel++;
            myProjectile.pathingStrength = EditorGUILayout.Slider("Pathing Strength", myProjectile.pathingStrength, 1.0f, 10.0f);
            myProjectile.explodeOnReachingTarget = EditorGUILayout.Toggle("Explode On Reaching Target", myProjectile.explodeOnReachingTarget);
            myProjectile.minimumExplodingDistanceFromTarget = EditorGUILayout.FloatField("Minimum Exploding Distance", myProjectile.minimumExplodingDistanceFromTarget);
            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel--;
        }
        
        // Bounce
        myProjectile.bounce = EditorGUILayout.Toggle("Bounce Off Boss and Walls", myProjectile.bounce);
        bool bounce = myProjectile.bounce;
        if(bounce)
        {
            EditorGUI.BeginDisabledGroup(bounce == false);
            EditorGUI.indentLevel++;
            myProjectile.electricBounceClip = (AudioClip)EditorGUILayout.ObjectField("Electric Bounce Clip", myProjectile.electricBounceClip, typeof(AudioClip), true);
            myProjectile.electricBounceVolume = EditorGUILayout.FloatField("Electric Bounce Volume", myProjectile.electricBounceVolume);
            myProjectile.electricBouncePitch = EditorGUILayout.FloatField("Electric Bounce Pitch", myProjectile.electricBouncePitch);
            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel--;
        }
        // Damage
        myProjectile.damageMinionsOnContact = EditorGUILayout.Toggle("Damage Minions On Contact", myProjectile.damageMinionsOnContact);
        bool damageMinionsContact = myProjectile.damageMinionsOnContact;
        if(damageMinionsContact)
        {
            EditorGUI.BeginDisabledGroup(damageMinionsContact == false);
            EditorGUI.indentLevel++;
            myProjectile.damageToTarget = EditorGUILayout.FloatField("Damage Dealt", myProjectile.damageToTarget);
            myProjectile.noLimitToDamagedMinions = EditorGUILayout.Toggle("No Limit To Damaged Minions", myProjectile.noLimitToDamagedMinions);
            bool noLimit = myProjectile.noLimitToDamagedMinions;
            EditorGUI.BeginDisabledGroup(noLimit == true);
            myProjectile.maxMinionsDamaged = EditorGUILayout.IntField("Max Minions Damaged", myProjectile.maxMinionsDamaged);
            EditorGUI.EndDisabledGroup();
            myProjectile.explodeOnDamagingMaxMinions = EditorGUILayout.Toggle("Explode on Damaging Max Minions", myProjectile.explodeOnDamagingMaxMinions);
            EditorGUI.EndDisabledGroup();
            EditorGUI.indentLevel--;
        }
        
        // Explode
        myProjectile.onExplodeSpawnKillZone = EditorGUILayout.Toggle("Spawn KillZone On Explode", myProjectile.onExplodeSpawnKillZone);
        bool spawnKillzone = myProjectile.onExplodeSpawnKillZone;
        if(spawnKillzone)
        { 
            EditorGUI.BeginDisabledGroup(spawnKillzone == false);
            EditorGUI.indentLevel++;
            myProjectile.killZonePrefab = (GameObject)EditorGUILayout.ObjectField("KillZone Object", myProjectile.killZonePrefab, typeof(GameObject), false);
            EditorGUI.indentLevel--;
            EditorGUI.EndDisabledGroup();
        }

        myProjectile.onExplodeSpawnOtherProjectiles = EditorGUILayout.Toggle("Spawn Projectiles On Explode", myProjectile.onExplodeSpawnOtherProjectiles);
        bool spawnProjectiles = myProjectile.onExplodeSpawnOtherProjectiles;
        if(spawnProjectiles)
        { 
            EditorGUI.BeginDisabledGroup(spawnProjectiles == false);
            EditorGUI.indentLevel++;
            myProjectile.spawnedProjectileType = (ProjectileManager.ProjectileTypes)EditorGUILayout.EnumPopup("Spawned Projectile Type", myProjectile.spawnedProjectileType);
            myProjectile.projectilesSpawned = EditorGUILayout.IntField("# Spawned", myProjectile.projectilesSpawned);
            myProjectile.explodeClip = (AudioClip)EditorGUILayout.ObjectField("Explosion Clip", myProjectile.explodeClip, typeof(AudioClip), true);
            EditorGUI.indentLevel--;
            EditorGUI.EndDisabledGroup();
        }

    }
	
}

#endif