﻿using System.Collections;
using UnityEngine;
using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Projectiles;
using Assets.Scripts.Entities;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Projectiles
{
    /// <summary>
    /// A projectile is any range object that is used by an entity to fire at the
    /// opposing entity. The projectile it self is only used as an "is-a" relationship
    /// and used provide the most basic operations. All derived projectiles will give
    /// more in-depth projection of its true behaviour.
    /// </summary>
    public sealed class Projectile : MonoBehaviour
    {
		// attributes	
        public float velocity = 1f;

        [Tooltip("Indicates whether or not the projectile should have a lifetime in which it explodes")]
		public bool hasLifeTime;
		public float lifeTime = 5f;
        private float lifeTimeTimer;

        // Blinking variables, or explosion indication variables
        public bool hasBlinkIndication = false;
        [Tooltip("Indicates at what time left to start blinking")]
        public float blinkIndicationLimit = 5f;
        [Tooltip("Indicates the delay of the first blink")]
        public float startingBlinkDelay = 2.0f;
        [Tooltip("Indicates the fraction at which blinking time should decrease")]
        public float blinkDegradingFactor= 0.8f;
        [Tooltip("Indicates the minimum time a blink should last ")]
        public float minimumBlinkDelay = 0.1f;
        private float blinkDelay;
        private float blinkTimer;
        private SpriteRenderer projectileRenderer = new SpriteRenderer();

        // Pathing variables
        public Pathing pathingType = Pathing.NONE;
        public float pathingStrength = 1.0f;
        public GameObject homingObject;
        private GameObject target;
        private Vector2 targetPosition;

        public bool bounce;

        // Contact killing variables
        public bool damageMinionsOnContact;
        public float damageToTarget = 9000f;
        public bool noLimitToDamagedMinions = true;
        public int maxMinionsDamaged = 1;
        private int curMinionsDamaged = 0;

        // On Explosion variables
        [Tooltip("Specific to On Spawn Pathing behavior")]
        public bool explodeOnReachingTarget = true;
        public float minimumExplodingDistanceFromTarget = 0.5f;
        public bool explodeOnDamagingMaxMinions = true;
        public bool onExplodeSpawnKillZone = false;
        public GameObject killZonePrefab;
        public bool onExplodeSpawnOtherProjectiles = false;
        public ProjectileManager.ProjectileTypes spawnedProjectileType;
        public int projectilesSpawned = 10;

        private bool destroyProjectile;
        private Vector2 trajectory;
        private Rigidbody2D body;
        private Animator animator;

        private float curCollideTime;
        private static readonly float COLLIDE_TIME = 0.25f;

        public AudioClip explodeClip;
        public AudioClip electricBounceClip;
        public float electricBounceVolume = 1.0f;
        public float electricBouncePitch = 1.0f;
        #region Enums

        // Determines pathing of the projectile
        public enum Pathing
        {
            NONE,
            HOMING_CLOSEST_MINIONS,
            HOMING_OBJECT,
            HOMING_CLOSEST_MINIONS_ON_SPAWN,
            HOMING_OBJECT_ON_SPAWN
        }

        #endregion

        #region Events
        /// <summary>
        /// Raises the trigger enter2 d event.
        /// </summary>
        /// <param name="coll">Coll.</param>
        /// <param name="coll">Col.</param>
        public void OnTriggerEnter2D(Collider2D coll)
        {
            if ( coll != null && coll.gameObject.tag == "Minion" && damageMinionsOnContact)
            {
                Minion minion = coll.gameObject.GetComponent<Minion>();
                if (minion != null && minion.CanKillUncollectedMinion() && curMinionsDamaged < maxMinionsDamaged)
                {
                    curMinionsDamaged++;
                    minion.LoseHealth(this.damageToTarget);
                    if (curMinionsDamaged >= maxMinionsDamaged && !noLimitToDamagedMinions)
                    {
                        if (explodeOnDamagingMaxMinions)
                        {
                            Explode();
                        }
                    }
                }
            }
            else if ( coll != null && (coll.gameObject.tag == "Tundra" || coll.gameObject.tag == "Boss" || coll.gameObject.tag == "Wall"))
            {
                if(bounce)
                {
        
                }
                else
                { 
                    Explode();
                }
            }
        }

        /// <summary>
        /// When the projectile hits an entity, then the base behaviour is to
        /// deal the specified damage and then be destroyed.
        /// </summary>
        /// <param name="coll">The properties of the entity</param>
        public  void OnCollisionEnter2D(Collision2D coll)
        {
            if (coll != null && coll.gameObject.tag == "Minion" && damageMinionsOnContact)
            {
                Minion minion = coll.gameObject.GetComponent<Minion>();
                if (minion != null && minion.CanKillUncollectedMinion() && curMinionsDamaged < maxMinionsDamaged)
                {
                    curMinionsDamaged++;
                    minion.LoseHealth(this.damageToTarget);
                    if (curMinionsDamaged >= maxMinionsDamaged && !noLimitToDamagedMinions)
                    {
                        if (explodeOnDamagingMaxMinions)
                        {
                            Explode();
                        }
                    }
                }
            }
            else if (coll != null && (coll.gameObject.tag == "Tundra" ||  coll.gameObject.tag == "Boss" || coll.gameObject.tag == "Wall"))
            {
                if (bounce)
                {
                    //Play the electric bounce sound
                    ProjectileManager.Instance.PlayElectricBounce(electricBounceClip, electricBounceVolume, electricBouncePitch);
                    curCollideTime = COLLIDE_TIME;
                    Vector3 normal = coll.contacts[0].normal;
                    normal = Math2d.RandomUnitVector(Vector2.Angle(Vector2.right, Math2d.RotateVector(normal, -30)), Vector2.Angle(Vector2.right, Math2d.RotateVector(normal, 30)));
                    Vector2 reflection = Vector3.Reflect(trajectory, normal);
                    SetTrajectory(reflection);
                }
                else
                {
                    Explode();
                }
            }
        }

        /// <summary>
        /// The bouncy projectiles have a nasty habit of getting stuck in the wall. If they are stuck in the wall for longer
        /// than the threshold timer, then it will go in the opposite trajectory.
        /// </summary>
        /// <param name="coll">The properties of the collision</param>
        public void OnCollisionStay2D(Collision2D coll)
        {
            if (bounce)
            {
                curCollideTime -= Time.deltaTime;
                if (curCollideTime <= 0)
                    SetTrajectory(-trajectory);
            }
        }

        #endregion

        public void Awake()
        {
            body = GetComponent<Rigidbody2D>();
            projectileRenderer = GetComponent<SpriteRenderer> ();
            destroyProjectile = false;
        }

        // Use this for initialization
        void Start()
        {
            // Sets lifetime variable
            lifeTimeTimer = lifeTime;

            // Sets blink varaibles
            blinkDelay = startingBlinkDelay;
            blinkTimer = blinkDelay;

            //Get the Animator
			animator = this.GetComponent<Animator>();
            if (animator != null)
				animator.SetBool ("Explode", false);

            // Sets Pathing target
            if (pathingType == Pathing.HOMING_CLOSEST_MINIONS_ON_SPAWN || pathingType == Pathing.HOMING_CLOSEST_MINIONS)
            {
                this.target = MinionManager.Instance.getMinionClosestToObjectLinearSearch(gameObject);
            }
            else if (pathingType == Pathing.HOMING_OBJECT_ON_SPAWN || pathingType == Pathing.HOMING_OBJECT)
            {
                this.target = homingObject;
            }

            if (this.target != null)
            {
                this.targetPosition = this.target.transform.position;
            }

            //If no target is defined when it should be, explode
            if (pathingType != Pathing.NONE && this.target == null)
            {
                Explode();  
            }
        }
        
        /// <summary>
        /// Fixeds the update.
        /// </summary>
		void FixedUpdate()
		{
            //Lifetime timer update
			if(hasLifeTime)
			{
				lifeTimeTimer -= Time.deltaTime;

                if(hasBlinkIndication && lifeTimeTimer <= blinkIndicationLimit)
                {
                    //Blinking calculation to indicate how close the projectile is to exploding
                    blinkTimer -= Time.deltaTime;
                    if(blinkTimer <=0)
                    {
                        blinkDelay = blinkDelay * blinkDegradingFactor;
                        if(blinkDelay <= minimumBlinkDelay)
                        {
                            blinkDelay = minimumBlinkDelay;
                        }
                        blinkTimer = blinkDelay;
                        Color newColor = projectileRenderer.color;
                        if(newColor.a > 0.9)
                        {
                            projectileRenderer.color = new Color(newColor.r,newColor.g,newColor.b,0.5f);
                        }
                        else
                        {
                            projectileRenderer.color = new Color(newColor.r,newColor.g,newColor.b,1.0f);
                        } 
                    }
                }
				if(lifeTimeTimer <= 0)
				{
					Explode();
				}
			}

            // Updates trajectory based upon pathing
            if (pathingType != Pathing.NONE) 
            {
                //Sets target object
                if(pathingType == Pathing.HOMING_CLOSEST_MINIONS )
                {
                    this.target = MinionManager.Instance.getMinionClosestToObjectLinearSearch(gameObject);
                }
                else if (pathingType == Pathing.HOMING_OBJECT)
                {
                    this.target = homingObject;
                }

                // Update target position based upon pathing type
                if (pathingType == Pathing.HOMING_CLOSEST_MINIONS || pathingType == Pathing.HOMING_OBJECT)
                {
                    //If target is defined, define target position, else explode
                    if (this.target != null)
                    {
                        this.targetPosition = this.target.transform.position;
                    }
                    else
                    {
                        Explode();
                    }
                }

                //Update trajectory
                Vector2 direction = (targetPosition - (Vector2)transform.position).normalized;
                SetTrajectory( (this.trajectory + ( pathingStrength * direction)).normalized);
            }

			body.MovePosition(transform.position + (Vector3)(trajectory * velocity * Time.deltaTime));
			
            // Exploding on reaching target position
            if(explodeOnReachingTarget && ((targetPosition - (Vector2)transform.position).sqrMagnitude < minimumExplodingDistanceFromTarget))
            {
                Explode();
            }

			if (destroyProjectile)
				Explode();
		}

        /// <summary>
        /// This will change the heading of the projectile.
        /// </summary>
        /// <param name="trajectory">The heading.</param>
        public void SetTrajectory(Vector2 trajectory)
        {
            this.trajectory = trajectory;
        }

		/// <summary>
		/// Explodes the Projectile
		/// **If your projectile has an animator**
		/// There must be a state named "Done" in the controller AND it must be set as the state at some point.
		/// </summary>
        public void Explode()
        {
            destroyProjectile = true;
            if (animator != null)
                animator.SetBool ("Explode", true);

            if (animator == null || animator.GetCurrentAnimatorStateInfo (0).IsName ("Done"))
			{
                if (onExplodeSpawnOtherProjectiles)
                {
                    int i = 0;
                    while (i < projectilesSpawned)
                    {
                        ProjectileManager.Instance.FireProjectile(spawnedProjectileType, transform.position,
                                                                  new Vector2((Random.Range(-100, 100) / 100.0f), (Random.Range(-100, 100) / 100.0f)));
                        ProjectileManager.Instance.PlayExplosion(explodeClip);
                        i++;
                    }
                }

                if (onExplodeSpawnKillZone)
                {
                    SpawnKillZone();
                }

                Destroy(gameObject);
			}
        }

        private void SpawnKillZone()
        {
            if (killZonePrefab != null)
            {
                Instantiate(killZonePrefab, transform.position, Quaternion.identity);
            }
            else
            {
                Debug.LogError("Instance of killzone not specified");
            }
        }
    }
}
