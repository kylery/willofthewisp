﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Projectiles
{
    /// <summary>
    /// The projectile manager handles the creation and management of all projectiles and
    /// bullets that are created. It is a singleton class so that there will only be one
    /// instance of the manager.
    /// </summary>
    public sealed class ProjectileManager : MonoBehaviour
    {
		public GameObject projectileMiniFireballPrefab;
        public GameObject projectileMediumFireballPrefab;

        public GameObject projectileMediumFireballNado;

        public GameObject projectileMediumElectricballPrefab;
        public GameObject projectileMediumElectricBouncyBallPrefab;

        public GameObject projectileMediumElectricPrefab;

        private static ProjectileManager instance;

        private Dictionary<ProjectileTypes, GameObject> projectileTypeToPrefab;
        private Dictionary<ProjectileTypes, List<GameObject>> projectilePool;

        private AudioSource sound;

        /// <summary>
        /// The different types of projectiles that can be shot.
        /// </summary>
        public enum ProjectileTypes
        {
            //Turtle Boss
			MINI_FIREBALL,
            MEDIUM_FIREBALL,
            MEDIUM_FIREBALLNADO,

            // Shorca
            MEDIUM_ELECTRICBALL,
            MEDIUM_ELECTRICBOUNCYBALL,

            MEDIUM_ELECTRIC
        }

        /// <summary>
        /// The current instance of the Projectile Manager.
        /// </summary>
        public static ProjectileManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (ProjectileManager)FindObjectOfType(typeof(ProjectileManager));
                    if (instance == null)
                        UnityEngine.Debug.LogError("Projectile Manager does not exist");
                }
                return instance;
            }
        }
        
        /// <summary>
        /// Initializes the manager and sets up the dictionary that
        /// maps the projectile types to its respective prefab.
        /// </summary>
        void Start()
        {
            projectileTypeToPrefab = new Dictionary<ProjectileTypes, GameObject>();
			//Turtle
            projectileTypeToPrefab.Add (ProjectileTypes.MINI_FIREBALL, projectileMiniFireballPrefab);
            projectileTypeToPrefab.Add (ProjectileTypes.MEDIUM_FIREBALL, projectileMediumFireballPrefab);
            projectileTypeToPrefab.Add(ProjectileTypes.MEDIUM_FIREBALLNADO, projectileMediumFireballNado);

            //Shorca
            projectileTypeToPrefab.Add (ProjectileTypes.MEDIUM_ELECTRICBALL, projectileMediumElectricballPrefab);
            projectileTypeToPrefab.Add(ProjectileTypes.MEDIUM_ELECTRICBOUNCYBALL, projectileMediumElectricBouncyBallPrefab);

            projectileTypeToPrefab.Add(ProjectileTypes.MEDIUM_ELECTRIC, projectileMediumElectricPrefab);

            sound = gameObject.GetComponent<AudioSource>();
            if(sound == null)
            {
                Debug.LogError("There is no audio source in projectile manager. " + gameObject);
                return;
            }
        }

        /// <summary>
        /// This will create a projectile.
        /// </summary>
        /// <param name="projectilePrefab">The kind of projectile to create.</param>
        /// <param name="position">The position of the projectile</param>
        /// <param name="trajectory">The heading of the projectile.</param>
        /// <returns>The projectiel object fired</returns>
        public GameObject FireProjectile(ProjectileTypes projectileType, Vector2 position, Vector3 trajectory)
        {
            GameObject projectilePrefab = projectileTypeToPrefab[projectileType];
            GameObject projectile = (GameObject)Instantiate(projectilePrefab, position, Quaternion.identity);
            projectile.GetComponent<Projectile>().SetTrajectory(trajectory);
            projectile.transform.parent = transform;
			projectile.transform.right = ((Vector2)trajectory).normalized;
            return projectile;
        }

        public void PlayExplosion(AudioClip clip)
        {
            if(clip == null)
            {
                Debug.LogError("There is no clip in " + gameObject);
                return;
            }
            sound.clip = clip;
            sound.Play();
        }

        public void PlayElectricBounce(AudioClip clip, float volume, float pitch)
        {
            if(clip == null)
            {
                Debug.LogError("There is no clip in " + gameObject);
                return;
            }
            sound.clip = clip;
            sound.volume = volume;
            sound.pitch = pitch;
            sound.Play();
        }
    }
}
