﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System;

using Assets.Scripts.Utils;
using Assets.Scripts.Entities.Player;
using Assets.Scripts.Entities.Swarm;
using Assets.Scripts.Entities;
using Assets.Scripts.Environment;
using Assets.Scripts.Effects;

namespace Assets.Scripts
{
    /// <summary>
    /// This will hold persistent data that will contain
    /// the progression of the player.
    /// </summary>
    [System.Serializable]
    struct GameData
    {
        public Dictionary<String, bool> turtleLevelsBeaten;
        public Dictionary<String, bool> shorcaLevelsBeaten;
        public Dictionary<String, bool> turtleTrophiesUnlocked;
        public Dictionary<String, bool> shorcaTrophiesUnlocked;

        public bool firstPlayThrough;
        public bool usingController;
    }

    /// <summary>
    /// The game manager contains all the global states of the game and gives outside
    /// sources the ability to send information and commands to the rest of the core
    /// game.
    /// </summary>
    public sealed class GameManager : MonoBehaviour
    {
        public Texture2D loadScreen;
        public Texture2D articLoadScreen;
        public Texture2D tropicLoadScreen;
        public GameObject gameOverPrefab;
        public GameObject winPrefab;

        private bool loadingLevel = false;
        private Texture2D loadingScreenImage;

        private static readonly string GAME_DATA_FILE_NAME = "/savedGame.sd";

        public enum GameState
        {
            MAIN_MENU,
            CORE_GAME
        }

        private static GameManager instance;

        /// <summary>
        /// The current instance of the manager.
        /// </summary>
        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = (GameManager)FindObjectOfType(typeof(GameManager));
                    if (instance == null)
                        UnityEngine.Debug.LogError("Game Manager does not exist");
                    DontDestroyOnLoad(instance.gameObject);
                }
                return instance;
            }
        }

        private GameState gameState;
        private GameData gameData;


        /// <summary>
        /// Is the game paused.
        /// </summary>
        public bool isPaused
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Returns if this is the players first playthrough. It will be set to
        /// false when the game data is saved for the first time.
        /// </summary>
        public bool IsFirstPlayThrough
        {
            get { return gameData.firstPlayThrough; }
        }

        /// <summary>
        /// This will return true if the player is using the controller, otherwise
        /// they are using the keyboard.
        /// </summary>
        public bool UsingController
        {
            get { return gameData.usingController; }
        }

        /// <summary>
        /// Given a level name, this will determine if the player has beaten the level or not.
        /// </summary>
        public bool LevelBeaten(string level)
        {
            Dictionary<String, bool> levelsBeaten = GetLevelsBeatenDict(level);
            return levelsBeaten[level];
        }

        /// <summary>
        /// Given a level name, this will determine if the player has recieved the trophy for the level.
        /// </summary>
        public bool TrophyUnlocked(string level)
        {
            Dictionary<String, bool> trophiesBeaten = GetTrophiesUnlockedDict(level);
            return trophiesBeaten[level];
        }

        /// <summary>
        /// This will unlock the trophy of the given level.
        /// </summary>
        public void UnlockTrophy(string level)
        {
            Dictionary<String, bool> trophiesBeaten = GetTrophiesUnlockedDict(level);
            trophiesBeaten[level] = true;
            SaveGameData();
        }

        /// <summary>
        /// This will change the players preferred way of playing with a controller or keyboard.
        /// </summary>
        /// <param name="usingController">If the player wants to play with a controller</param>
        public void SetUsingController(bool usingController)
        {
            gameData.usingController = usingController;
            SaveGameData();

            ControllerKeyboardInput[] inputs = FindObjectsOfType<ControllerKeyboardInput>();
            foreach(ControllerKeyboardInput input in inputs)
            {
                input.UpdateIcon();
            }
        }

        /// <summary>
        /// This will check to see if the level exists and then change to
        /// it if it does.
        /// </summary>
        /// <param name="level">The level to change to.</param>
        public void ChangeLevel(string level)
        {
            DebugUtils.Assert(Application.CanStreamedLevelBeLoaded(level), "Level does not exist.");

            if(IsFirstPlayThrough)
            {
                CompletedFirstPlaythrough();
            }

            switch (level)
            {
                case Constants.SHORCA_BOSS_LEVEL:
                case Constants.SHORCA_CHALLENGE_ONE_LEVEL:
                case Constants.SHORCA_CHALLENGE_THREE_LEVEL:
                case Constants.SHORCA_CHALLENGE_TWO_LEVEL:
                    loadingScreenImage = articLoadScreen;
                    break;
                case Constants.TURTLE_BOSS_LEVEL:
                case Constants.TURTLE_CHALLENGE_ONE_LEVEL:
                case Constants.TURTLE_CHALLENGE_THREE_LEVEL:
                case Constants.TURTLE_CHALLENGE_TWO_LEVEL:
                    loadingScreenImage = tropicLoadScreen;
                    break;
                default:
                    loadingScreenImage = loadScreen;
                    break;
            }

            MusicController.Instance.StopMusic();
            PauseGame();
            enabled = true;
            ChangeGameState(level);
            loadingLevel = true;

            Application.LoadLevel(level);
            if (Time.timeScale == 0)
                ResumeGame();
        }

        /// <summary>
        /// A quick fix to always resume the game when a new level is loaded.
        /// 
        /// TODO: Find a better way to resume the game when the loading screen is finished.
        /// </summary>
        /// <param name="level">number of the loaded level.</param>
        void OnLevelWasLoaded(int level)
        {
            loadingLevel = false;
            ResumeGame();
        }

        /// <summary>
        /// This provides a direct way to get to the game over screen and
        /// reduce error.
        /// </summary>
        public void GameOver()
        {
			GameOverMenuController gomc = GameOverMenuController.Instance;
			gomc.SetChildrenActive (true);
			PauseMenuController pmc = PauseMenuController.Instance;
			GameObject.Destroy (pmc.gameObject);
            PlayerManager.Instance.Player.SetActive(false);
            MusicController.Instance.PlayGameOver();
        }

        /// <summary>
        /// This provides a direct way to display the win screen
        /// </summary>
        public void WinScreen()
        {
			WinMenuController wmc = WinMenuController.Instance;
			wmc.SetChildrenActive (true);
			PauseMenuController pmc = PauseMenuController.Instance;
			GameObject.Destroy (pmc.gameObject);
            PlayerManager.Instance.Player.SetActive(false);
            MusicController.Instance.PlayVictory();
        }

        /// <summary>
        /// This will progress the game to the next level ending
        /// at the Win Screen when the final level is complete.
        /// </summary>
        /// <param name="level">The current level.</param>
        public void UpdateGameBossDefeated(string level)
        {
            Dictionary<String, bool> levelsBeaten = GetLevelsBeatenDict(level);
            levelsBeaten[level] = true;
            SaveGameData();
            WinScreen();
        }

        /// <summary>
        /// This will return the dictionary that contains if levels have been beaten or not
        /// depending if the level is an orca or turtle level.
        /// </summary>
        private Dictionary<String, bool> GetLevelsBeatenDict(string level)
        {
            switch (level)
            {
                case Constants.TURTLE_CHALLENGE_THREE_LEVEL:
                case Constants.TURTLE_CHALLENGE_TWO_LEVEL:
                case Constants.TURTLE_CHALLENGE_ONE_LEVEL:
                case Constants.TURTLE_BOSS_LEVEL:
                    return gameData.turtleLevelsBeaten;
                case Constants.SHORCA_CHALLENGE_THREE_LEVEL:
                case Constants.SHORCA_CHALLENGE_TWO_LEVEL:
                case Constants.SHORCA_CHALLENGE_ONE_LEVEL:
                case Constants.SHORCA_BOSS_LEVEL:
                    return gameData.shorcaLevelsBeaten;
                default:
                    Debug.LogError("Unknown Boss Defeated");
                    return null;
            }
        }

        /// <summary>
        /// This will return the dictionary that contains if a trophy has been unlocked
        /// depending if the level is an orca or turtle level.
        /// </summary>
        private Dictionary<String, bool> GetTrophiesUnlockedDict(string level)
        {
            switch (level)
            {
                case Constants.TURTLE_CHALLENGE_THREE_LEVEL:
                case Constants.TURTLE_CHALLENGE_TWO_LEVEL:
                case Constants.TURTLE_CHALLENGE_ONE_LEVEL:
                case Constants.TURTLE_BOSS_LEVEL:
                    return gameData.turtleTrophiesUnlocked;
                case Constants.SHORCA_CHALLENGE_THREE_LEVEL:
                case Constants.SHORCA_CHALLENGE_TWO_LEVEL:
                case Constants.SHORCA_CHALLENGE_ONE_LEVEL:
                case Constants.SHORCA_BOSS_LEVEL:
                    return gameData.shorcaTrophiesUnlocked;
                default:
                    Debug.LogError("Unknown Boss Defeated");
                    return null;
            }
        }

        /// <summary>
        /// This will set the first playthrough as being complete so the user
        /// doesn't have to redo any setup steps again.
        /// </summary>
        public void CompletedFirstPlaythrough()
        {
            gameData.firstPlayThrough = false;
            SaveGameData();
        }

        /// <summary>
        /// This will unpause the game.
        /// </summary>
        public void ResumeGame()
        {
			isPaused = false;
            Time.timeScale = 1;
            AudioListener.pause = false;
        }

        /// <summary>
        /// This will pause the game by disable all the game object's.
        /// </summary>
        private void PauseGame()
        {
			isPaused = true;
            Time.timeScale = 0;
            AudioListener.pause = true;
        }

        /// <summary>
        /// This will display the loading screen and do any animation to distract the user.
        /// </summary>
        void OnGUI()
        {
            if (loadingLevel)
            {
                MusicController.Instance.StopMusic();
                AudioSource[] soundList = FindObjectsOfType<AudioSource>();
                foreach (AudioSource sound in soundList)
                {
                    sound.Stop();
                }

                Rect drawWindow = new Rect(0, 0, Screen.width, Screen.height);
                GUI.DrawTexture(drawWindow, loadingScreenImage, ScaleMode.StretchToFill);
            }
        }

        /// <summary>
        /// This will initailize the game at a high-level.
        /// </summary>
        void Awake()
        {
            if (FindObjectsOfType(typeof(GameManager)).Length > 1)
            {
                DestroyImmediate(gameObject);
            }
            isPaused = false;

            ChangeGameState(Application.loadedLevelName);

            if (File.Exists(Application.persistentDataPath + GAME_DATA_FILE_NAME))
            {
                var formatter = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + GAME_DATA_FILE_NAME, FileMode.Open);

                try
                {
                    gameData = (GameData)formatter.Deserialize(file);
                }
                catch (SerializationException)
                {
                    Debug.LogError("You're save file isn't up to the current specs. Delete your save file and play again.\nYour save file is located at " + Application.persistentDataPath);
                }
                finally
                {
                    file.Close();
                }
            }
            else
            {
                CreateInitalGameFile();
            }
        }

        /// <summary>
        /// This will create the initial game file that a player playing for the first time will need.
        /// </summary>
        private void CreateInitalGameFile()
        {
            gameData = new GameData();

            gameData.turtleLevelsBeaten = new Dictionary<string, bool>();
            gameData.turtleLevelsBeaten.Add(Constants.TURTLE_BOSS_LEVEL, false);
            gameData.turtleLevelsBeaten.Add(Constants.TURTLE_CHALLENGE_ONE_LEVEL, false);
            gameData.turtleLevelsBeaten.Add(Constants.TURTLE_CHALLENGE_TWO_LEVEL, false);
            gameData.turtleLevelsBeaten.Add(Constants.TURTLE_CHALLENGE_THREE_LEVEL, false);

            gameData.shorcaLevelsBeaten = new Dictionary<string, bool>();
            gameData.shorcaLevelsBeaten.Add(Constants.SHORCA_BOSS_LEVEL, false);
            gameData.shorcaLevelsBeaten.Add(Constants.SHORCA_CHALLENGE_ONE_LEVEL, false);
            gameData.shorcaLevelsBeaten.Add(Constants.SHORCA_CHALLENGE_TWO_LEVEL, false);
            gameData.shorcaLevelsBeaten.Add(Constants.SHORCA_CHALLENGE_THREE_LEVEL, false);

            gameData.turtleTrophiesUnlocked = new Dictionary<string, bool>();
            gameData.turtleTrophiesUnlocked.Add(Constants.TURTLE_BOSS_LEVEL, false);
            gameData.turtleTrophiesUnlocked.Add(Constants.TURTLE_CHALLENGE_ONE_LEVEL, false);
            gameData.turtleTrophiesUnlocked.Add(Constants.TURTLE_CHALLENGE_TWO_LEVEL, false);
            gameData.turtleTrophiesUnlocked.Add(Constants.TURTLE_CHALLENGE_THREE_LEVEL, false);

            gameData.shorcaTrophiesUnlocked = new Dictionary<string, bool>();
            gameData.shorcaTrophiesUnlocked.Add(Constants.SHORCA_BOSS_LEVEL, false);
            gameData.shorcaTrophiesUnlocked.Add(Constants.SHORCA_CHALLENGE_ONE_LEVEL, false);
            gameData.shorcaTrophiesUnlocked.Add(Constants.SHORCA_CHALLENGE_TWO_LEVEL, false);
            gameData.shorcaTrophiesUnlocked.Add(Constants.SHORCA_CHALLENGE_THREE_LEVEL, false);

            gameData.firstPlayThrough = true;
            gameData.usingController = true;
            SaveGameData();
        }

        /// <summary>
        /// This checks user inputs to affect game state such
        /// as pausing the game.
        /// </summary>
        void Update()
        {
            InputManager inputManager = InputManager.Instance;

            switch(gameState)
            {
                case GameState.MAIN_MENU:
                    break;
                case GameState.CORE_GAME:
                    if (inputManager.GetButtonUp(InputManager.ButtonTypes.START) || Input.GetKeyUp(KeyCode.Escape))
                    {
                        if (Application.loadedLevelName != Constants.MAIN_MENU_LEVEL)
                        {
                            if (isPaused)
                            {
                                ResumeGame();
                            }
                            else
                            {
                                PauseGame();
                            }
                        }
                    }
                    
                    break;
            }
        }

        /// <summary>
        /// When the player beats a boss it will unlock the level for the player. The
        /// progress needs to be saved so that when the player starts the game again
        /// they can continue where they left off.
        /// </summary>
        private void SaveGameData()
        {
            var formatter = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + GAME_DATA_FILE_NAME);
            formatter.Serialize(file, gameData);
            file.Close();
        }

        /// <summary>
        /// This will change the state of the Game depending on the level.
        /// The game will default to the core game unless given a special level
        /// that specifies that is a menu.
        /// </summary>
        /// <param name="level">The level the game will switch to.</param>
        private void ChangeGameState(string level)
        {
            switch (level)
            {
                case "Load_Main_Menu":
                case Constants.CREDITS_LEVEL:
                    gameState = GameState.MAIN_MENU;
                    break;
                default:
                    gameState = GameState.CORE_GAME;
                    break;
            }
        }

    }
}
