﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Effects
{
    /// <summary>
    /// Script to set a particle field if a certain sorting layer on start
    /// (since this functionality is not in the inspector by default).
    /// </summary>
    public class ParticleSortingLayer : MonoBehaviour
    {
        [SerializeField]
        private string layer;

        /// <summary>
        /// Set the sorting layer of the particle system
        /// </summary>
        void Start ()
        {
            GetComponent<ParticleSystem>().renderer.sortingLayerName = layer;
        }
    }
}
