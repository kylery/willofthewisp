﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Utils;

namespace Assets.Scripts.Effects
{
    public class CanvasControllerKeyboardInput : MonoBehaviour
    {
        [SerializeField]
        private Sprite keyboardIcon;
        [SerializeField]
        private Sprite controllerIcon;

        private UnityEngine.UI.Image imageRenderer;

        void Awake()
        {
            imageRenderer = GetComponent<UnityEngine.UI.Image>();
            UpdateIcon();
        }

        /// <summary>
        /// This will update the sprite to reflect the correct icon depending if the user
        /// is using a controller or keyboard.
        /// </summary>
        public void UpdateIcon()
        {
            DebugUtils.Assert(imageRenderer != null, "Sprite Renderer needs to be set.");
            DebugUtils.Assert(keyboardIcon != null, "Keyboard Icon needs to be set.");
            DebugUtils.Assert(controllerIcon != null, "Controller Icon needs to be set.");

            if (GameManager.Instance.UsingController)
            {
                imageRenderer.sprite = controllerIcon;
            }
            else
            {
                imageRenderer.sprite = keyboardIcon;
            }
        }

        /// <summary>
        /// When the icon is enabled again, this has to make sure that a change in controller
        /// input hasn't changed while inactive.
        /// </summary>
        void OnEnable()
        {
            UpdateIcon();
        }
    }
}
