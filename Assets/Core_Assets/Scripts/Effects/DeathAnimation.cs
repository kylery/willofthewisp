﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Effects
{
    /// <summary>
    /// This script deletes a gameobject after an animation has finished.
    /// </summary>
    public class DeathAnimation : MonoBehaviour
    {
        private Animator anim;

        /// <summary>
        /// Get the animator of object
        /// </summary>
        void Awake()
        {
            anim = GetComponent<Animator>();
        }

        /// <summary>
        /// Delete the animation when it enters a Finished state.
        /// </summary>
        void Update()
        {
            if(anim.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Finished"))
            {
                Destroy(gameObject);
            }
        }
    }
}
