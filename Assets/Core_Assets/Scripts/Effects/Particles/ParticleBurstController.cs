﻿using UnityEngine;
using System.Collections;


namespace Assets.Particles
{ 
    public class ParticleBurstController : MonoBehaviour {

        private ParticleSystem particleBurstSystem;

	    // Use this for initialization
	    void Awake () {
            particleBurstSystem = GetComponent<ParticleSystem>();
	    }
	
	    // Update is called once per frame
	    void Update () {
            if (particleBurstSystem == null || !particleBurstSystem.IsAlive())
                Destroy(gameObject);
	    }
    }

}