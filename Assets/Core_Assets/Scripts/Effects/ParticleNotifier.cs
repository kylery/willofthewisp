﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Effects
{
    /// <summary>
    /// This will start and stop a particle effect if the system wants the user to
    /// know something without directly telling them.
    /// </summary>
    public class ParticleNotifier : MonoBehaviour
    {
        /// <summary>
        /// Notify the user by starting the particle system.
        /// </summary>
        public void Notify()
        {
            GetComponent<ParticleSystem>().Play();
        }

        /// <summary>
        /// When the user has done what they need to, then stop the particle
        /// system.
        /// </summary>
        public void StopNotification()
        {
            GetComponent<ParticleSystem>().Stop();
        }
    }
}
