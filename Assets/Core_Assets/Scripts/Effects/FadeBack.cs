﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Effects
{
    public class FadeBack : MonoBehaviour
    {
        private Color nonDamagedColor;
        [SerializeField]
        private Color damagedColor;
        [SerializeField]
		private SpriteRenderer sprite; //if not set, will assume sprite renderer is on this GameObject
		[SerializeField]
        private float visualDamageFade;
        private float curVisualDamageFade;
//        private SpriteRenderer sprite;
        private bool fading;

        void Awake()
        {
//			if (sprite == null)
//				sprite = transform.parent.FindChild ("Mesh").GetComponent<SpriteRenderer> ();
//			else
//				sprite = GetComponent<SpriteRenderer> ();
			if (sprite == null)
				sprite = GetComponent<SpriteRenderer> ();

			Utils.DebugUtils.Assert(sprite != null, "couldn't find the Sprite. Should be attached to sibling GameObject title 'Mesh'.");
            nonDamagedColor = sprite.color;
        }

        // Use this for initialization
        void Start()
        {
            curVisualDamageFade = 0;
            fading = false;
        }

        // Update is called once per frame
        void Update()
        {
            //Fade from damaged to healthy
            if (fading)
            {
                curVisualDamageFade -= Time.deltaTime;
                //Fade back to the nondamaged color
                sprite.color = Color.Lerp(nonDamagedColor, damagedColor, curVisualDamageFade / visualDamageFade);
                if (curVisualDamageFade <= 0)
                {
                    EndFade();
                }
            }
        }
        public void BeginFade()
        {
            if(enabled)
            {
                if(!fading)
                  nonDamagedColor = sprite.color;
                fading = true;
                curVisualDamageFade = visualDamageFade;
                sprite.color = damagedColor;
            }
        }

        public void EndFade()
        {
            if(enabled)
            {
                fading = false;
                sprite.color = nonDamagedColor;
            }
        }

    }
}
