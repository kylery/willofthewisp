﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Effects
{
    public class MoveViewBox : MonoBehaviour
    {
        public enum MoveDirection
        {
            UP,
            RIGHT
        }

        public float moveSpeed;
        public MoveDirection moveDirection;

        private bool startMoving;
        private float delta;

        void Awake()
        {
            startMoving = false;
            delta = 0;
        }

        void FixedUpdate()
        {
            if (startMoving)
            {
                bool deleteViewBox = false;
                if (moveDirection == MoveDirection.UP)
                {
                    delta += Mathf.Abs(moveSpeed * Time.deltaTime);
                    transform.position += new Vector3(0, moveSpeed * Time.deltaTime, 0);
                    if (delta >= GetComponent<RectTransform>().rect.height)
                        deleteViewBox = true;
                }
                else
                {
                    delta += moveSpeed * Time.deltaTime;
                    transform.position += new Vector3(moveSpeed * Time.deltaTime, 0, 0);
                    if (delta >= GetComponent<RectTransform>().rect.width)
                        deleteViewBox = true;
                }

                if (deleteViewBox)
                    GameObject.Destroy(gameObject);
            }
        }

        public void StartMoving()
        {
            startMoving = true;
        }
    }
}
