﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Assets.Scripts.Utils;

namespace Assets.Scripts.Effects
{
    /// <summary>
    /// Since this a PC release, it is prudent to allow the player the choice to use either the keyboard
    /// or a controller. They will decide this when first starting up the game. This will select the appropriate
    /// icon to show to the player depending on the control they want to use.
    /// </summary>
    public class ControllerKeyboardInput : MonoBehaviour
    {
        [SerializeField]
        private Sprite keyboardIcon;
        [SerializeField]
        private Sprite controllerIcon;

        private SpriteRenderer spriteRenderer;

        void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            UpdateIcon();
        }

        /// <summary>
        /// This will update the sprite to reflect the correct icon depending if the user
        /// is using a controller or keyboard.
        /// </summary>
        public void UpdateIcon()
        {
            DebugUtils.Assert(spriteRenderer != null, "Sprite Renderer needs to be set.");
            DebugUtils.Assert(keyboardIcon != null, "Keyboard Icon needs to be set.");
            DebugUtils.Assert(controllerIcon != null, "Controller Icon needs to be set.");

            if(GameManager.Instance.UsingController)
            {
                spriteRenderer.sprite = controllerIcon;
            }
            else
            {
                spriteRenderer.sprite = keyboardIcon;
            }
        }

        /// <summary>
        /// When the icon is enabled again, this has to make sure that a change in controller
        /// input hasn't changed while inactive.
        /// </summary>
        void OnEnable()
        {
            UpdateIcon();
        }
    }
}
