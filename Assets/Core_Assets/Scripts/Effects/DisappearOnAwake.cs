﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Effects
{
    /// <summary>
    /// This allows a quick way to make game objects not render while still being able to
    /// update.
    /// </summary>
    public class DisappearOnAwake : MonoBehaviour
    {
        void Awake()
        {
            renderer.enabled = false;
        }
    }
}